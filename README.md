

Miko Engine
===========

Game engine for bullet hell games.

Dependencies
------------

Build tools
* [CMake](https://cmake.org/) Build file generator

Libraries

* [GLEW](http://glew.sourceforge.net/)  The OpenGL Extension Wrangler library
* [GLM](http://glm.g-truc.net/) The OpenGL Mathematics library
* [ImGUI](https://github.com/ocornut/imgui) Immediate mode GUI library
* [STB](https://github.com/nothings/stb) Single-file public domain libraries
* [SDL](https://www.libsdl.org/) Simple DirectMedia Layer

Building
--------

Install dependencies and then in the build directory:

~~~{.sh}
$ cmake CMakeLists.txt
$ make
~~~

Documentation
-------------

To generate HTML documentation for the engine, run the following command which
will build the the documentation in doc/generated directory

~~~{.sh}
make doc
~~~

Authors
-------

Joel Polso [joel.polso@gmail.com](joel.polso@gmail.com) 2016

