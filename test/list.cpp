#include "miko/container/list.hpp"

#include "miko/io/file_logger.hpp"

#include <CppUTest/TestHarness.h>
#include <CppUTest/CommandLineTestRunner.h>

using namespace miko;

TEST_GROUP(Basic)
{
};

TEST(Basic, Construct)
{
    List<int> intList(defaultAllocator);
    List<std::string> stringList(defaultAllocator);
}

TEST_GROUP(IntList)
{
    void setup()
    {
        list = new List<int>(defaultAllocator);
    }

    void teardown()
    {
        delete list;
    }

    List<int> *list;
};


TEST(IntList, Prepend)
{
    for (int i = 0; i < 100; i++) {
        list->prepend(i);
    }

    CHECK_EQUAL(list->size(), 100);
}

TEST(IntList, Append)
{
    for (int i = 0; i < 100; i++) {
        list->append(i);
    }

    CHECK_EQUAL(list->size(), 100);
}

TEST(IntList, Clear)
{
    for (int i = 0; i < 100; i++) {
        list->append(i);
    }

    CHECK_EQUAL(list->size(), 100);
    list->clear();
    CHECK_EQUAL(list->size(), 0);
}

TEST(IntList, PrependAppend)
{
    for (int i = 0; i < 100; i++) {
        if (i % 2 == 0) {
            list->append(i);
        }
        else {
            list->prepend(i);
        }
    }

    CHECK_EQUAL(list->size(), 100);
}

TEST(IntList, PrependErase)
{
    for (int i = 0; i < 100; i++) {
        list->prepend(i);
    }

    CHECK_EQUAL(list->size(), 100);

    for (int i = 0; i < 100; i++) {
        list->erase(list->begin());
    }

    CHECK_EQUAL(list->size(), 0);
}

TEST(IntList, AppendErase)
{
    for (int i = 0; i < 100; i++) {
        list->append(i);
    }

    CHECK_EQUAL(list->size(), 100);

    for (int i = 0; i < 100; i++) {
        list->erase(list->begin());
    }

    CHECK_EQUAL(list->size(), 0);
}

TEST(IntList, PrependAppendErase)
{
    for (int i = 0; i < 100; i++) {
        if (i % 2 == 0) {
            list->append(i);
        }
        else {
            list->prepend(i);
        }
    }
    CHECK_EQUAL(list->size(), 100);

    for (int i = 0; i < 100; i++) {
        list->erase(list->begin());
    }

    CHECK_EQUAL(list->size(), 0);
}

TEST(IntList, Iterator)
{
    List<int>::Iterator iter1;
    List<int>::Iterator iter2(list->begin());
    List<int>::Iterator iter3 = list->begin();

    // Hush compiler
    (void) iter1;
    (void) iter2;
    (void) iter3;
}

TEST(IntList, IteratorSanity)
{
    CHECK(list->begin() == list->begin());
    CHECK(list->end() == list->end());
    CHECK(list->begin() == list->end());

    list->append(1);

    CHECK(list->begin() == list->begin());
    CHECK(list->end() == list->end());
    CHECK(list->begin() != list->end());

    list->erase(list->begin());

    CHECK(list->begin() == list->begin());
    CHECK(list->end() == list->end());
    CHECK(list->begin() == list->end());
}

TEST(IntList, Iterate)
{
    for (int i = 0; i < 100; i++) {
        list->append(i);
    }

    List<int>::Iterator iter;

    iter = list->begin();

    for (int i = 0; i < 100; i++) {
        CHECK(iter != list->end());
        CHECK_EQUAL(*iter, i);
        ++iter;
    }

    CHECK(iter == list->end());
}

TEST(IntList, IterateErase)
{
    for (int i = 0; i < 100; i++) {
        list->append(i);
    }

    List<int>::Iterator iter = list->begin();
    for (int i = 0; i < 100; i++) {
        if (i % 2 == 0) {
            iter = list->erase(iter);
        }
        else {
            iter++;
        }
    }
}

TEST(IntList, IterateInsert)
{
    list->insertBefore(list->end(), 3);
    list->insertBefore(list->begin(), 1);
    list->insertAfter(list->begin(), 2);

    CHECK_EQUAL(list->size(), 3);

    List<int>::Iterator iter = list->begin();
    CHECK_EQUAL(*iter, 1);
    iter++;

    CHECK_EQUAL(*iter, 2);
    iter++;

    CHECK_EQUAL(*iter, 3);
    iter++;

    CHECK(iter == list->end());

    list->removeFirst();
    list->removeLast();
    list->removeFirst();

    CHECK(list->size() == 0);
    CHECK(list->begin() == list->end());
}

TEST(IntList, HugeList)
{
    for (int i = 0; i < 10000; i++) {
        list->append(i);
    }

    CHECK_EQUAL(list->size(), 10000);

    List<int>::Iterator iter = list->begin();
    for (int i = 0; i < 10000; i++) {
        CHECK_EQUAL(*iter, i);
        CHECK(iter != list->end());
        iter++;
    }

    CHECK(iter == list->end());
}

TEST_GROUP(StringList)
{
};

TEST_GROUP(RefList)
{
};

int main(int argc, const char *argv[])
{
    FileLogger logger(stderr, false, true);
    logger.listen();

    return CommandLineTestRunner::RunAllTests(argc, argv);
}
