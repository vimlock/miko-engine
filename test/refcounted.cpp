#include "miko/util/refcounted.hpp"
#include "miko/memory/memory.hpp"

#include <CppUTest/TestHarness.h>
#include <CppUTest/CommandLineTestRunner.h>

using namespace miko;

class Int : public Refcounted
{
public:
    Int(int v)
    {
        value = v;
    }

    int value;
};

TEST_GROUP(IntRef)
{
};

TEST(IntRef, Construct)
{
    Ref<Int> a;
    Ref<Int> b(MIKO_NEW(defaultAllocator, Int) (10));
    Ref<Int> c(b);
    Ref<Int> d(&*c);

    CHECK(b->getRefcount() == 3);
    CHECK(!a);
    CHECK(b == d);

    CHECK_EQUAL(d->value, 10);

    {
        Ref<Int> e = d;
        CHECK(e->getRefcount() == 4);
    }

    CHECK(d->getRefcount() == 3);
}

int main(int argc, const char *argv[])
{
    return CommandLineTestRunner::RunAllTests(argc, argv);
}
