#include "miko/memory/memory.hpp"
#include "miko/container/hash_map.hpp"
#include "miko/io/file_logger.hpp"

#include "miko/io/file_logger.hpp"
#include <CppUTest/TestHarness.h>
#include <CppUTest/CommandLineTestRunner.h>

using namespace miko;

TEST_GROUP(IntHashMap)
{
    void setup()
    {
        hashMap = new HashMap<int, int>(defaultAllocator);
    }

    void teardown()
    {
        delete hashMap;
    }

    HashMap<int, int> *hashMap;
};

TEST(IntHashMap, Create)
{
}

TEST(IntHashMap, Insert)
{
    CHECK(hashMap->set(1, 2));
}

TEST(IntHashMap, InsertMany)
{
    for (int i = 0; i < 100; i++) {
        CHECK(hashMap->set(i, i));
    }

    CHECK_EQUAL(hashMap->size(), 100);
}

TEST(IntHashMap, InsertRemove)
{
    for (int i = 0; i < 100; i++) {
        CHECK(hashMap->set(i, i));
    }

    CHECK_EQUAL(hashMap->size(), 100);

    for (int i = 0; i < 100; i++) {
        hashMap->remove(i);
    }

    CHECK_EQUAL(hashMap->size(), 0);
}

TEST(IntHashMap, InsertReplace)
{
    for (int i = 0; i < 100; i++) {
        CHECK(hashMap->set(i, i));
    }

    CHECK_EQUAL(hashMap->size(), 100);

    for (int i = 0; i < 100; i++) {
        CHECK(hashMap->set(i, i));
    }

    CHECK_EQUAL(hashMap->size(), 100);
}

TEST(IntHashMap, InsertReplaceRemove)
{
    for (int i = 0; i < 100; i++) {
        CHECK(hashMap->set(i, i));
    }

    for (int i = 0; i < 100; i++) {
        CHECK(hashMap->set(i, i));
    }

    for (int i = 0; i < 100; i++) {
        hashMap->remove(i);
    }

    CHECK_EQUAL(hashMap->size(), 0);
}

TEST(IntHashMap, RemoveRandom)
{
    for (int i = 0; i < 100; i++) {
        CHECK(hashMap->set(i, i));
    }

    for (int i = 0; i < 100; i += 8) {
        hashMap->remove(i);
    }

    for (int i = 0; i < 100; i += 5) {
        hashMap->remove(i);
    }

    for (int i = 0; i < 100; i += 3) {
        hashMap->remove(i);
    }

    for (int i = 0; i < 100; i += 1) {
        hashMap->remove(i);
    }

    CHECK_EQUAL(hashMap->size(), 0);
}

TEST(IntHashMap, Get)
{
    for (int i = 0; i < 100; i++) {
        CHECK(hashMap->set(i, i));
    }

    for (int i = 0; i < 100; i++) {
        int *tmp = 0;
        CHECK(hashMap->get(i, tmp));
        CHECK_EQUAL(*tmp, i);
    }
}

TEST(IntHashMap, Thousands)
{
    for (int i = 0; i < 6000; i++) {
        CHECK(hashMap->set(i, i));
    }
}

TEST(IntHashMap, Iterate)
{
    for (int i = 0; i < 100; i++) {
        CHECK(hashMap->set(i, i));
    }

    HashMap<int, int>::Iterator iter;

    iter = hashMap->begin();
    for (int i = 0; i < 100; i++) {
        CHECK(iter != hashMap->end());
        iter++;
    }

    CHECK(iter == hashMap->end());
}

TEST(IntHashMap, IterateRemove)
{
    for (int i = 0; i < 100; i++) {
        CHECK(hashMap->set(i, i));
    }

    {
        HashMap<int, int>::Iterator iter;
        iter = hashMap->begin();
        while (iter != hashMap->end()) {
            iter++;
        }
    }

    for (int i = 0; i < 100; i += 2) {
        hashMap->remove(i);
    }

    {
        HashMap<int, int>::Iterator iter;
        iter = hashMap->begin();
        while (iter != hashMap->end()) {
            iter++;
        }
    }
}

TEST(IntHashMap, IterateErase)
{
    for (int i = 0; i < 100; i++) {
        CHECK(hashMap->set(i, i));
    }

    HashMap<int, int>::Iterator iter = hashMap->begin();
    while (iter != hashMap->end()) {
        iter = hashMap->erase(iter);
    }

    CHECK_EQUAL(hashMap->size(), 0);
}

TEST(IntHashMap, Find)
{
    for (int i = 0; i < 100; i++) {
        CHECK(hashMap->set(i, i));
    }

    HashMap<int, int>::Iterator i = hashMap->find(66);
    CHECK(i != hashMap->end());
    CHECK_EQUAL(i->val, 66);

    i = hashMap->find(55);
    CHECK(i != hashMap->end());
    CHECK_EQUAL(i->val, 55);

    i = hashMap->find(0);
    CHECK(i != hashMap->end());
    CHECK_EQUAL(i->val, 0);

    i = hashMap->find(103);
    CHECK(i == hashMap->end());

    for (int i = 0; i < 100; i++) {
        CHECK(hashMap->find(i) != hashMap->end());
    }
}

TEST_GROUP(StringHashMap)
{
    void setup()
    {
        hashMap = new HashMap<std::string, std::string>(defaultAllocator);
    }

    void teardown()
    {
        delete hashMap;
    }

    HashMap<std::string, std::string> *hashMap;
};

TEST(StringHashMap, Create)
{
}

TEST(StringHashMap, Insert)
{
    CHECK(hashMap->set("foo", "bar"));
}

TEST(StringHashMap, InsertMany)
{
    for (int i = 0; i < 100; i++) {
        char tmp[32];
        snprintf(tmp, sizeof(tmp), "%d", i);
        CHECK(hashMap->set(tmp, tmp));
    }

    CHECK_EQUAL(hashMap->size(), 100);
}

TEST(StringHashMap, InsertRemove)
{
    for (int i = 0; i < 100; i++) {
        char tmp[32];
        snprintf(tmp, sizeof(tmp), "%d", i);
        CHECK(hashMap->set(tmp, tmp));
    }

    CHECK_EQUAL(hashMap->size(), 100);

    for (int i = 0; i < 100; i++) {
        char tmp[32];
        snprintf(tmp, sizeof(tmp), "%d", i);
        hashMap->remove(tmp);
    }

    CHECK_EQUAL(hashMap->size(), 0);
}

TEST(StringHashMap, InsertReplace)
{
    for (int i = 0; i < 100; i++) {
        char tmp[32];
        snprintf(tmp, sizeof(tmp), "%d", i);
        CHECK(hashMap->set(tmp, tmp));
    }

    CHECK_EQUAL(hashMap->size(), 100);

    for (int i = 0; i < 100; i++) {
        char tmp[32];
        snprintf(tmp, sizeof(tmp), "%d", i);
        CHECK(hashMap->set(tmp, tmp));
    }

    CHECK_EQUAL(hashMap->size(), 100);
}

TEST(StringHashMap, InsertReplaceRemove)
{
    for (int i = 0; i < 100; i++) {
        char tmp[32];
        snprintf(tmp, sizeof(tmp), "%d", i);
        CHECK(hashMap->set(tmp, tmp));
    }

    for (int i = 0; i < 100; i++) {
        char tmp[32];
        snprintf(tmp, sizeof(tmp), "%d", i);
        CHECK(hashMap->set(tmp, tmp));
    }

    for (int i = 0; i < 100; i++) {
        char tmp[32];
        snprintf(tmp, sizeof(tmp), "%d", i);
        hashMap->remove(tmp);
    }

    CHECK_EQUAL(hashMap->size(), 0);
}

int main(int argc, const char *argv[])
{
    FileLogger logger(stderr, false, true);
    logger.listen();

    return CommandLineTestRunner::RunAllTests(argc, argv);
}
