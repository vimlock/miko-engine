#include "miko/graphics/mesh.hpp"
#include "miko/io/text_serializer.hpp"
#include "miko/io/text_deserializer.hpp"
#include "miko/io/file_logger.hpp"

#include <CppUTest/TestHarness.h>
#include <CppUTest/CommandLineTestRunner.h>

using namespace miko;

TEST_GROUP(Basic)
{
};

TEST(Basic, ReadWriteEmpty)
{
}

TEST(Basic, ReadWritePlane)
{
}

int main(int argc, const char *argv[])
{
    FileLogger logger(stderr, false, true);
    logger.listen();

    return CommandLineTestRunner::RunAllTests(argc, argv);
}
