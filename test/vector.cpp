#include "miko/container/vector.hpp"
#include "miko/memory/memory.hpp"
#include "miko/io/file_logger.hpp"

#include <CppUTest/TestHarness.h>
#include <CppUTest/CommandLineTestRunner.h>

using namespace miko;

TEST_GROUP(IntVector)
{
    void setup()
    {
        vector = new Vector<int>(defaultAllocator);
    }

    void teardown()
    {
        delete vector;
    }

    Vector<int> *vector;
};

TEST(IntVector, Create)
{
}

TEST(IntVector, Reserve)
{
    CHECK(vector->reserve(1024));
    CHECK(vector->reserved() >= 1024);
}

TEST(IntVector, Append)
{
    for (int i = 0; i < 10; i++) {
        CHECK(vector->append(i));
    }

    CHECK_EQUAL(vector->size(), 10);
}

TEST(IntVector, Resize)
{
    for (int i = 0; i < 10; i++) {
        CHECK(vector->append(i));
    }

    CHECK_EQUAL(vector->size(), 10);

    CHECK_EQUAL(vector->resize(0), true);
    CHECK_EQUAL(vector->size(), 0);

    CHECK_EQUAL(vector->resize(10), true);
    CHECK_EQUAL(vector->size(), 10);

    CHECK_EQUAL(vector->resize(20), true);
    CHECK_EQUAL(vector->size(), 20);
}

TEST_GROUP(StringVector)
{
    void setup()
    {
        vector = new Vector<std::string>(defaultAllocator);
    }

    void teardown()
    {
        delete vector;
    }

    Vector<std::string> *vector;
};

TEST(StringVector, Create)
{
}

TEST(StringVector, Reserve)
{
    CHECK(vector->reserve(1024));
    CHECK(vector->reserved() >= 1024);
}

TEST(StringVector, Append)
{
    for (int i = 0; i < 10; i++) {
        char buf[32];
        snprintf(buf, sizeof(buf), "%d", i);
        CHECK(vector->append(buf));
    }

    CHECK_EQUAL(vector->size(), 10);
}

int main(int argc, const char *argv[])
{
    FileLogger logger(stderr, false, true);
    logger.listen();

    return CommandLineTestRunner::RunAllTests(argc, argv);
}
