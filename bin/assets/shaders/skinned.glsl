uniform float cTime = 0.0f;

uniform vec3 cEyePosition;
uniform vec3 cEyeDirection = vec3(0.0, 0.0, -1.0);
uniform vec3 cFogColor = vec3(0.5, 0.9, 0.5);

uniform vec3 cAmbient = vec3(0.0);

uniform vec4 cMatDiffuse = vec4(1.0);
uniform vec4 cMatSpecular = vec4(1.0);
uniform vec4 cMatEmissive = vec4(0.0);
uniform vec4 cMatAmbient = vec4(1.0);

uniform mat4 cProjection;
uniform mat4 cView;
uniform mat3 cNormalMatrix;
uniform mat4 cModelViewProjection;

uniform mat4 cBones[MIKO_MAX_BONES];

uniform sampler2D cTexture;

flat varying vec3 vColor;
smooth varying vec2 vTexcoord;
smooth varying vec3 vPosition;
flat varying vec3 vNormal;

flat varying vec3 vBoneWeight;
flat varying vec3 vBoneIndex;

/* ========================================================================== */
#ifdef MIKO_VERTEX_SHADER

in vec3 iPosition;
in vec2 iTexcoord;
in vec3 iNormal;
in vec3 iColor;
in vec4 iBoneIndex;
in vec4 iBoneWeight;

mat4 getBoneMatrix()
{
    ivec4 index = ivec4(iBoneIndex);

    float norm = 1.0 / (
        iBoneWeight[0] +
        iBoneWeight[1] +
        iBoneWeight[2] +
        iBoneWeight[3]);

    mat4 boneMatrix = cBones[index[0]] * (iBoneWeight[0] * norm);
    boneMatrix += cBones[index[1]] * (iBoneWeight[1] * norm);
    boneMatrix += cBones[index[2]] * (iBoneWeight[2] * norm);
    boneMatrix += cBones[index[3]] * (iBoneWeight[3] * norm);

    return boneMatrix;
}

void main()
{
    ivec4 index = ivec4(iBoneIndex);
    vec3 position = vec3(0.0, 0.0, 0.0);

    position += (cBones[index[0]] * vec4(iBoneWeight[0] * iPosition, 1.0)).xyz;
    position += (cBones[index[1]] * vec4(iBoneWeight[1] * iPosition, 1.0)).xyz;
    position += (cBones[index[2]] * vec4(iBoneWeight[2] * iPosition, 1.0)).xyz;
    position += (cBones[index[3]] * vec4(iBoneWeight[3] * iPosition, 1.0)).xyz;

    gl_Position = cModelViewProjection * vec4(position, 1.0);

    //vec4 normal = boneMatrix * vec4(iNormal, 0.0);
    //vNormal = cNormalMatrix * normal.xyz;

    int bone = 9;

    if (index[0] == bone) {
        vColor = vec3(1.0, 0.0, 0.0) * iBoneWeight[0];
    }
    else if (index[1] == bone) {
        vColor = vec3(1.0, 0.0, 0.0) * iBoneWeight[1];
    }
    else if (index[2] == bone) {
        vColor = vec3(1.0, 0.0, 0.0) * iBoneWeight[2];
    }
    else if (index[3] == bone) {
        vColor = vec3(1.0, 0.0, 0.0) * iBoneWeight[3];
    }
    else {
        vColor = vec3(0.2, 0.2, 0.2);
    }

    vTexcoord = iTexcoord;

    vBoneWeight = iBoneWeight.rgb;
    vBoneIndex = iBoneIndex.rgb;
}
#endif

/* ========================================================================== */
#ifdef MIKO_FRAGMENT_SHADER

out vec4 fColor;
out vec3 fNormal;

void main()
{
    fColor = vec4(vColor, 1.0);

    #if 0
    // the normal might not be in [0.1, 1.0] range because varying variables
    // are interpolated linearily
    vec3 normal = normalize(vNormal);

    // vec3 diffColor = texture2D(cTexture, vTexcoord).xyz;
    vec3 diffColor = vec3(0.5, 0.5, 0.5);
    //fColor = vBoneWeight.rgb;

    // calculate diffuse
    diffColor *= cMatDiffuse.rgb;
    float diffFactor = max(0.0, dot(cEyeDirection, normal));

    // calculate specular
    vec3 vertexToEye = normalize(vPosition - cEyePosition);
    vec3 lightReflect = normalize(reflect(cEyeDirection, normal));

    vec3 specColor = cMatSpecular.rgb;
    float specFactor = clamp(dot(vertexToEye, lightReflect), 0.0, 1.0);
    specFactor = pow(specFactor, 50 * cMatSpecular.a);

    fColor = vec4((diffColor * diffFactor) + (specFactor * specColor), 1.0f);
    fNormal = normal;

    // color = vec4(diffuse * intensity, 1.0);

    // color = vec4(vec3(0.9, 0.9, 0.9) * intensity, 1.0);
    // vec3 diffuse = texture2D(iTexture, vTexcoord).xyz;
    // color = vec4(mix(vNormal, diffuse, 0.5), 1.0);
    // color = vec4(1.0, 0.0, 0.0, 1.0);
    #endif
}

#endif
