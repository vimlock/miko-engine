
uniform float iTime = 0.0f;

uniform mat4 iModelViewProjection;

/* ========================================================================== */
#ifdef MIKO_VERTEX_SHADER

in vec3 iPosition;

void main()
{
    gl_Position = iModelViewProjection * vec4(iPosition, 1.0);
}

#endif

/* ========================================================================== */
#ifdef MIKO_FRAGMENT_SHADER

out vec3 fColor;

void main()
{
    fColor = vec3(1.0, 1.0, 0.5);
}

#endif
