uniform float cTime = 0.0f;

uniform vec3 cEyePosition;
uniform vec3 cEyeDirection = vec3(0.0, 0.0, -1.0);
uniform vec3 cFogColor = vec3(0.5, 0.9, 0.5);

uniform vec3 cAmbient = vec3(0.0);

uniform vec4 cMatDiffuse = vec4(1.0);
uniform vec4 cMatSpecular = vec4(1.0);
uniform vec4 cMatEmissive = vec4(0.0);
uniform vec4 cMatAmbient = vec4(1.0);

uniform mat3 cNormalMatrix;
uniform mat4 cModelViewProjection;

uniform mat4 cBones[MIKO_MAX_BONES];

uniform sampler2D cTexture;


smooth varying vec3 vColor;
smooth varying vec2 vTexcoord;
smooth varying vec3 vPosition;
flat varying vec3 vNormal;

/* ========================================================================== */
#ifdef MIKO_VERTEX_SHADER

in vec3 iPosition;
in vec2 iTexcoord;
in vec3 iNormal;
in vec3 iColor;

void main()
{
    gl_Position = cModelViewProjection * vec4(iPosition, 1.0);
    // gl_Position = vec4(iPosition * 0.3, 1.0);

    vPosition = gl_Position.xyz;
    vTexcoord = iTexcoord;
    vNormal = cNormalMatrix * iNormal;
    vColor = iColor;
}
#endif

/* ========================================================================== */
#ifdef MIKO_FRAGMENT_SHADER

out vec3 fColor;
out vec3 fNormal;

void main()
{
    // the normal might not be in [0.1, 1.0] range because varying variables
    // are interpolated linearily
    vec3 normal = normalize(vNormal);

    // vec3 diffColor = texture2D(cTexture, vTexcoord).xyz;
    vec3 diffColor = vec3(0.5, 0.5, 0.5);

    // calculate diffuse
    diffColor *= cMatDiffuse.rgb;
    float diffFactor = max(0.0, dot(cEyeDirection, normal));

    // calculate specular
    vec3 vertexToEye = normalize(vPosition - cEyePosition);
    vec3 lightReflect = normalize(reflect(cEyeDirection, normal));

    vec3 specColor = cMatSpecular.rgb;
    float specFactor = clamp(dot(vertexToEye, lightReflect), 0.0, 1.0);
    specFactor = pow(specFactor, 50 * cMatSpecular.a);

    fColor = (diffColor * diffFactor) + (specFactor * specColor);
    fNormal = normal;

    // color = vec4(diffuse * intensity, 1.0);

    // color = vec4(vec3(0.9, 0.9, 0.9) * intensity, 1.0);
    // vec3 diffuse = texture2D(iTexture, vTexcoord).xyz;
    // color = vec4(mix(vNormal, diffuse, 0.5), 1.0);
    // color = vec4(1.0, 0.0, 0.0, 1.0);
}

#endif
