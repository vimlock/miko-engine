varying vec2 vTexcoord;
varying vec2 vPosition;

uniform sampler2D iTexture;

/* ========================================================================== */
#ifdef MIKO_VERTEX_SHADER

in vec2 iPosition;
in vec2 iTexcoord;

void main()
{
    gl_Position = vec4(iPosition, 0.0, 1.0);
    vTexcoord = iTexcoord;
    vPosition = iPosition;
}

#endif


/* ========================================================================== */
#ifdef MIKO_FRAGMENT_SHADER

out vec4 color;

void main()
{
    //color = mix(texture(iTexture, vTexcoord),
        //iFogColor, vNormal.r);
    color = texture(iTexture, vTexcoord);

    // color = vec4(0.0, 0.0, 1.0, 1.0);
    // color = vec4(vPosition, length(vPosition), 1.0);
}

#endif
