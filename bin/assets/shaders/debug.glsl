
smooth varying vec4 vColor;

/* ========================================================================== */
#ifdef MIKO_VERTEX_SHADER

in vec3 iPosition;
in vec4 iColor;

void main()
{
    gl_Position = vec4(iPosition, 1.0);
    vColor = iColor;
}

#endif

/* ========================================================================== */
#ifdef MIKO_FRAGMENT_SHADER

out vec4 fColor;

void main()
{
    fColor = vColor;
}

#endif
