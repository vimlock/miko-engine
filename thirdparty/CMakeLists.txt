
set (STB_SOURCES
    "stb.c"
)

add_library (stb ${STB_SOURCES})

set (IMGUI_SOURCES
    imgui/imgui.cpp
    imgui/imgui_draw.cpp
    imgui/imgui_impl_sdl_gl3.cpp
)

add_library (imgui ${IMGUI_SOURCES})

target_link_libraries (imgui stb)
