
TODO
====

- Extend ResourceManager to allow directly accessing resources using find()

- Add GLEW to thirdparty libraries and link it staticly

- Remove GLU depencies, only thing we're currently relying on is gluErrorString()
  which is easily replacable.

- Implement profiling
    * Implement profiler to editor so that performance can be monitored in realtime

- Write missing documentation on many classes
    * Check the Doxygen generated docs to see which classes are missing documentation

- Write unit tests for certain classes
    * Lexer
    * TextSerializer
    * TextDeserializer
    * Signal + Slot

- Fix the allocator madness
    * Make certain allocators thread safe
    * Add virtual check that an allocator is thread safe

- Some classes require some serious renaming...
    * unserialize is not probably even a word. replace it with deserialize
    * ResourceManager
        - getDirect
        - loadHelper
        - etc...

Editor
    * Pattern editor
    * Sprite editor
        - 

Implement sprite rendering
    * Sprite batching
    * Sprite materials

Implement text rendering
    * font serialization + deserialization

Implement scenes
    * Scene serialization + deserialization
    * Scene rendering

Implement proper lighting
    * Light scene nodes
    * Check the shaders

Editor
    * Implement signals / slots

Implement particle systems
Implement render buckets

Implement material textures
Implement scripting - Wren vs Squirrel vs Lua
Implement custom String class

LATER
=====

- Implement automated Travis builds
    - Cross builds to Windows and Linux

- Windows builds
    - Verify that the build works on Windows

- Get rid of exceptions
    * Can't do that while we rely on std::string

- Fix the skeletal animation bug
    * Not important right now

- Implement proper asynchronous resource loading
    * Currently the resource loader should *in theory* support adding worker threads

DONE
====

## 2016-08-26

## 2016-08-25

- Implement asset browser
- Add default allocators to containers
- Update resource manager to include the resource locator which was used to find the asset
  when calling getDirEntries


## 2016-08-24
- Sprite animation serialization + deserialization

- Move most template code to .inl headers (refcounted.hpp)
- Remove requirement to pass buffer for token for TextDeserializer
- Make shader programs themselvels resources
- If shader is named .mkshader, load it differently
    - look for a sample in /shaders/test.mkshader
- Make GPU objects inherit from a common base e.g. GlObject
- Add MIKO\_API keyword for class definitions to allow dynamic linking on Windows

## 2016-08-17 ##
- Implement typeinfos on resources and templated resource get methods
- Implement ResourceHandle
- Make HashMap non-throwing
- Add MIKO\_WARN\_UNUSED macro which is added to functions which might fail
- Add more shader parameters
    - Quality, low - high
- Remove enum namespaces

- Move resource.hpp to resource directory


## 2016-08-15 ##

- Move the main repository to Bitbucket

Implement material loading
Remove sprite sheets
Verify that no constructor throws
Add support for skeleton loading
Implement render targets
Draw inside IMGUI window
IMGUI integration
Implement debug rendering
Implement material properties on renderer
Implement normal matrix for meshes
Verirify end token on asset parsers
Implement sprite loading

shader loading
texture loading
stb integration
unit test
serializer
