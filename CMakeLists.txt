
cmake_minimum_required (VERSION 2.6)
project (MikoEngine)

find_package (PkgConfig REQUIRED)
find_package (Doxygen)

set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)
set (CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)
set (CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)

include_directories (src)
include_directories (SYSTEM thirdparty)
include_directories (SYSTEM thirdparty/glm)

add_definitions (-Wall -Wextra)
add_definitions (-ggdb3 -O0)
add_definitions (-std=c++11)
# add_definitions (-DMIKO_UNITTEST)

enable_testing ()

add_custom_target (check COMMAND ${CMAKE_CTEST_COMMAND} --output-on-failure)

add_subdirectory (src/miko)
add_subdirectory (src/tools/asset-tool)
add_subdirectory (src/tools/editor)
add_subdirectory (src/tools/player)
add_subdirectory (test)

add_subdirectory (thirdparty)

if (DOXYGEN_FOUND)
    add_custom_target (doc
        ${DOXYGEN_EXECUTABLE} doc/Doxyfile
        ${CMAKE_CURRENT_BINARY_DIR}
    )

else (DOXYGEN_FOUND)
    message ("Doxygen not found on system, can't build documentation")
endif (DOXYGEN_FOUND)
