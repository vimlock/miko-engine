
#include <string.h>
#include "miko/io/file_logger.hpp"

#include "model_tool.hpp"

using namespace miko;

void usage(const char *name)
{
    fprintf(stderr, "Usage: %s command [options] args...\n", name);
    fprintf(stderr, "       %s command --help\n", name);
    fprintf(stderr, "       %s --help\n", name);
    fprintf(stderr, "\n");
    fprintf(stderr, "where command is any of\n");
    fprintf(stderr, "   model - convert a model file into .mkmodel\n");
}

int main(int argc, const char *argv[])
{
    FileLogger logger(stderr, false, true);
    logger.listen();

    const char *name;

    // might happen on bizarre cases where caller fucks up exec
    if (argc > 0) {
        name = argv[0];
    }
    else {
        name = "asset-tool";
    }

    if (argc <= 1) {
        usage(name);
        return 0;
    }

    const char * cmd = argv[1];
    if (strcmp(cmd, "model") == 0) {
        return ModelTool().main(argc - 2, &argv[2]);
    }
    else {
        usage(name);
        return 1;
    }

    return 0;
}
