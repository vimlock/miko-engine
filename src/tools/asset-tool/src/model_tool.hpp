#ifndef MIKO_MODEL_TOOL_HPP
#define MIKO_MODEL_TOOL_HPP

#include <SDL2/SDL_rwops.h>

struct aiNode;
struct aiMaterial;
struct aiScene;
struct aiMesh;
struct aiBone;
struct aiAnimation;
struct aiNodeAnim;

namespace miko
{

class SubMeshData;
class MeshData;
class Skeleton;

class ModelTool
{
public:
    ModelTool();

    int main(int argc, const char **argv);

private:
    void usage();

    void dump(const aiScene *scene);
    void dumpNode(int indent, const aiScene *scene, const aiNode *node);
    void dumpMesh(int indent, const aiScene *scene, const aiMesh *mesh);
    void dumpBone(int indent, const aiScene *scene, const aiBone *bone);
    void dumpMaterial(int indent, const aiScene *scene, const aiMaterial *material);
    void dumpAnimation(int indent, const aiScene *scene, const aiAnimation *anim);
    void dumpNodeAnim(int indent, const aiScene *scene, const aiNodeAnim *node);


};
    
} /* namespace miko */

#endif /* MIKO_MODEL_TOOL_HPP */

