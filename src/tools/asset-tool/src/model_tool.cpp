#include "model_tool.hpp"

#include "miko/container/list.hpp"

#include "miko/io/log.hpp"
#include "miko/io/filesys.hpp"
#include "miko/io/text_serializer.hpp"

#include "miko/graphics/animation.hpp"
#include "miko/graphics/mesh.hpp"
#include "miko/graphics/skeleton.hpp"

#include "miko/memory/memory.hpp"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/matrix_decompose.hpp>

namespace miko {

struct Options
{
    Options():
        generateNormals(false),
        generateTangents(false),

        optimize(false),

        writeSkeleton(true),
        writeMeshes(true),
        writeAnimations(false),
        writeMaterials(false),
        writeTextures(false)
    {
        // empty
    }

    bool generateNormals;
    bool generateTangents;

    bool optimize;

    bool writeSkeleton;
    bool writeMeshes;
    bool writeAnimations;
    bool writeMaterials;
    bool writeTextures;
};

bool process(const char *filepath, const Options &options);

bool processMeshes(const aiScene *scene, MeshData *out, const Options &options);
bool processMesh(const aiScene *scene, const aiMesh *mesh, MeshData &outMesh);

void processAnimation(const aiScene *scene, const aiAnimation *anim, SDL_RWops *rwops);
bool processAnimations(const aiScene *scene, const Options &options);
bool processMaterials(const aiScene *scene, const Options &options);

const aiNode * findRootBone(const aiNode *node, Skeleton *skeleton);

const aiBone * findBone(const aiScene *scene, const aiString &name);
const aiNode * findNode(const aiScene *scene, const aiString &name);

bool buildSkeleton(const aiScene *scene, Skeleton *skeleton);

void decompose(const aiMatrix4x4 &m, glm::vec3 &position, glm::quat &rotation);
void decompose(const aiMatrix4x4 &m, glm::vec3 &position, glm::quat &rotation, glm::vec3 &scale);

glm::vec3 toVec3(const aiVector3D &v);
glm::quat toQuat(const aiQuaternion &v);
glm::mat4 toMat4(const aiMatrix4x4 &v);

const aiBone * findBone(const aiScene *scene, const aiString &name)
{
    for (unsigned int i = 0; i < scene->mNumMeshes; i++) {
        for (unsigned int k = 0; k < scene->mMeshes[i]->mNumBones; k++) {
            if (scene->mMeshes[i]->mBones[k]->mName == name) {
                return scene->mMeshes[i]->mBones[k];
            }
        }
    }
    
    return NULL;
}

const aiNode * findNode(const aiScene *scene, const aiString &name)
{
    return scene->mRootNode->FindNode(name);
}

bool nodeHasBones(const aiScene *scene, const aiNode *node, bool recurse)
{
    for (unsigned int i = 0; i < node->mNumMeshes; i++) {
        if (scene->mMeshes[i]->mNumBones > 0) {
            return true;
        }
    }

    if (!recurse) {
        return false;
    }

    for (unsigned int i = 0; i < node->mNumChildren; i++) {
        if (nodeHasBones(scene, node->mChildren[i], true)) {
            return true;
        }
    }

    return false;
}


ModelTool::ModelTool()
{
}

void ModelTool::usage()
{
    fprintf(stderr, "options\n");
    fprintf(stderr, "  -a  --animations         Include animations\n");
    fprintf(stderr, "  -h  --help               Show this help\n");
    fprintf(stderr, "  -gn --generate-normals   Generate missing face normals\n");
    fprintf(stderr, "  -gt --generate-tangents  Generate missing tangent normals\n");
    fprintf(stderr, "  -m  --materials          Include materials\n");
    fprintf(stderr, "  -o  --optimize           Apply Assimp mesh optimizations\n");
    fprintf(stderr, "  -t  --textures           Include textures\n");
    fprintf(stderr, "  -nm --no-mesh            Do not include mesh\n");
    fprintf(stderr, "  -ns --no-skeleton        Do not include skeleton in the mesh\n");

}

bool option(const char *arg, const char *s, const char *l)
{
    return strcmp(arg, s) == 0 || strcmp(arg, l) == 0;
}

int ModelTool::main(int argc, const char **argv)
{
    const char *target = NULL;

    Options options;

    for (int i = 0; i < argc; i++) {
        const char *arg = argv[i];

        if (option(arg, "-a", "--anims")) {
            options.writeAnimations = true;
        }
        else if (option(arg, "-h", "--help")) {
            usage();
            return 0;
        }
        else if (option(arg, "-gn", "--generate-normals")) {
            options.generateNormals = true;
        }
        else if (option(arg, "-gt", "--generate-tangents")) {
            options.generateTangents = true;
        }
        else if (option(arg, "-m", "--materials")) {
            options.writeTextures = true;
        }
        else if (option(arg, "-ns", "--no-skeleton")) {
            options.writeSkeleton = false;
        }
        else if (option(arg, "-nm", "--no-mesh")) {
            options.writeMeshes = false;
        }
        else if (option(arg, "-o", "--optimize")) {
            options.optimize = true;
        }
        else if (option(arg, "-t", "--textures")) {
            options.writeTextures = true;
        }
        else {
            if (!target) {
                target = argv[i];
            }
            else {
                LOG_ERROR("too many arguments");
                return 1;
            }
        }
    }

    if (!target) {
        LOG_ERROR("no argument given");
        return -1;
    }

    process(target, options);
    
    return 0;
}

bool process(const char *filepath, const Options &options)
{
    Assimp::Importer importer;

    unsigned int flags = 
        aiProcess_Triangulate           |
        aiProcess_LimitBoneWeights      |
        aiProcess_SortByPType           ;


    if (options.generateNormals) {
        flags |= aiProcess_GenSmoothNormals;
    }

    if (options.generateTangents) {
        flags |= aiProcess_CalcTangentSpace;
    }

    if (options.optimize) {
        flags |= aiProcess_JoinIdenticalVertices |
                 // aiProcess_OptimizeMeshes          |
                 // aiProcess_SplitByBoneCount        |
                 aiProcess_ImproveCacheLocality  ;
    }

    const aiScene *scene = importer.ReadFile(filepath, flags);
    if (!scene) {
        LOG_ERRORF("failed to load file %s: %s", filepath, importer.GetErrorString());
        return false;
    }

    glm::mat4 m = toMat4(scene->mRootNode->mTransformation);
    for (unsigned int i = 0; i < 4; i++) {
        LOG_DEBUGF("%f %f %f %f",
            m[i][0],
            m[i][1],
            m[i][2],
            m[i][3]
        );
    }

    if (options.writeMeshes) {

        MeshData out(defaultAllocator);

        bool haveBones = false;

        // If we have bones, we need to build the skeleton before parsing the vertex data
        for (unsigned int i = 0; i < scene->mNumMeshes; i++) {
            if (scene->mMeshes[i]->mNumBones > 0) {
                haveBones = true;
                break;
            }
        }

        // If we have bones and skeleton is not explicitly omitted, build the the skeleton
        if (haveBones && options.writeSkeleton) {
            Skeleton *skeleton = out.addSkeleton();
            if (!skeleton) {
                return false;
            }

            if (!buildSkeleton(scene, skeleton)) {
                return false;
            }
        }

        processMeshes(scene, &out, options);

        std::string outpath = fileNameWithoutExtension(filepath) + ".mkmesh";
        SDL_RWops *rwops = SDL_RWFromFile(outpath.c_str(), "wb");
        if (!rwops) {
            LOG_ERRORF("failed to open file '%s' for writing: %s", outpath.c_str(), SDL_GetError());
            return false;
        }

        LOG_INFOF("writing mesh to %s", outpath.c_str());
        TextSerializer serializer(rwops, true);

        out.serialize(serializer);
        SDL_RWclose(rwops);
    }

    if (options.writeAnimations) {
        for (unsigned int i = 0; i < scene->mNumAnimations; i++) {
            const aiAnimation *anim = scene->mAnimations[i];

            std::string name(anim->mName.C_Str());
            if (name.empty()) {
                char buf[32];
                snprintf(buf, sizeof(buf), "anim_%u", i);
                name = buf;
            }

            std::string outpath = fileNameWithoutExtension(filepath) + "_" + name + ".mkanim";

            SDL_RWops *rwops = SDL_RWFromFile(outpath.c_str(), "wb");
            if (!rwops) {
                LOG_ERRORF("failed to open file '%s' for writing: %s", outpath.c_str(), SDL_GetError());
                continue;
            }

            LOG_INFOF("writing animation '%s' to %s", anim->mName.C_Str(), outpath.c_str());
            processAnimation(scene, anim, rwops);
            SDL_RWclose(rwops);
        }
    }

    return true;
}

bool nodeContainsMesh(const aiNode *node, unsigned int meshIndex)
{
    for (unsigned int i = 0; i < node->mNumMeshes; i++) {
        if (node->mMeshes[i] == meshIndex) {
            return true;
        }
    }

    return false;
}

unsigned nodeDistanceToRoot(const aiNode *node)
{
    unsigned int n = 0;
    while (node->mParent) {
        node = node->mParent;
        n++;
    }

    return n;
}

void addBoneNode(const aiScene *scene, const aiNode *node, Skeleton *skeleton)
{
    int parent = skeleton->getBoneIndex(node->mParent->mName.C_Str());
    Bone *bone = skeleton->addBone(node->mName.C_Str(), parent);
    if (!bone) {
        return;
    }

    glm::quat rotation;
    glm::vec3 position;

    decompose(node->mTransformation, position, rotation);
    bone->position = position;
    bone->rotation = rotation;

    const aiBone *boneNode = findBone(scene, node->mName);
    if (!boneNode) {
        LOG_ERRORF("missing bone '%s'", node->mName.C_Str());
        bone->offsetMatrix = toMat4(node->mTransformation);
    }
    else {
        bone->offsetMatrix = toMat4(boneNode->mOffsetMatrix);
    }

    for (unsigned int i = 0; i < node->mNumChildren; i++) {
        addBoneNode(scene, node->mChildren[i], skeleton);
    }
}

bool buildSkeleton(const aiScene *scene, Skeleton *skeleton)
{
    Vector<const aiNode *> boneNodes(defaultAllocator);

    // collect bone nodes
    for (unsigned int i = 0; i < scene->mNumMeshes; i++) {
        int meshIndex = i;
        const aiMesh *mesh = scene->mMeshes[meshIndex];

        for (unsigned int k = 0; k < mesh->mNumBones; k++) {
            const aiBone *bone = mesh->mBones[k];

            const aiNode *node = findNode(scene, bone->mName);
            if (!node) {
                LOG_WARNF("bone %s does not have a scene node", bone->mName.C_Str());
            }

            while (node) {
                // dont add root node
                if (!node->mParent) {
                    break;
                }
                // dont add duplicates
                else if (boneNodes.contains(node)) {
                    break;
                }
                else {
                    boneNodes.append(node);
                    node = node->mParent;
                }

                if (nodeContainsMesh(node, meshIndex)) {
                    break;
                }
            }
        }
    }

    // no bones?
    if (boneNodes.size() == 0) {
        return true;
    }

    const aiNode *bestNode = NULL;
    int bestDistance = INT_MAX;

    // find the closest bone to root
    for (unsigned int i = 0; i < boneNodes.size(); i++) {
        const aiNode *n = boneNodes[i];
        int d = nodeDistanceToRoot(n);

        if (d < bestDistance && d != 0) {
            bestDistance = d;
            bestNode = n;
        }
    }

    LOG_INFOF("using '%s' as root bone", bestNode->mName.C_Str());

    addBoneNode(scene, bestNode, skeleton);
    return true;
}

bool processMeshes(const aiScene *scene, MeshData *out, const Options &options)
{
    for (unsigned i = 0; i < scene->mNumMeshes; i++) {
        aiMesh *mesh = scene->mMeshes[i];

        if (!processMesh(scene, mesh, *out)) {
            return false;
        }

    }

    return true;
}

static void writeBytes(Vector<char> &v, const void *data, unsigned size)
{
    unsigned offset = v.size();
    v.resize(v.size() + size);
    memcpy(&v[offset], data, size);
}

bool processMesh(const aiScene *scene, const aiMesh *mesh, MeshData &outMesh)
{
    (void) scene;

    SubMeshData *submesh = outMesh.addSubmesh();
    if (!submesh) {
        return false;
    }

    Vector<char> &vertices = submesh->vertices;
    Vector<uint16_t> &indices = submesh->indices;
    Vector<GeometryData> &geometries = submesh->geometries;
    VertexFormat &format = submesh->vertexFormat;

    mikoAssert(format.vertexSize() == 0);

    if (mesh->mVertices) {
        format.addComponent(COMPONENT_POSITION, COMPONENT_VEC3);
    }

    if (mesh->mNormals) {
        format.addComponent(COMPONENT_NORMAL, COMPONENT_VEC3);
    }

    if (mesh->mTangents) {
        format.addComponent(COMPONENT_TANGENT, COMPONENT_VEC3);
    }

    if (mesh->mColors[0]) {
        format.addComponent(COMPONENT_COLOR, COMPONENT_VEC4);
    }

    if (mesh->mTextureCoords[0]) {
        format.addComponent(COMPONENT_TEXCOORD, COMPONENT_VEC3);
    }

    if (mesh->mNumBones) {
        format.addComponent(COMPONENT_BONE_WEIGHT, COMPONENT_VEC4);
        format.addComponent(COMPONENT_BONE_INDEX, COMPONENT_VEC4);
    }

    // avoid too many allocations
    vertices.reserve(format.vertexSize() * mesh->mNumVertices);
    indices.reserve(mesh->mNumFaces * 3);

    // read vertices
    for (unsigned int i = 0; i < mesh->mNumVertices; i++) {

        // important that we add the vertices in same order as they are defined in the vertex format
        
        if (mesh->mVertices) {
            const aiVector3D *tmp = &mesh->mVertices[i];
            glm::vec3 position(tmp->x, tmp->y, tmp->z);
            writeBytes(vertices, &position, sizeof(position));
        }

        if (mesh->mNormals) {
            const aiVector3D *tmp = &mesh->mNormals[i];
            glm::vec3 normal(tmp->x, tmp->y, tmp->z);
            writeBytes(vertices, &normal, sizeof(normal));
        }

        if (mesh->mTangents) {
            const aiVector3D *tmp = &mesh->mTangents[i];
            glm::vec3 tangent(tmp->x, tmp->y, tmp->z);
            writeBytes(vertices, &tangent, sizeof(tangent));
        }

        if (mesh->mColors[0]) {
            const aiColor4D *tmp = &mesh->mColors[0][i];
            glm::vec4 color(tmp->r, tmp->b, tmp->g, tmp->a);
            writeBytes(vertices, &color, sizeof(color));
        }

        if (mesh->mTextureCoords[0]) {
            const aiVector3D *tmp = &mesh->mTextureCoords[0][i];
            glm::vec3 texcoord(tmp->x, tmp->y, tmp->z);
            writeBytes(vertices, &texcoord, sizeof(texcoord));
        }

        // If we have bones, add placeholder bone weights + indices which we will fill up later
        if (mesh->mNumBones > 0) {
            glm::vec4 indices(0.0, 0.0, 0.0, 0.0);
            glm::vec4 weights(0.0, 0.0, 0.0, 0.0);
            writeBytes(vertices, &indices, sizeof(indices));
            writeBytes(vertices, &weights, sizeof(weights));
        }
    }


    // read indices
    for (unsigned i = 0; i < mesh->mNumFaces; i++) {
        const aiFace *face = &mesh->mFaces[i];

        // should be triangulated for us
        mikoAssert(face->mNumIndices == 3);

        indices.append(face->mIndices[0]);
        indices.append(face->mIndices[1]);
        indices.append(face->mIndices[2]);
    }

    geometries.append(GeometryData("", 0, indices.size()));

    mikoAssert(vertices.size() == format.vertexSize() * mesh->mNumVertices);

    // read bones 
    if (mesh->mNumBones == 0) {
        return true;
    }

    Skeleton *skeleton = outMesh.getSkeleton();

    Vector<unsigned int> assignedWeights(defaultAllocator);
    assignedWeights.reserve(mesh->mNumVertices);
    for (unsigned int i = 0; i < mesh->mNumVertices; i++) {
        assignedWeights.append(0);
    }

    int weightOffset = format.componentOffset(COMPONENT_BONE_WEIGHT);
    int indexOffset = format.componentOffset(COMPONENT_BONE_INDEX);

    for (unsigned int i = 0; i < mesh->mNumBones; i++) {

        const aiBone *meshBone = mesh->mBones[i];
        std::string name = meshBone->mName.C_Str();

        int boneIndex = skeleton->getBoneIndex(name);
        if (boneIndex < 0) {
            LOG_WARNF("bone '%s' does not exist", name.c_str());
            continue;
        }

        for (unsigned int k = 0; k < meshBone->mNumWeights; k++) {
            unsigned vertexIndex = meshBone->mWeights[k].mVertexId;
            float vertexWeight = meshBone->mWeights[k].mWeight;

            mikoAssert(vertexIndex < assignedWeights.size());
            
            unsigned int numAssignedWeights = assignedWeights[vertexIndex];

            if (numAssignedWeights >= 4) {
                LOG_ERRORF("too many weights on vertex %u", vertexIndex);
                continue;
            }

            glm::vec4 *indices = reinterpret_cast<glm::vec4 *>(
                &vertices[vertexIndex * format.vertexSize() + indexOffset]);
            glm::vec4 *weights = reinterpret_cast<glm::vec4 *>(
                &vertices[vertexIndex * format.vertexSize() + weightOffset]);

            (*indices)[numAssignedWeights] = boneIndex;
            (*weights)[numAssignedWeights] = vertexWeight;

            assignedWeights[vertexIndex]++;
        }
    }


    unsigned int numUnassigned = 0;

    for (unsigned int i = 0 ; i < assignedWeights.size(); i++) {
        if (assignedWeights[i] == 0) {
            numUnassigned++;
        }
    }

    if (numUnassigned > 0) {
        LOG_WARNF("%u vertices without bones assigned", numUnassigned);
    }

    return true;
}

void processAnimation(const aiScene *scene, const aiAnimation *anim, SDL_RWops *rwops)
{
    (void) scene;

    TextSerializer s(rwops, true);
    Ref<SkeletonAnimation> out(MIKO_NEW(defaultAllocator, SkeletonAnimation)(defaultAllocator));
    if (!out) {
        return;
    }

    out->speed = anim->mTicksPerSecond;
    out->duration = anim->mDuration;

    for (unsigned int i = 0; i < anim->mNumChannels; i++) {
        const aiNodeAnim *anode = anim->mChannels[i];

        if (anode->mNumPositionKeys == 0 ||
                anode->mNumRotationKeys == 0 ||
                anode->mNumScalingKeys  == 0) {
            LOG_WARNF("ignoring channel '%s' on animation '%s' with missing keyframes",
                anim->mName.C_Str(), anode->mNodeName.C_Str());
            continue;
        }

        if (anode->mNumPositionKeys != anode->mNumRotationKeys ||
                anode->mNumPositionKeys != anode->mNumScalingKeys  ||
                anode->mNumRotationKeys != anode->mNumScalingKeys) {

            LOG_WARNF("ignoring channel '%s' on animation '%s', with varying number of keyframes per channel",
                anim->mName.C_Str(), anode->mNodeName.C_Str());
            continue;
        }

        SkeletonAnimationChannel *channel = out->addChannel();
        if (!channel) {
            return;
        }

        channel->name = anode->mNodeName.C_Str();

        // we should have the same number of keyframes in each channel by now
        for (unsigned int k = 0; k < anode->mNumPositionKeys; k++) {
            const aiVectorKey &position = anode->mPositionKeys[k];
            const aiQuatKey &rotation = anode->mRotationKeys[k];
            const aiVectorKey &scale = anode->mScalingKeys[k];

            // test if the time values are OK
            
            const float epsilon = 0.001f;
            if (fabs(position.mTime - rotation.mTime) > epsilon ||
                    fabs(position.mTime - scale.mTime) > epsilon ||
                    fabs(rotation.mTime - scale.mTime) > epsilon) {
                LOG_WARNF("bad keyframe %u on animation '%s' channel '%s'",
                    i, anim->mName.C_Str(), anode->mNodeName.C_Str());
                continue;
            }

            SkeletonAnimationFrame *keyframe = channel->addFrame();
            keyframe->time = position.mTime;
            keyframe->position = glm::vec3(position.mValue.x, position.mValue.y, position.mValue.z);
            keyframe->rotation = glm::quat(rotation.mValue.w, rotation.mValue.x, rotation.mValue.y, rotation.mValue.z);
        }
    }

    out->serialize(s);
}

void processMaterial(const aiScene *scene, const aiMaterial *material, SDL_RWops *rwops)
{
    (void) scene;
    (void) material;
    (void) rwops;
}

void ModelTool::dump(const aiScene *scene)
{
    LOG_INFOF("%d meshes", scene->mNumMeshes);
    LOG_INFOF("%d materials", scene->mNumMaterials);
    LOG_INFOF("%d animations", scene->mNumAnimations);
    LOG_INFOF("%d cameras", scene->mNumCameras);
    LOG_INFOF("%d lights", scene->mNumLights);
    LOG_INFOF("%d textures", scene->mNumTextures);

    dumpNode(0, scene, scene->mRootNode);

    for (unsigned int i = 0; i < scene->mNumMaterials; i++) {
        dumpMaterial(0, scene, scene->mMaterials[i]);
    }

    for (unsigned int i = 0; i < scene->mNumAnimations; i++) {
        dumpAnimation(0, scene, scene->mAnimations[i]);
    }
}

void ModelTool::dumpNode(int indent, const aiScene *scene, const aiNode *node)
{
    LOG_INFOF("%*snode %s (%u children) (%u meshes)",
        indent, "", node->mName.C_Str(),
        node->mNumChildren,
        node->mNumMeshes
    );

    for (unsigned i = 0; i < node->mNumMeshes; i++) {
        LOG_INFOF("%*smesh %d", indent, "", i);
        dumpMesh(indent + 1, scene, scene->mMeshes[node->mMeshes[i]]);
    }

    for (unsigned int i = 0; i < node->mNumChildren; i++) {
        dumpNode(indent + 1, scene, node->mChildren[i]);
    }
}

void ModelTool::dumpMesh(int indent, const aiScene *scene, const aiMesh *mesh)
{
    LOG_INFOF("%*s- %u faces", indent, "", mesh->mNumFaces);
    LOG_INFOF("%*s- %u vertices", indent, "", mesh->mNumVertices);
    LOG_INFOF("%*s- %u bones", indent, "", mesh->mNumBones);

    unsigned int numColorChannels = 0;
    for (unsigned int i = 0; i < AI_MAX_NUMBER_OF_COLOR_SETS; i++) {
        if (mesh->mColors[i]) {
            numColorChannels++;
        }
    }

    LOG_INFOF("%*s- %u color channels", indent, "", numColorChannels);

    unsigned int numUVSets = 0;
    for (unsigned int i = 0; i < AI_MAX_NUMBER_OF_TEXTURECOORDS; i++) {
        if (mesh->mTextureCoords[i]) {
            numUVSets++;
        }
    }

    LOG_INFOF("%*s- %u uv sets", indent, "", numUVSets);

    if (mesh->mBitangents) {
        LOG_INFOF("%*s- bitangents", indent, "");
    }

    if (mesh->mNormals) {
        LOG_INFOF("%*s- normals", indent, "");
    }

    if (mesh->mTangents) {
        LOG_INFOF("%*s- tangents", indent, "");
    }

    for (unsigned i = 0; i < mesh->mNumBones; i++) {
        dumpBone(indent + 1, scene, mesh->mBones[i]);
    }
}

void ModelTool::dumpBone(int indent, const aiScene *scene, const aiBone *bone)
{
    (void) scene;

    LOG_INFOF("%*s bone %s (%u weights)", indent, "", bone->mName.C_Str(), bone->mNumWeights);
}

void ModelTool::dumpMaterial(int indent, const aiScene *scene, const aiMaterial *material)
{
    (void) indent;
    (void) scene;

    LOG_INFOF(" - %u material properties", material->mNumProperties);

    aiString name;
    material->Get(AI_MATKEY_NAME, name);

    LOG_INFOF(" - material %s", name.C_Str());
    
    aiColor3D diffuse, specular, ambient, emissive, transparent;
    material->Get(AI_MATKEY_COLOR_DIFFUSE, diffuse);
    material->Get(AI_MATKEY_COLOR_SPECULAR, specular);
    material->Get(AI_MATKEY_COLOR_AMBIENT, ambient);
    material->Get(AI_MATKEY_COLOR_EMISSIVE, emissive);
    material->Get(AI_MATKEY_COLOR_TRANSPARENT, transparent);

    LOG_INFOF(" - diffuse  %f %f %f", diffuse.r, diffuse.g, diffuse.b);
    LOG_INFOF(" - specular %f %f %f", specular.r, specular.g, specular.b);
    LOG_INFOF(" - ambient  %f %f %f", ambient.r, ambient.g, ambient.b);
    LOG_INFOF(" - emissive %f %f %f", emissive.r, emissive.g, emissive.b);

    int wireframe, twosided, shading, blend;
    material->Get(AI_MATKEY_ENABLE_WIREFRAME, wireframe);
    material->Get(AI_MATKEY_TWOSIDED, twosided);
    material->Get(AI_MATKEY_SHADING_MODEL, shading);
    material->Get(AI_MATKEY_BLEND_FUNC, blend);

    LOG_INFOF(" - wireframe %d", wireframe);
    LOG_INFOF(" - twosided  %d", twosided);
    LOG_INFOF(" - shading   %d", shading);
    LOG_INFOF(" - blend     %d", blend);
}

void ModelTool::dumpAnimation(int indent, const aiScene *scene, const aiAnimation *anim)
{
    LOG_INFOF("%*sanimation '%s'", indent, "", anim->mName.C_Str());
    LOG_INFOF("%*s - duration %f", indent, "", anim->mDuration);
    LOG_INFOF("%*s - speed    %f", indent, "", anim->mTicksPerSecond);
    LOG_INFOF("%*s - bone channels %d", indent, "", anim->mNumChannels);
    LOG_INFOF("%*s - mesh channels %d", indent, "", anim->mNumMeshChannels);

    for (unsigned int i = 0; i < anim->mNumChannels; i++) {
        dumpNodeAnim(indent + 1, scene, anim->mChannels[i]);
    }
}

void ModelTool::dumpNodeAnim(int indent, const aiScene *scene, const aiNodeAnim *node)
{
    (void) scene;

    LOG_INFOF("%*sanimation node '%s'", indent, "", node->mNodeName.C_Str());

    LOG_INFOF("%*s - position keys %u", indent, "", node->mNumPositionKeys);
    indent++;
    for (unsigned int i = 0; i < node->mNumPositionKeys; i++) {
        const aiVectorKey &key = node->mPositionKeys[i];
        LOG_INFOF("%*s key %u time %f, position %f %f %f", indent, "", i, key.mTime, key.mValue.x, key.mValue.y, key.mValue.z);
    }
    indent--;

    LOG_INFOF("%*s - rotation keys %u", indent, "", node->mNumRotationKeys);
    indent++;
    for (unsigned int i = 0; i < node->mNumRotationKeys; i++) {
        LOG_INFOF("%*s rotation key %u time %f", indent, "", i, node->mRotationKeys[i].mTime);
    }
    indent--;

    LOG_INFOF("%*s - scaling  keys %u", indent, "", node->mNumScalingKeys);
    indent++;
    for (unsigned int i = 0; i < node->mNumScalingKeys; i++) {
        LOG_INFOF("%*s scaling key %u time %f", indent, "", i, node->mScalingKeys[i].mTime);
    }
    indent--;
}

void decompose(const aiMatrix4x4 &m, glm::vec3 &outPosition, glm::quat &outRotation, glm::vec3 &outScale)
{
    aiVector3D position;
    aiQuaternion rotation;
    aiVector3D scale;

    m.Decompose(scale, rotation, position);

    outPosition = toVec3(position);
    outRotation = toQuat(rotation);
    outScale    = toVec3(scale);
}

void decompose(const aiMatrix4x4 &m, glm::vec3 &outPosition, glm::quat &outRotation)
{
    aiVector3D position;
    aiQuaternion rotation;

    m.DecomposeNoScaling(rotation, position);

    outPosition = toVec3(position);
    outRotation = toQuat(rotation);
}

glm::vec3 toVec3(const aiVector3D &v)
{
    return glm::vec3(v.x, v.y, v.z);
}

glm::quat toQuat(const aiQuaternion &v)
{
    return glm::quat(v.w, v.x, v.y, v.z);
}

// let's not make guesses about the assimp matrices memory layout
glm::mat4 toMat4(const aiMatrix4x4 &v)
{
    glm::mat4 m;

    m[0][0] = v.a1; m[1][0] = v.a2; m[2][0] = v.a3; m[3][0] = v.a4;
    m[0][1] = v.b1; m[1][1] = v.b2; m[2][1] = v.b3; m[3][1] = v.b4;
    m[0][2] = v.c1; m[1][2] = v.c2; m[2][2] = v.c3; m[3][2] = v.c4;
    m[0][3] = v.d1; m[1][3] = v.d2; m[2][3] = v.d3; m[3][3] = v.d4;

    return m;
}

    
} /* namespace miko */
