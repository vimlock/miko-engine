
pkg_search_module (ASSIMP REQUIRED assimp)

set (ASSET_TOOL_SOURCES
    "src/main.cpp"
    "src/model_tool.cpp"
)

add_definitions (
    ${ASSIMP_CFLAGS}
)

include_directories (
    ${ASSIMP_INCLUDE_DIRS}
)

add_executable (asset-tool ${ASSET_TOOL_SOURCES})
target_link_libraries (asset-tool
    ${ASSIMP_LIBRARIES}
    miko
)
