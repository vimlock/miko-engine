#ifndef MIKO_EDITOR_FILE_SELECT_HPP
#define MIKO_EDITOR_FILE_SELECT_HPP

#include <string>
#include "miko/container/vector.hpp"

#include "editor.hpp"

namespace miko {

enum FileSelectMode {
    FILE_READ,
    FILE_WRITE
};

enum FileType {
    /// File is a "regular" file
    FILE_REGULAR,

    /// File is a directory
    FILE_DIRECTORY,

    /// File is something else, socket, device, etc.
    FILE_OTHER
};

struct FileInfo
{
    FileType type;

    std::string path;
    std::string name;

    /// Size of the file (in bytes)
    uint32_t size;
};

enum FileSortMode {
    FILE_SIZE,
    FILE_NAME
};

struct FileCancelEvent : public EditorEvent
{
    FileCancelEvent():
        EditorEvent(EVENT_FILE_SELECT_CANCELED)
    {
        // empty
    }
};

struct FileSelectEvent : public EditorEvent
{
    FileSelectEvent(const std::string _filepath):
        EditorEvent(EVENT_FILE_SELECT_SELECTED),
        filepath(_filepath)
    {
        // empty
    }

    std::string filepath;
};

class FileSelector : public Editor
{
public:
    FileSelector(EditorContext *context);

    /// Sets the the given editor as listener
    /// @remarks if there is already a listener, false is returned
    bool addListener(Editor *listener, int event);

    /// Removes the given editor from listeners
    void removeListener(Editor *listener, int event);

    /// Removes all listeners
    void removeAllListeners();

    bool open(const std::string &path, FileSelectMode mode);

protected:
    virtual void handleGuiUpdate(float timePassed);

private:
    void cancel();

    /// Notifies our listener that we have selected the file
    void signalSelect(const std::string &filepath);

    /// Notifies our listener that user canceled selecting the file
    void signalCancel();

    FileSelectMode selectMode;
    FileSortMode sortMode;

    bool showDotfiles;

    Editor *listener;
    int userdata;

    /// Files infos of the current directory
    Vector<FileInfo> files;

    /// Invidual path chunks of the path
    Vector<std::string> pathChunks;

    /// The path chunks joined
    std::string path;

    /// Current file candidate
    char name[512];
};
    
} /* namespace miko */

#endif /* MIKO_EDITOR_FILE_SELECT_HPP */

