#ifndef MIKO_EDITOR_MAIN_HPP
#define MIKO_EDITOR_MAIN_HPP

#include "miko/core/context.hpp"

#include "asset_browser.hpp"
#include "sprite_editor.hpp"
#include "file_selector.hpp"

namespace miko {

class Context;

class EditorContext
{
public:
    EditorContext(Context *context);

    void update(float timePassed);

    void quit();
    bool shouldQuit() const;

    ResourceManager * getResourceManager()
    {
        return context->getResourceManager();
    }

    FileSelector * getFileSelector()
    {
        return &fileSelector;
    }

    AssetBrowser * getAssetBrowser()
    {
        return &assetBrowser;
    }

private:
    void menubar();

    bool quitFlag;
    Context *context;

    FileSelector fileSelector;
    AssetBrowser assetBrowser;
    SpriteEditor spriteEditor;
};
    
} /* namespace miko */

#endif /* MIKO_EDITOR_MAIN_HPP */

