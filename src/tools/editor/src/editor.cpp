#include "editor.hpp"

#include <imgui/imgui.h>
#include <stdio.h>

namespace miko {

Editor::Editor(EditorContext *_context, const char *_name):
    context(_context),
    name(_name),
    shown(false)
{
    // empty
}

void Editor::show()
{
    if (!shown) {
        shown = true;
        handleShow();
    }
}

void Editor::hide()
{
    if (shown) {
        shown = false;
        handleHide();
    }
}

bool Editor::isShown() const
{
    return shown;
}

void Editor::update(float timePassed)
{
    handleUpdate(timePassed);

    if (shown) {
        if (ImGui::Begin(name)) {
            handleGuiUpdate(timePassed);
        }

        ImGui::End();
    }
}

void Editor::handleUpdate(float)
{
    // Do nothing
}

void Editor::handleGuiUpdate(float)
{
    // Do nothing
}

void Editor::handleEvent(const EditorEvent *event, int userdata)
{
    // Do nothing
}

void formatFileSize(int bufSize, char *buf, uint32_t numBytes)
{
    const char *symbol;
    float size;

    uint32_t kb = 1024;
    uint32_t mb = kb * 1024;
    uint32_t gb = mb * 1024;

    // Let's hope that we don't have to deal with a terabyte sized file...
    if (numBytes < kb) {
        symbol = "B";
        size = numBytes;
    }
    else if (numBytes < mb) {
        symbol = "KB";
        size = numBytes / kb;
    }
    /// Let's format files larger than 100MB as gigabytes
    else if (numBytes < mb * 100) {
        symbol = "MB";
        size = numBytes / mb;
    }
    else {
        symbol = "GB";
        size = numBytes / gb;
    }

    snprintf(buf, bufSize, "%.2f%s", size, symbol);
}

} /* namespace miko */
