#ifndef MIKO_EDITOR_EDITOR_HPP
#define MIKO_EDITOR_EDITOR_HPP

#include "miko/miko.hpp"

namespace miko {

class EditorContext;

enum EditorEventType
{
    EVENT_FILE_SELECT_SELECTED,
    EVENT_FILE_SELECT_CANCELED,

    EVENT_ASSET_SELECT_SELECTED,
    EVENT_ASSET_SELECT_CANCELLED
};

struct EditorEvent
{
    EditorEvent(int _type):
        type(_type)
    {
        // empty
    }

    int type;
};

/// Base class for all other editors, SpriteEditor, PatternEditor, etc.
class Editor
{
public:
    Editor(EditorContext *context, const char *name);

    virtual ~Editor()
    {
        // empty
    }

    void show();
    void hide();

    bool isShown() const;

    void update(float timePassed);

    EditorContext * getContext()
    {
        return context;
    }

    virtual void handleShow()
    {
        // empty
    }

    virtual void handleHide()
    {
        // empty
    }

    /// Called regardless of the editors visibility
    virtual void handleUpdate(float timePassed);

    /// Called when the editor is visible
    virtual void handleGuiUpdate(float timePassed);

    virtual void handleEvent(const EditorEvent *event, int userdata);

    EditorContext *context;

private:
    const char *name;
    bool shown;
};

void formatFileSize(int bufSize, char *buf, uint32_t numBytes);
    
} /* namespace miko */

#endif /* MIKO_EDITOR_EDITOR_HPP */

