#include "asset_browser.hpp"

#include "miko/io/filesys.hpp"
#include "miko/io/log.hpp"
#include "miko/memory/memory.hpp"
#include "miko/resource/resource_manager.hpp"


#include "main.hpp"

#include <imgui/imgui.h>

namespace miko {

AssetDirectory::AssetDirectory():
    collapsed(true)
{
}

AssetDirectory::~AssetDirectory()
{
    for (unsigned int i = 0; i < subdirs.size(); i++) {
        MIKO_DELETE(defaultAllocator, subdirs[i]);
    }
}

AssetBrowser::AssetBrowser(EditorContext *context):
    Editor(context, "AssetBrowser"),
    rootDir(NULL),
    assets(defaultAllocator),
    listener(NULL),
    userdata(0)
{
    refresh();
}

AssetBrowser::~AssetBrowser()
{
}

bool AssetBrowser::addListener(Editor *_listener, int _event)
{
    if (listener) {
        return false;
    }

    listener = _listener;
    userdata = _event;

    return true;
}

void AssetBrowser::removeListener(Editor *_listener)
{
    if (listener == _listener) {
        listener = NULL;
        userdata = 0;
    }
}

void AssetBrowser::refresh()
{
    MIKO_DELETE(defaultAllocator, rootDir);
    assets.clear();
    preview = NULL;

    ResourceManager * resourceManager = context->getResourceManager();
    if (!resourceManager) {
        LOG_DEBUG("asset browser missing resource manager");
        return;
    }

    rootDir = refreshRecurse(resourceManager, "/");
    if (rootDir) {
        rootDir->name = '/';
    }
    else {
        LOG_ERROR("failed to refresh asset browser contents");
    }
}

AssetDirectory * AssetBrowser::refreshRecurse(ResourceManager *resourceManager, const std::string &dirpath)
{
    AssetDirectory *dir = MIKO_NEW(defaultAllocator, AssetDirectory);
    if (!dir) {
        return NULL;
    }

    ResourceEntryList tmp;
    if (!resourceManager->getDirEntries(dirpath, &tmp)) {
        LOG_DEBUGF("failed to get resource list for directory '%s'", dirpath.c_str());
        goto fail;
    }

    for (unsigned int i = 0; i < tmp.size(); i++) {
        ResourceEntry &e = tmp[i];

        if (e.type == ResourceEntry::DIRECTORY) {
            AssetDirectory *subdir = refreshRecurse(resourceManager, e.path);
            if (!subdir) {
                goto fail;
            }

            subdir->name = e.name;

            if (!dir->subdirs.append(subdir)) {
                MIKO_DELETE(defaultAllocator, subdir);
                goto fail;
            }
        }
        else if (e.type == ResourceEntry::RESOURCE) {
            if (!dir->files.append(e)) {
                goto fail;
            }
        }
        else {
            LOG_DEBUG("unknown resource type, ignoring");
        }
    }

    return dir;

    fail:
    MIKO_DELETE(defaultAllocator, dir);

    return NULL;
}

void AssetBrowser::signalSelect(const std::string &filepath)
{
    if (listener) {
        AssetSelectEvent event(filepath);
        listener->handleEvent(&event, userdata);
    }

    listener = NULL;
    userdata = 0;

    hide();
}

void AssetBrowser::signalCancel()
{
    if (listener) {
        AssetCancelEvent event;
        listener->handleEvent(&event, userdata);
    }

    listener = NULL;
    userdata = 0;

    hide();
}

void AssetBrowser::handleGuiUpdate(float)
{
    if (ImGui::Button("Refresh")) {
        refresh();
    }

    ImGui::SameLine();
    if (ImGui::Button("Cancel")) {
        signalCancel();
    }

    ImGui::BeginChild("left", ImVec2(300, 0), true);
    if (rootDir) {
        directoryUpdate(rootDir);
    }
    else {
        ImGui::Text("huh, it seems that refresh failed");
    }
    ImGui::EndChild();
    ImGui::SameLine();

    ImGui::BeginGroup();
    ImGui::BeginChild("right", ImVec2(0, 0), true);
    if (preview) {
        ImGui::Text("Path: %s", preview->path.c_str());
        ImGui::Text("Source: %s %s", preview->locator->getType(), preview->locator->getPath());

        char buf[64];
        formatFileSize(sizeof(buf), buf, preview->size);
        ImGui::Text("Size: %s", buf);
    }
    else {
        ImGui::Text("no resource selected");
    }
    ImGui::EndChild();
    ImGui::EndGroup();

    ImGui::Columns(1);
}

void AssetBrowser::directoryUpdate(AssetDirectory *dir)
{
    ImGui::SetNextTreeNodeOpen(true, ImGuiSetCond_Once);
    if (ImGui::TreeNode(dir->name.c_str())) {
        ImGui::SameLine(200); 
        ImGui::Text("<directory>"); 

        for (unsigned int i = 0; i < dir->subdirs.size(); i++) {
            directoryUpdate(dir->subdirs[i]);
        }

        for (unsigned int i = 0; i < dir->files.size(); i++) {
            if (ImGui::Selectable(dir->files[i].name.c_str(),
                    preview == &dir->files[i],
                    ImGuiSelectableFlags_AllowDoubleClick)) {
                preview = &dir->files[i];
                if (ImGui::IsMouseDoubleClicked(0)) {
                    signalSelect(dir->files[i].path);
                }
            }
        }
        ImGui::TreePop();
    }
    else {
        ImGui::SameLine(200); 
        ImGui::Text("<directory>"); 
    }
}
    
} /* namespace miko */
