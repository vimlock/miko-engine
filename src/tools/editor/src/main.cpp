#include "miko/core/context.hpp"
#include "miko/device/sdl_device.hpp"

#include "miko/io/file_logger.hpp"
#include "miko/io/filesys.hpp"

#include "miko/graphics/gl/framebuffer.hpp"
#include "miko/graphics/gl/shader.hpp"

#include "miko/resource/filesystem_locator.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include <imgui/imgui.h>
#include <imgui/imgui_impl_sdl_gl3.h>

#include "main.hpp"

using namespace miko;

EditorContext::EditorContext(Context *_context):
    quitFlag(false),
    context(_context),
    fileSelector(this),
    assetBrowser(this),
    spriteEditor(this)
{
    ImGuiStyle &style = ImGui::GetStyle();

    style.Alpha = 1.0f;
    style.WindowRounding = 0.0f;
    style.ChildWindowRounding = 0.0f;
    style.FrameRounding = 0.0f;
    style.ScrollbarRounding = 0.0f;
}

void EditorContext::update(float timePassed)
{
    menubar();

    fileSelector.update(timePassed);
    assetBrowser.update(timePassed);
    spriteEditor.update(timePassed);
}

void EditorContext::quit()
{
    quitFlag = true;
}

bool EditorContext::shouldQuit() const
{
    return quitFlag;
}

void EditorContext::menubar()
{
    if (ImGui::BeginMainMenuBar()) {

        if (ImGui::BeginMenu("file")) {
            if (ImGui::MenuItem("new")) {
            }

            if (ImGui::MenuItem("quit")) {
                quit();
            }

            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("preferences")) {
            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("edit")) {
            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("view")) {
            if (ImGui::MenuItem("Sprite Editor")) {
                spriteEditor.show();
            }

            ImGui::EndMenu();
        }

        ImGui::EndMainMenuBar();
    }
}

static void gameview(GlTexture *texture)
{
    ImGuiWindowFlags flags =
        ImGuiWindowFlags_NoResize    |
        ImGuiWindowFlags_NoScrollbar |
        ImGuiWindowFlags_AlwaysAutoResize ;

    if (!ImGui::Begin("Game View", NULL, flags)) {
        ImGui::End();
        return;
    }

    if (texture) {
        ImGui::Image(
            reinterpret_cast<ImTextureID>(texture->getGpuObject()),
            ImVec2(texture->getWidth(), texture->getHeight())
        );
    }

    ImGui::End();
}

int main(int argc, const char *argv[])
{
    (void) argc;
    (void) argv;

    mikoAssert(isMainThread());

    FileLogger logger(stderr, false, true);
    logger.listen();

    DeviceOptions opts;
    SdlDevice device;

    if (!device.init(opts)) {
        LOG_FATAL("failed to initialize device");
        return 0;
    }

    Context context;
    SdlWindow *win = device.createWindow(1280, 768, true, WindowDisplayMode::WINDOWED);
    win->maximize();

    ResourceManager *resourceManager = context.getResourceManager();

    FilesystemLocator locator(getProgramDirectory() + "assets");
    resourceManager->addLocator(&locator);

    // GlShaderProgram *spriteShader = resourceManager->get<GlShaderProgram>(
        // "/shaders/sprite.mkshader");
       
    // ResourceHandle<GlTexture> texture = resourceManager->getHandle<GlTexture>("/textures/pepe.jpeg");
    // resourceManager->preload<GlTexture>("/textures/tenshi.jpg");

    Camera camera;
    //camera.setPerspective(glm::radians(70.0f), 1.0f, 1.0f);
    camera.setOrtho(256.0f, 256.0f);
    camera.setPosition(glm::vec3(0.0, 0.0, 0.0));
    camera.setTarget(glm::vec3(0.0f, 0.0f, -1.0f));

    ImGui_ImplSdlGL3_Init(win->getSdlWindow());

    Ref<GlFramebuffer> framebuffer(GlFramebuffer::create("viewport"));
    Ref<GlTexture> colorBuffer(GlTexture::create(TEXTURE_FORMAT_RGB, 512, 768));
    Ref<GlRenderbuffer> depthBuffer(GlRenderbuffer::create(GL_DEPTH24_STENCIL8, 512, 768));

    framebuffer->setAttachment(GlFramebuffer::COLOR_ATTACHMENT_0, colorBuffer);
    framebuffer->setAttachment(GlFramebuffer::DEPTH_ATTACHMENT, depthBuffer);
    framebuffer->setAttachment(GlFramebuffer::STENCIL_ATTACHMENT, depthBuffer);

    AnimatedSprite sprite;

    // sprite.setAnimation(resourceManager->get<SpriteAnimation>("/sprites/bullet_red.mksprite"));

    sprite.frame.x = 0;
    sprite.frame.y = 0;

    sprite.frame.width = 10;
    sprite.frame.height = 10;

    sprite.frame.origin.x = 5;
    sprite.frame.origin.y = 5;

    sprite.angle = glm::radians(45.0f);

    glClearColor(0.8f, 0.8f, 0.8f, 1.0f);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    glEnable(GL_BLEND);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    EditorContext editor(&context);

    bool running = true;
    while (running) {
        SDL_Event event;
        while (device.pollEvent(&event)) {
            ImGui_ImplSdlGL3_ProcessEvent(&event);
            if (event.type == SDL_QUIT) {
                running = false;
            }
        }

        GlRenderer *renderer = win->getRenderer();

        resourceManager->update();
        renderer->update(1000.0f / 60.0f);
        ImGui_ImplSdlGL3_NewFrame(win->getSdlWindow());

        renderer->setFramebuffer(framebuffer);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // renderer->setTexture(0, texture);
        // renderer->drawQuad(spriteShader);

        renderer->drawSprite(glm::vec3(0.0, 0.0, 0.0), sprite);

        renderer->setCamera(camera);
        renderer->drawBatches();

        renderer->setFramebuffer(NULL);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        editor.update(1000.0f / 60.0f);
        // gameview(colorBuffer);

        if (editor.shouldQuit()) {
            running = false;
        }

        ImGui::Render();

        win->update();
    }

    ImGui_ImplSdlGL3_Shutdown();

    return 0;
}
