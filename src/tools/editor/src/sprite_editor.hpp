#ifndef EDITOR_SPRITE_EDITOR_HPP
#define EDITOR_SPRITE_EDITOR_HPP

#include "editor.hpp"
#include "miko/graphics/sprite.hpp"

namespace miko {

class SpriteEditor : public Editor
{
public:
    SpriteEditor(EditorContext *context);
    
protected:
    virtual void handleGuiUpdate(float timePassed);
    virtual void handleEvent(const EditorEvent *event, int userdata);

private:
    void setTexture(const std::string &id);
    void setMaterial(const std::string &id);

    void setCurrent(SpriteAnimation *anim);

    void select();
    void edit();
    void texture_preview();
    void animation_preview();

    Ref<SpriteAnimation> animation;

    char textureName[256];
    char materialName[256];

    int frame;
};

} /* namespace miko */

#endif /* EDITOR_SPRITE_EDITOR_HPP */

