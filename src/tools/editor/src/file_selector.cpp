#include "file_selector.hpp"

#include "miko/io/log.hpp"
#include "miko/io/filesys.hpp"
#include "miko/memory/memory.hpp"
#include "miko/util/assert.hpp"

#include <imgui/imgui.h>

#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

namespace miko {


/// @todo Move these to a more suited place
/// @todo Verify that this works on Windows also, stat() might not be combatible

bool readDirContents(Vector<FileInfo> *result, const std::string &path)
{
    mikoAssert(result != NULL);

    DIR *dir;

    dir = opendir(path.c_str());
    if (!dir) {
        return false;
    }

    struct dirent *ent;
    while ((ent = readdir(dir))) {
        const char *name = ent->d_name;
        #if 0
        if (name[0] == '.' && name[1] == '\0') {
            continue;
        }

        if (name[0] == '.' && name[1] == '.' && name[2] == '\0') {
            continue;
        }
        #endif

        if (!result->append(FileInfo())) {
            continue;
        }

        FileInfo &fi = result->back();
        fi.name = ent->d_name;
        fi.path = path;
        if (path.size() != 0 && path[path.size() - 1] != '/') {
            fi.path += '/';
        }

        fi.path += fi.name;

        struct stat sb;
        if (stat(fi.path.c_str(), &sb) == -1) {
            LOG_WARNF("failed to stat() %s", fi.path.c_str());
            continue;
        }


        switch (sb.st_mode & S_IFMT) {
        case S_IFREG: fi.type = FILE_REGULAR; break;
        case S_IFDIR: fi.type = FILE_DIRECTORY; break;
        default: fi.type = FILE_OTHER; break;
        }

        fi.size = sb.st_size;
    }

    closedir(dir);

    return true;
}

static int sortSize(const void *_a, const void *_b)
{
    const FileInfo &a = *reinterpret_cast<const FileInfo *>(_a);
    const FileInfo &b = *reinterpret_cast<const FileInfo *>(_b);

    return a.size - b.size;
}

static int sortName(const void *_a, const void *_b)
{
    const FileInfo &a = *reinterpret_cast<const FileInfo *>(_a);
    const FileInfo &b = *reinterpret_cast<const FileInfo *>(_b);

    return strcmp(a.name.c_str(), b.name.c_str());
}

void sortFiles(Vector<FileInfo> *files, FileSortMode mode)
{
    int (*compar)(const void *, const void *);

    if (mode == FILE_SIZE) {
        compar = sortSize;
    }
    else if (mode == FILE_NAME) {
        compar = sortName;
    }
    else {
        mikoUnreachable();
        return;
    }

    qsort(&files->front(), files->size(), sizeof(FileInfo), compar);
}

bool splitPath(Vector<std::string> *result, const std::string &path)
{
    unsigned int iter = 0;

    // read root
    if (path.size() > 0 && path[0] == '/') {
        if (!result->append("/")) {
            return false;
        }

        iter++;
    }

    while (iter < path.size()) {
        // skip leading separators
        if (path[iter] == '/') {
            iter++;
            continue;
        }

        // read a path chunk
        unsigned int start = iter;
        unsigned int end = iter;
        while (iter < path.size()) {
            if (path[iter] == '/') {
                break;
            }
            else {
                iter++;
                end++;
            }
        }

        if (!result->append(path.substr(start, end - start))) {
            return false;
        }
    }

    return true;
}

std::string joinPath(const Vector<std::string> &chunks, int start, int end)
{
    std::string out;

    for (int i = start; i < end; i++) {
        out += chunks[i];
        if (i + 1 < end) {
            out += '/';
        }
    }

    return out;
}

static bool isDotfile(const std::string &path) {
    return path.length() > 0 && path[0] == '.';
}

static bool isParentDirName(const std::string &path) {
    if (path.length() != 2) {
        return false;
    }

    return path[0] == '.' && path[1] == '.';
}

FileSelector::FileSelector(EditorContext *context):
    Editor(context, "File Selector"),
    selectMode(FILE_READ),
    sortMode(FILE_NAME),
    showDotfiles(false),
    listener(NULL),
    files(defaultAllocator),
    pathChunks(defaultAllocator),
    path()
{
    name[0] = '\0';
}

bool FileSelector::addListener(Editor *_listener, int _event)
{
    // we currently have a listener, can't add another
    if (listener != NULL) {
        return false;
    }

    listener = _listener;
    userdata = _event;

    show();

    return true;
}

void FileSelector::removeListener(Editor *_listener, int _event)
{
    if (listener == _listener && userdata == _event) {
        cancel();
    }
}

void FileSelector::removeAllListeners()
{
    cancel();
}

bool FileSelector::open(const std::string &_path, FileSelectMode _mode)
{
    selectMode = _mode;
    path = fileAbsPath(_path);

    files.clear();
    pathChunks.clear();
    name[0] = '\0';

    readDirContents(&files, path);
    sortFiles(&files, sortMode);

    splitPath(&pathChunks, path);

    return true;
}

void FileSelector::handleGuiUpdate(float timePassed)
{
    // we don't need this for anything (for now)
    (void) timePassed;

    int flags = ImGuiWindowFlags_NoCollapse;

    std::string jumpTarget;
    std::string result;

    for (unsigned int i = 0; i < pathChunks.size(); i++) {
        if (ImGui::Button(pathChunks[i].c_str())) {
            jumpTarget = joinPath(pathChunks, 0, i);
        }
        ImGui::SameLine();
    }
    ImGui::NewLine();

    ImGui::Separator();

    // Filename input
    if (ImGui::InputText("filename", name, sizeof(name), ImGuiInputTextFlags_EnterReturnsTrue)) {
        result = fileAbsPath(path + "/" + name);
    }

    ImGui::SameLine();
    if (ImGui::Button("Open")) {
        jumpTarget = fileAbsPath(path + "/" + name);
    }

    ImGui::SameLine();
    if (ImGui::Button("Select")) {
        if (name[0] != '\0') {
            result = fileAbsPath(path + "/" + name);
        }
    }

    ImGui::SameLine();
    if (ImGui::Button("Refresh")) {
        jumpTarget = path;
    }

    ImGui::SameLine();
    if (ImGui::Button("Cancel")) {
        cancel();
    }
    ImGui::NextColumn();

    ImGui::Columns(2, "asd", false);
    if (ImGui::Selectable("Name")) {
        if (sortMode != FILE_NAME) {
            sortMode = FILE_NAME;
            sortFiles(&files, FILE_NAME);
        }
    }
    ImGui::NextColumn();

    if (ImGui::Selectable("Size")) {
        if (sortMode != FILE_SIZE) {
            sortMode = FILE_SIZE;
            sortFiles(&files, FILE_SIZE);
        }
    }
    ImGui::NextColumn();

    ImGui::Separator();

    ImGui::BeginGroup();

    for (unsigned int i = 0; i < files.size(); i++) {
        FileInfo &fi = files[i];

        if (!showDotfiles && isDotfile(fi.name) && !isParentDirName(fi.name)) {
            continue;
        }

        if (ImGui::Selectable(fi.name.c_str(), false, ImGuiSelectableFlags_AllowDoubleClick)) {
            strncpy(name, fi.name.c_str(), sizeof(name));
            if (ImGui::IsMouseDoubleClicked(0)) {

                // double-clicking a regular or special file selects it
                if (fi.type == FILE_REGULAR || fi.type == FILE_OTHER) {
                }
                // double-clicking a directory opens it
                else if (fi.type == FILE_DIRECTORY) {
                    jumpTarget = fi.path;
                }
            }
        }
        ImGui::NextColumn();

        if (fi.type == FILE_REGULAR) {
            char buf[32];
            formatFileSize(sizeof(buf), buf, fi.size);
            ImGui::Text(buf);
        }

        else if (fi.type == FILE_DIRECTORY) {
            ImGui::Text("<directory>");
        }
        else {
            ImGui::Text("<special>");
        }

        ImGui::NextColumn();
    }

    if (ImGui::IsMouseClicked(1)) {
        ImGui::OpenPopup("file-dialog");
    }
    if (ImGui::BeginPopup("file-dialog")) {
        if (ImGui::MenuItem("Refresh")) {
            jumpTarget = path;
        }

        if (ImGui::MenuItem("Cancel")) {
            cancel();
        }

        ImGui::Separator();

        if (ImGui::MenuItem("Create directory")) {
        }
        if (ImGui::MenuItem("Rename")) {
        }
        if (ImGui::MenuItem("Delete")) {
        }
        ImGui::EndPopup();
    }

    ImGui::EndGroup();

    if (jumpTarget.size() > 0) {
        open(jumpTarget, selectMode);
    }
    else if (result.size() > 0){
        signalSelect(result);
        hide();
    }
}

void FileSelector::cancel()
{
    signalCancel();

    listener = NULL;
    userdata = 0;

    hide();
}

void FileSelector::signalSelect(const std::string &filepath)
{
    if (listener) {
        FileSelectEvent event(filepath);
        listener->handleEvent(&event, userdata);
    }
}

void FileSelector::signalCancel()
{
    if (listener) {
        FileCancelEvent event;
        listener->handleEvent(&event, userdata);
    }
}

} /* namespace miko */
