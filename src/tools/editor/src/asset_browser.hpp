#ifndef MIKO_EDITOR_ASSET_BROWSER_HPP
#define MIKO_EDITOR_ASSET_BROWSER_HPP

#include "miko/container/vector.hpp"

#include "miko/resource/resource_locator.hpp"
#include "editor.hpp"

namespace miko {

class ResourceManager;

struct AssetCancelEvent : public EditorEvent
{
    AssetCancelEvent():
        EditorEvent(EVENT_ASSET_SELECT_CANCELLED)
    {
        // empty
    }
};

struct AssetSelectEvent : public EditorEvent
{
    AssetSelectEvent(const std::string _filepath):
        EditorEvent(EVENT_ASSET_SELECT_SELECTED),
        filepath(_filepath)
    {
        // empty
    }

    std::string filepath;
};

class AssetDirectory
{
public:
    AssetDirectory();
    ~AssetDirectory();

    std::string name;
    bool collapsed;
    Vector<AssetDirectory *> subdirs;
    Vector<ResourceEntry> files;
};

class AssetBrowser : public Editor
{
public:
    AssetBrowser(EditorContext *context);
    virtual ~AssetBrowser();
    
    bool addListener(Editor *listener, int event);
    void removeListener(Editor *listener);

    /// Reload the resource list
    void refresh();

protected:
    virtual void handleGuiUpdate(float timePassed);

    void directoryUpdate(AssetDirectory *dir);

private:
    AssetDirectory * refreshRecurse(ResourceManager *resourceManager, const std::string &dir);

    void signalSelect(const std::string &filepath);
    void signalCancel();

    AssetDirectory *rootDir;

    /// List of every asset excluding directories
    Vector<ResourceEntry> assets;

    /// Target for preview
    /// Should point to inside the directory tree
    ResourceEntry *preview;

    Editor *listener;
    int userdata;
};
    
} /* namespace miko */

#endif /* MIKO_EDITOR_ASSET_BROWSER_HPP */

