#include "sprite_editor.hpp"
#include "asset_browser.hpp"
#include "main.hpp"

#include <imgui/imgui.h>
#include <string.h>

namespace miko {

enum SpriteEditorEvent
{
    SPRITE_EDITOR_ANIMATION_CHANGED,

    SPRITE_EDITOR_TEXTURE_CHANGED,
    SPRITE_EDITOR_MATERIAL_CHANGED
};

SpriteEditor::SpriteEditor(EditorContext *context):
        Editor(context, "Sprite Editor"),
        frame(0)
{
    textureName[0] = '\0';
    materialName[0] = '\0';
}

void SpriteEditor::handleGuiUpdate(float)
{
    AssetBrowser *assetBrowser = context->getAssetBrowser();

    if (ImGui::Button("New")) {
        setCurrent(SpriteAnimation::create(defaultAllocator));
    }

    ImGui::SameLine();
    if (ImGui::Button("Open")) {
        assetBrowser->addListener(this, SPRITE_EDITOR_ANIMATION_CHANGED);
        assetBrowser->show();
    }

    if (animation) {
        edit();
        animation_preview();
        texture_preview();
    }
    else {
        select();
    }
}

void SpriteEditor::handleEvent(const EditorEvent *event, int userdata)
{
    if (event->type == EVENT_ASSET_SELECT_SELECTED) {
        const FileSelectEvent *e = static_cast<const FileSelectEvent *>(event);

        ResourceManager *resourceManager = getContext()->getResourceManager();

        switch (userdata) {
        case SPRITE_EDITOR_ANIMATION_CHANGED:
            animation = resourceManager->getHandle<SpriteAnimation>(e->filepath);
            
            if (animation) {
                strncpy(textureName, animation->texture.getName().c_str(), sizeof(textureName));
                strncpy(materialName, animation->material.getName().c_str(), sizeof(materialName));
            }
            else {
                textureName[0] = '\0';
                materialName[0] = '\0';
            }

            break;

        case SPRITE_EDITOR_TEXTURE_CHANGED:
            animation->texture = resourceManager->getHandle<GlTexture>(e->filepath);
            strncpy(textureName, e->filepath.c_str(), sizeof(textureName));
            break;
        case SPRITE_EDITOR_MATERIAL_CHANGED:
            animation->material = resourceManager->getHandle<GlMaterial>(e->filepath);
            strncpy(materialName, e->filepath.c_str(), sizeof(materialName));
            break;
        default:
            break;
        }
    }
}

void SpriteEditor::setCurrent(SpriteAnimation *anim)
{
    animation = anim;
    if (!anim) {
        return;
    }

    if (anim->texture) {
        strncpy(textureName, anim->texture->getName().c_str(), sizeof(textureName));
    }
    else {
        textureName[0] = '\0';
    }

    if (anim->material) {
        strncpy(materialName, anim->material->getName().c_str(), sizeof(materialName));
    }
    else {
        materialName[0] = '\0';
    }
}

void SpriteEditor::select()
{
    ImGui::Text("no animation selected");
}

void SpriteEditor::edit()
{
    AssetBrowser *assetBrowser = context->getAssetBrowser();

    const std::string &name = animation->getName();
    if (name.length() > 0) {
        ImGui::Text("%s", name.c_str());
    }
    else {
        ImGui::Text("<untitled>");
    }

    if (ImGui::CollapsingHeader("Rendering properties", ImGuiTreeNodeFlags_DefaultOpen)) {
        if (ImGui::InputText("Texture", textureName, sizeof(textureName))) {
        }

        ImGui::SameLine();
        if (ImGui::Button("Open##texture")) {
            assetBrowser->addListener(this, SPRITE_EDITOR_TEXTURE_CHANGED);
            assetBrowser->show();
        }

        if (ImGui::InputText("Material", materialName, sizeof(materialName))) {
        }

        ImGui::SameLine();
        if (ImGui::Button("Open##material")) {
            assetBrowser->addListener(this, SPRITE_EDITOR_MATERIAL_CHANGED);
            assetBrowser->show();
        }

        ImGui::SameLine();
        if (ImGui::Button("Edit##material")) {
        }
    }

    if (ImGui::CollapsingHeader("Frame properties", ImGuiTreeNodeFlags_DefaultOpen)) {
        if (animation->frames.size() > 0) {
            ImGui::SliderInt("Frame index", &frame, 0, animation->frames.size() - 1);

            AnimatedSpriteFrame &f = animation->frames[frame];

            ImGui::DragFloat("X", &f.x, 1.0f, 0.0f, 1024.0f);
            ImGui::DragFloat("Y", &f.y, 1.0f, 0.0f, 1024.0f);
            ImGui::DragFloat("Width", &f.width, 1.0f, 0.0f, 1024.0f);
            ImGui::DragFloat("Height", &f.height, 1.0f, 0.0f, 1024.0f);
            ImGui::DragFloat("X offset", &f.origin.x, 1.0f, 0.0f, 1024.0f);
            ImGui::DragFloat("Y offset", &f.origin.y, 1.0f, 0.0f, 1024.0f);

            int duration = f.duration;
            if (ImGui::DragInt("Duration", &duration, 1.0f, 0, 1024)) {
                f.duration = duration;
            }
        }
    }
}

void SpriteEditor::texture_preview()
{
    if (!ImGui::CollapsingHeader("Texture preview", ImGuiTreeNodeFlags_DefaultOpen)) {
        return;
    }

    // draw the preview
    if (!animation->texture) {
        ImGui::Text("No texture, preview not available");
        return;
    }

    GlTexture *texture = animation->texture;
    ImGui::Image(
        reinterpret_cast<ImTextureID>((uintptr_t)texture->getGpuObject()),
        ImVec2(
            texture->getWidth(),
            texture->getHeight()
        )
    );
}

void SpriteEditor::animation_preview()
{
    if (!ImGui::CollapsingHeader("Animation preview", ImGuiTreeNodeFlags_DefaultOpen)) {
        return;
    }

    if (!animation->texture) {
        ImGui::Text("No texture, preview not available");
        return;
    }

    if (!animation->material) {
        ImGui::Text("No material, preview not available");
        return;
    }
}

} /* namespace miko */
