#ifndef MIKO_BULLET_TYPE_HPP
#define MIKO_BULLET_TYPE_HPP

#include "miko/core/object.hpp"

namespace miko {

class BulletType : public Object
{
public:
    float radius;

    uint32_t trailRate;
};
    
} /* namespace miko */

#endif /* MIKO_BULLET_TYPE_HPP */

