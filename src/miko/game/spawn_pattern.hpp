#ifndef MIKO_SPAWN_PATTERN_HPP
#define MIKO_SPAWN_PATTERN_HPP

#include "miko/miko.hpp"

namespace miko {

enum AngleOrigin
{
    ANGLE_ZERO,
    ANGLE_TO_PLAYER,
    ANGLE_RANDOM
};

struct SpawnPattern
{
public:
    uint16_t numBullets;
    uint16_t numWaves;
    uint16_t numPatterns;

    uint16_t bulletInterval;
    uint16_t waveInterval;
    uint16_t patternInterval;

    uint16_t beginDelay;

    AngleOrigin angleOrigin;
    float angleDistort;
    float angleOffset;
    float waveArch;
    float waveRotation;

    float baseSpeed;
    float waveSpeed;

};

class Spawner
{
public:
    SpawnPattern *pattern;

    uint16_t bulletNum;
    uint16_t waveNum;
};
    
} /* namespace miko */

#endif /* MIKO_SPAWN_PATTERN_HPP */

