#ifndef MIKO_HASH_MAP_INL
#define MIKO_HASH_MAP_INL

#include "miko/container/hash_map.hpp"
#include "miko/memory/memory.hpp"
#include "miko/util/assert.hpp"

#include <string>

namespace miko {

// ============================================================================
// hash

template<>
inline HashResult hash(const std::string &str)
{
    HashResult tmp = 0xC0FFEE;
    for (unsigned i = 0; i < str.length(); i++) {
        tmp = str[i] + (tmp << 5) + tmp;
    }

    return tmp;
}

template<typename T>
inline HashResult hash(const T &val)
{
    return val;
}

// ============================================================================
// HashElem

template <typename K, typename V>
HashElem<K, V>::HashElem(const K &k, const V &v, HashResult hash):
    pair(k, v),
    hash(hash)
{
    // empty
}

template <typename K, typename V>
HashElem<K, V>::HashElem(const K &k, V &v, HashResult hash):
    pair(k, v),
    hash(hash)
{
    // empty
}

// ============================================================================
// HashMap
    
template <typename K, typename V>
inline HashMap<K, V>::HashMap(Allocator &allocator):
        allocator(allocator),
        list(0),
        slots(0),
        numSlots(0),
        numElems(0),
        iterChecksum(0)
{
    // get some random number here
    iterChecksum  = reinterpret_cast<uintptr_t>(this);
}

template <typename K, typename V>
inline HashMap<K, V>::~HashMap()
{
    for (unsigned i = 0; i < numSlots; i++) {
        ElemType *elem = slots[i];
        while (elem) {
            ElemType *next = elem->next;
            elem->~ElemType();
            allocator.free(elem);
            elem = next;
        }
    }

    allocator.free(slots);
}

template <typename K, typename V>
inline bool HashMap<K, V>::contains(const KeyType &key) const
{
    return find(key) != end();
}

template <typename K, typename V>
inline bool HashMap<K, V>::get(const KeyType &key, ValType *&val)
{
    // abort early
    if (numElems == 0) {
        return false;
    }

    HashResult hash = getHash(key);
    unsigned int slot = hash % numSlots;

    ElemType *e = slots[slot];
    while (e) {
        if (e->hash == hash && e->pair.key == key) {
            val = &e->pair.val;
            return true;
        }

        e = e->next;
    }

    return false;
}

template <typename K, typename V>
bool HashMap<K, V>::set(const KeyType &key, ValType &val)
{
    invalidateIterarators();

    HashResult hash = getHash(key);
    unsigned slot;
    ElemType *elem;

    if (numElems == 0) {
        goto add_new;
    }

    slot = hash % numSlots;

    // replace old key if one exists
    elem = slots[slot];
    while (elem) {
        if (elem->hash == hash && elem->pair.key == key) {
            // replace the old key
            elem->pair.key = key;
            elem->pair.val = val;
            return true;
        }

        elem = elem->next;
    }

    add_new:

    // maintain proper slot/element balance
    if (numElems == 0 || numElems > numSlots * 3) {
        // too tight? lets resize
        if (!resize(numSlots == 0 ? 2 : numSlots * 2)) {
            return false;
        }

        // recalculate the slot because it is invalidated by the resize operation
        slot = hash % numSlots;
    }

    // create a new element
    elem = MIKO_NEW(allocator, ElemType) (key, val, hash);
    if (!elem) {
        return false;
    }

    numElems++;

    // add the element to the slot chain
    elem->next = slots[slot];
    slots[slot] = elem;

    // add the element to list of all elements
    elem->listNext = list;
    elem->listPrev= &list;
    if (list) {
        list->listPrev = &elem->listNext;
    }
    list = elem;

    return true;
}

template <typename K, typename V>
bool HashMap<K, V>::set(const KeyType &key, const ValType &val)
{
    return set(key, const_cast<ValType &>(val));
}

template <typename K, typename V>
void HashMap<K, V>::remove(const KeyType &key)
{
    invalidateIterarators();

    if (numElems == 0) {
        return;
    }

    HashResult hash = getHash(key);
    uint32_t slot = hash % numSlots;

    ElemType **prev = &slots[slot];
    ElemType *iter = slots[slot];

    while (iter) {
        if (iter->hash == hash && iter->pair.key == key) {
            // delete the element from slot chain
            *prev = iter->next;

            // delete the element from list of all elements
            if (iter->listNext) {
                iter->listNext->listPrev = iter->listPrev;
            }
            *iter->listPrev = iter->listNext;

            MIKO_DELETE(allocator, iter);
            numElems--;
            
            return;
        }

        prev = &iter->next;
        iter = iter->next;
    }
}

template <typename K, typename V>
bool HashMap<K, V>::erase(const KeyType &key)
{
    Iterator i = find(key);
    if (i != end()) {
        erase(i);
        return true;
    }
    else {
        return false;
    }
}

template <typename K, typename V>
typename HashMap<K, V>::Iterator HashMap<K, V>::erase(const Iterator &iter)
{
    checkIterator(iter);
    mikoAssert(iter != end() && "can't erase end()");

    invalidateIterarators();

    Iterator next(this, iter.elem->listNext);
    removeElement(iter.elem, NULL);

    return next;
}

template <typename K, typename V>
typename HashMap<K, V>::Iterator HashMap<K, V>::find(const KeyType &key)
{
    if (numElems == 0) {
        return end();
    }

    HashResult hash = getHash(key);
    unsigned int slot = hash % numSlots;

    ElemType *e = slots[slot];
    while (e) {
        if (e->hash == hash && e->pair.key == key) {
            return Iterator(this, e);
        }

        e = e->next;
    }

    return end();
}

template <typename K, typename V>
typename HashMap<K, V>::ConstIterator HashMap<K, V>::find(const KeyType &key) const
{
    if (numElems == 0) {
        return end();
    }

    HashResult hash = getHash(key);
    unsigned int slot = hash % numSlots;

    ElemType *e = slots[slot];
    while (e) {
        if (e->hash == hash && e->pair.key == key) {
            return ConstIterator(this, e);
        }

        e = e->next;
    }

    return end();
}

template <typename K, typename V>
typename HashMap<K,V>::Iterator HashMap<K, V>::begin()
{
    return Iterator(this, list);
}

template <typename K, typename V>
typename HashMap<K,V>::ConstIterator HashMap<K, V>::begin() const
{
    return ConstIterator(this, list);
}

template <typename K, typename V>
typename HashMap<K,V>::Iterator HashMap<K, V>::end()
{
    return Iterator(this, 0);
}

template <typename K, typename V>
typename HashMap<K,V>::ConstIterator HashMap<K, V>::end() const
{
    return ConstIterator(this, 0);
}

template <typename K, typename V>
void HashMap<K, V>::reinsert(ElemType *elem)
{
    uint32_t slot;

    slot = elem->hash % numSlots;
    elem->next = slots[slot];
    slots[slot] = elem;
}

template <typename K, typename V>
bool HashMap<K, V>::resize(uint32_t newSlotsCount)
{
    ElemType **newSlots = reinterpret_cast<ElemType**>(allocator.alloc(sizeof(ElemType *) * newSlotsCount));
    if (!newSlots) {
        return false;
    }
    
    // TODO: memset this
    for (unsigned int i = 0; i < newSlotsCount; i++) {
        newSlots[i] = 0;
    }

    ElemType **oldSlots = slots;
    uint32_t oldSlotsCount = numSlots;

    slots = newSlots;
    numSlots = newSlotsCount;

    for (unsigned int i = 0; i < oldSlotsCount; i++) {
        ElemType *elem = oldSlots[i];
        while (elem) {
            ElemType *next = elem->next;
            reinsert(elem);
            elem = next;
        }
    }

    allocator.free(oldSlots);

    return true;
}

template <typename K, typename V>
void HashMap<K, V>::checkIterator(const Iterator &iter)
{
    mikoAssert((iter.elem == 0 || iter.iterChecksum == iterChecksum) && "Iterator invalidated");
}

template <typename K, typename V>
void HashMap<K, V>::invalidateIterarators()
{
    iterChecksum++;
}

template <typename K, typename V>
void HashMap<K, V>::removeElement(ElemType *elem, ElemType **slotPrev)
{
    if (!slotPrev) {
        // find the previous slot
        unsigned int slot = elem->hash % numSlots;
        slotPrev = &slots[slot];
        while (*slotPrev != elem) {
            slotPrev = &(*slotPrev)->next;
        }
    }

    // delete the element from slot chain
    *slotPrev = elem->next;

    // delete the element from list of all elements
    if (elem->listNext) {
        elem->listNext->listPrev = elem->listPrev;
    }

    *elem->listPrev = elem->listNext;

    MIKO_DELETE(allocator, elem);
    numElems--;
}

template <typename K, typename V>
HashResult HashMap<K, V>::getHash(const KeyType &key) const
{
    return hash(key);
}

// ============================================================================
// HashMap::Iterator
//
template <typename K, typename V>
inline HashMap<K, V>::Iterator::Iterator()
{
    elem = 0;
    iterChecksum = 0;
}

template <typename K, typename V>
inline HashMap<K, V>::Iterator::Iterator(Type *target, ElemType *elem)
{
    this->elem = elem;
    this->iterChecksum = target->iterChecksum;
}

template <typename K, typename V>
inline typename HashMap<K, V>::Iterator& HashMap<K, V>::Iterator::operator ++ ()
{
    mikoAssert(elem != NULL && "can't iterate past end()");
    elem = elem->listNext;
    return *this;
}

template <typename K, typename V>
inline typename HashMap<K, V>::Iterator& HashMap<K, V>::Iterator::operator ++ (int)
{
    return ++*this;
}

template <typename K, typename V>
inline bool HashMap<K, V>::Iterator::operator == (const Iterator &that) const
{
    return this->elem == that.elem;
}

template <typename K, typename V>
inline bool HashMap<K, V>::Iterator::operator != (const Iterator &that) const
{
    return ! (*this == that);
}

template <typename K, typename V>
KeyValPair<K, V> & HashMap<K, V>::Iterator::operator * ()
{
    mikoAssert(elem != NULL && "cant dereference end()");
    return elem->pair;
}

template <typename K, typename V>
const KeyValPair<K, V> & HashMap<K, V>::Iterator::operator * () const
{
    mikoAssert(elem != NULL && "cant dereference end()");
    return elem->pair;
}

template <typename K, typename V>
KeyValPair<K, V> * HashMap<K, V>::Iterator::operator -> ()
{
    mikoAssert(elem != NULL && "cant dereference end()");
    return &elem->pair;
}

template <typename K, typename V>
const KeyValPair<K, V> * HashMap<K, V>::Iterator::operator -> () const
{
    mikoAssert(elem != NULL && "cant dereference end()");
    return &elem->pair;
}

// ============================================================================
// HashMap::ConstIterator
//
//
template <typename K, typename V>
inline HashMap<K, V>::ConstIterator::ConstIterator()
{
    // empty
}

template <typename K, typename V>
inline HashMap<K, V>::ConstIterator::ConstIterator(const Type *target, const ElemType *elem):
    Iterator(const_cast<Type*>(target), const_cast<ElemType*>(elem))
{
    // empty
}

template <typename K, typename V>
inline typename HashMap<K, V>::ConstIterator& HashMap<K, V>::ConstIterator::operator ++ ()
{
    this->Iterator::operator ++ ();
    return *this;
}

template <typename K, typename V>
inline typename HashMap<K, V>::ConstIterator& HashMap<K, V>::ConstIterator::operator ++ (int)
{
    return ++*this;
}

template <typename K, typename V>
inline bool HashMap<K, V>::ConstIterator::operator == (const ConstIterator &that) const
{
    return this->elem == that.elem;
}

template <typename K, typename V>
inline bool HashMap<K, V>::ConstIterator::operator != (const ConstIterator &that) const
{
    return ! (*this == that);
}

template <typename K, typename V>
const KeyValPair<K, V> & HashMap<K, V>::ConstIterator::operator * () const
{
    return this->Iterator::operator * ();
}

template <typename K, typename V>
const KeyValPair<K, V> * HashMap<K, V>::ConstIterator::operator -> () const
{
    return this->Iterator::operator -> ();
}

} /* namespace miko */

#endif /* MIKO_HASH_MAP_INL */

