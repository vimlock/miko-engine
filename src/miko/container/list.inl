#ifndef __MIKO_LIST_INL__
#define __MIKO_LIST_INL__

#include "miko/container/list.hpp"
#include "miko/util/assert.hpp"

namespace miko {

// ============================================================================
// List<T>::Node
template <typename T>
List<T>::Node::Node(T &_value):
        next(0),
        prev(0),
        value(_value)
{
    // empty
}

template <typename T>
List<T>::Node::Node(const T &_value):
        next(0),
        prev(0),
        value(_value)
{
    // empty
}

// ============================================================================
// List<T>::Iterator


template <typename T>
List<T>::Iterator::Iterator():
    list(0),
    node(0),
    checksum(0)
{
    // empty
}

template <typename T>
List<T>::Iterator::Iterator(List<T> *_list, Node *_node):
    list(_list),
    node(_node),
    checksum(_list->checksum)
{
    // empty
}

template <typename T>
T& List<T>::Iterator::operator * ()
{
    return node->value;
}

template <typename T>
const T& List<T>::Iterator::operator * () const
{
    return node->value;
}

template <typename T>
T* List<T>::Iterator::operator -> ()
{
    return &node->value;
}

template <typename T>
const T* List<T>::Iterator::operator -> () const
{
    return &node->value;
}

template <typename T>
typename List<T>::Iterator & List<T>::Iterator::operator ++ ()
{
    mikoAssert(list != 0 && "iterator not bound to a list");
    mikoAssert(node != 0 && "can't increment past end()");
    node = node->next;

    return *this;
}

template <typename T>
typename List<T>::Iterator & List<T>::Iterator::operator -- ()
{
    mikoAssert(list != 0 && "iterator not bound to a list");

    Node *prev;
    // at the end
    if (node == 0) {
        prev = list->tail;
    }
    else {
        prev = node->prev;
    }

    mikoAssert(prev != 0 && "can't decrement past begin()");
    node = prev;

    return *this;
}

template <typename T>
typename List<T>::Iterator List<T>::Iterator::operator ++ (int)
{
    Iterator tmp(*this);
    operator++();
    return tmp;
}

template <typename T>
typename List<T>::Iterator List<T>::Iterator::operator -- (int)
{
    Iterator tmp(*this);
    operator--();
    return tmp;
}

template <typename T>
bool List<T>::Iterator::operator == (const Iterator &other) const
{
    return node == other.node;
}

template <typename T>
bool List<T>::Iterator::operator != (const Iterator &other) const
{
    return node != other.node;
}

// ============================================================================
// List<T>

template <typename T>
List<T>::List(Allocator &_allocator):
        head(0),
        tail(0),
        count(0),
        checksum(reinterpret_cast<uintptr_t>(this)), // initialize to some random value 
        allocator(_allocator)
{
}

template <typename T>
List<T>::~List()
{
    clear();
}

template <typename T>
typename List<T>::Iterator List<T>::begin()
{
    return Iterator(this, head);
}

template <typename T>
typename List<T>::Iterator List<T>::end()
{
    return Iterator(this, 0);
}

template <typename T>
void List<T>::append(const T& value)
{
    Node *n = MIKO_NEW(allocator, Node) (value);

    if (!head) {
        head = n;
        tail = n;
    }
    else {
        tail->next = n;
        n->prev = tail;
        tail = n;
    }

    count++;
}

template <typename T>
void List<T>::prepend(const T& value)
{
    Node *n = MIKO_NEW(allocator, Node) (value);

    if (!head) {
        head = n;
        tail = n;
    }
    else {
        head->prev = n;
        n->next = head;
        head = n;
    }

    count++;
}

template <typename T>
void List<T>::insertBetween(Node *prev, Node *next, const T& value)
{
    Node *n = MIKO_NEW(allocator, Node) (value);

    n->prev = prev;
    n->next = next;
    
    prev->next = n;
    next->prev = n;

    count++;
}

template <typename T>
void List<T>::insertAfter(const Iterator &offset, const T& value)
{
    validateIterator(offset);
    mikoAssert(offset != end());

    if (offset.node->next == 0) {
        append(value);
    }
    else {
        insertBetween(offset.node, offset.node->next, value);
    }
}

template <typename T>
void List<T>::insertBefore(const Iterator &offset, const T& value)
{
    validateIterator(offset);

    if (offset.node == 0) {
        append(value);
    }
    else if (offset.node == head) {
        prepend(value);
    }
    else {
        insertBetween(offset.node->prev, offset.node, value);
    }
}

template <typename T>
T & List<T>::first()
{
    return head->value;
}

template <typename T>
const T & List<T>::first() const
{
    return head->value;
}

template <typename T>
T & List<T>::last()
{
    return tail->value;
}

template <typename T>
const T & List<T>::last() const
{
    return tail->value;
}

template <typename T>
void List<T>::clear()
{
    Node *iter = head;

    while (iter) {
        Node *next = iter->next;
        MIKO_DELETE(allocator, iter);
        mikoAssert(count > 0);
        count--;
        iter = next;
    }

    head = 0;
    tail = 0;
}

template <typename T>
void List<T>::removeFirst()
{
    if (!head) {
        return;
    }

    invalidateIterators();
    if (head->next) {
        Node *tmp = head; 
        head->next->prev = 0;
        head = head->next;
        MIKO_DELETE(allocator, tmp);
    }
    else {
        MIKO_DELETE(allocator, head);
        head = 0;
        tail = 0;
    }

    count--;
}

template <typename T>
void List<T>::removeLast()
{
    if (!tail) {
        return;
    }

    invalidateIterators();
    if (tail->prev) {
        Node *tmp = tail;
        tail->prev->next = 0;
        tail = tail->prev;
        MIKO_DELETE(allocator, tmp);
    }
    else {
        MIKO_DELETE(allocator, tail);
        head = 0;
        tail = 0;
    }

    count--;
}

template <typename T>
typename List<T>::Iterator List<T>::erase(const Iterator &offset)
{
    validateIterator(offset);
    mikoAssert(offset != end());

    Node *node = offset.node;
    Node *next = node->next;

    if (node->prev) {
        node->prev->next = node->next;
    }
    else {
        head = node->next;
    }

    if (node->next) {
        node->next->prev = node->prev;
    }
    else {
        tail = node->prev;
    }

    // just to ease debugging
    node->next = 0;
    node->prev = 0;

    MIKO_DELETE(allocator, node);
    count--;

    invalidateIterators();

    return Iterator(this, next);
}
    
template <typename T>
uint32_t List<T>::size() const
{
    return count;
}

template <typename T>
void List<T>::validateIterator(const Iterator &iter)
{
    mikoAssert((iter.node == 0 || (iter.list == this && iter.checksum == checksum)) &&
        "using invalidated iterator()"
    );
}

template <typename T>
void List<T>::invalidateIterators()
{
    checksum++;
}
    
} /* namespace miko */

#endif /* __MIKO_LIST_INL__ */

