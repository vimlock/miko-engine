#ifndef MIKO_HASH_MAP_HPP
#define MIKO_HASH_MAP_HPP

#include "miko/memory/memory.hpp"

namespace miko {

typedef uint32_t HashResult;

template<typename T>
HashResult hash(const T& t);

template<typename KeyType, typename ValType>
struct KeyValPair
{
    KeyValPair(KeyType &k, ValType &v):
        key(k),
        val(v)
    {
        // empty
    }

    KeyValPair(const KeyType &k, ValType &v):
        key(k),
        val(v)
    {
        // empty
    }

    KeyValPair(const KeyType &k, const ValType &v):
        key(k),
        val(v)
    {
        // empty
    }

    KeyType key;
    ValType val;
};

template<typename KeyType, typename ValType>
struct HashElem
{
    typedef HashElem<KeyType, ValType> Type;

    HashElem(const KeyType &k, const ValType &v, HashResult hash);
    HashElem(const KeyType &k, ValType &v, HashResult hash);

    KeyValPair<KeyType, ValType> pair;
    HashResult hash;

    Type *next;
    Type *listNext;
    Type **listPrev;
};

template<typename K, typename V>
class HashMap
{
private:
    typedef HashElem<K, V> ElemType;

public:
    typedef HashMap<K, V> Type;
    typedef K KeyType;
    typedef V ValType;

    class Iterator
    {
    friend class HashMap;
    public:
        Iterator();

        // prefix
        Iterator& operator ++ ();

        // postfix
        Iterator& operator ++ (int);

        bool operator == (const Iterator &iter) const;
        bool operator != (const Iterator &iter) const;

        KeyValPair<K, V> & operator * ();
        const KeyValPair<K, V> & operator * () const;

        KeyValPair<K, V> * operator -> ();
        const KeyValPair<K, V> * operator -> () const;

    private:
        Iterator(Type *target, ElemType *elem);
    protected:

        ElemType *elem;
        uint32_t iterChecksum;
    };

    class ConstIterator : private Iterator
    {
    friend class HashMap;
    public:
        ConstIterator();

        // prefix
        ConstIterator& operator ++ ();

        // postfix
        ConstIterator& operator ++ (int);

        bool operator == (const ConstIterator &iter) const;
        bool operator != (const ConstIterator &iter) const;

        const KeyValPair<K, V> & operator * () const;
        const KeyValPair<K, V> * operator -> () const;

    private:
        ConstIterator(const Type *target, const ElemType *elem);

    };


    friend class Iterator;

    HashMap(Allocator &allocator=defaultAllocator);
    ~HashMap();

    uint32_t size() const
    {
        return numElems;
    }

    Allocator & getAllocator()
    {
        return allocator;
    }

    bool contains(const KeyType &key) const;
    bool get(const KeyType &key, ValType *&val) MIKO_WARN_UNUSED();

    bool set(const KeyType &key, ValType &val) MIKO_WARN_UNUSED();
    bool set(const KeyType &key, const ValType &val) MIKO_WARN_UNUSED();

    void remove(const KeyType &key);

    bool erase(const KeyType &key);
    Iterator erase(const Iterator &iter);

    Iterator find(const KeyType &key);
    ConstIterator find(const KeyType &key) const;

    Iterator begin();
    ConstIterator begin() const;

    Iterator end();
    ConstIterator end() const;

private:
    void reinsert(ElemType *elem);
    bool resize(uint32_t newSlotsCount);

    void checkIterator(const Iterator &iter);
    void invalidateIterarators();

    void removeElement(ElemType *elem, ElemType **slotPrev);

    HashResult getHash(const KeyType &key) const;

    Allocator &allocator;

    ElemType* list;
    ElemType **slots;
    uint32_t numSlots;

    uint32_t numElems;
    uint32_t iterChecksum;
};

} /* namespace miko */

#include "miko/container/hash_map.inl"

#endif /* MIKO_HASH_MAP_HPP */

