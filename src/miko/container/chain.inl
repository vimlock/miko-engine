#ifndef MIKO_CHAIN_INL
#define MIKO_CHAIN_INL

#include "miko/container/chain.hpp"

namespace miko {

// ============================================================================
// ChainLink

template <typename T>
ChainLink<T>::ChainLink()
{
    next = 0;
    prev = 0;
}

template <typename T>
ChainLink<T>::~ChainLink()
{
    unlink();
}

template <typename T>
void ChainLink<T>::linkAfter(ChainLink<T> &n)
{
}

template <typename T>
void ChainLink<T>::linkBefore(ChainLink<T> &n)
{
}

template <typename T>
void ChainLink<T>::linkBetween(ChainLink<T> &prev, ChainLink<T> &next)
{
}

template <typename T>
void ChainLink<T>::unlink()
{
}

// ============================================================================
// ChainList

template <typename T>
ChainList<T>::ChainList()
{
    // make the root circular
    root.next = &root;
    root.prev = &root;
}

template <typename T>
ChainList<T>::~ChainList()
{
}

} /* namespace miko */

#endif /* MIKO_CHAIN_INL */

