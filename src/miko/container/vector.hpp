#ifndef MIKO_VECTOR_HPP
#define MIKO_VECTOR_HPP

#include "miko/memory/memory.hpp"

namespace miko {

template <typename T>
class Vector
{
public:
    typedef T ElementType;

    Vector(Allocator &allocator=defaultAllocator);
    ~Vector();

    bool resize(uint32_t count);
    bool reserve(uint32_t count);

    void clear();

    bool empty() const { return _size == 0; }

    uint32_t size() const { return _size; }
    uint32_t reserved() const { return _reserved; }

    T & front()
    {
        return elems[0];
    }

    const T & front() const
    {
        return elems[0];
    }

    T & back()
    {
        return elems[_size - 1];
    }

    const T & back() const
    {
        return elems[_size - 1];
    }

    bool append(T &t) MIKO_WARN_UNUSED();
    bool append(const T &t) MIKO_WARN_UNUSED();

    void pop();

    void remove(uint32_t i);

    T & operator [] (uint32_t i)
    {
        return elems[i];
    }

    const T & operator [] (uint32_t i) const
    {
        return elems[i];
    }

    Allocator & getAllocator()
    {
        return allocator;
    }

    bool contains(const T& value) const;

    int find(const T& value) const;

private:
    bool emplace(uint32_t count);
    bool growTo(uint32_t size);

    uint32_t _size;
    uint32_t _reserved;
    T *elems;
    Allocator &allocator;

};
    
} /* namespace miko */

#include "miko/container/vector.inl"

#endif /* MIKO_VECTOR_HPP */

