#ifndef MIKO_CHAIN_HPP
#define MIKO_CHAIN_HPP

// Chain is an intrusive list which not really a container
// but fits in the container module

namespace miko {

template <typename T>
class ChainLink
{
friend class ChainList;

public:
    ChainLink();
    ~ChainLink();

    void linkAfter(ChainLink<T> &n);
    void linkBefore(ChainLink<T> &n);

    void unlink();

private:
    void linkBetween(ChainLink<T> &prev, ChainLink<T> &next);

    ChainLink<T> *next;
    ChainLink<T> *prev;
};

template <typename T>
class ChainList
{
public:
    ChainList();
    ~ChainList();

private:
    ChainLink<T> root;

};
    
} /* namespace miko */

#include "miko/container/chain.inl"

#endif /* MIKO_CHAIN_HPP */

