#ifndef MIKO_VECTOR_INL
#define MIKO_VECTOR_INL

#include "miko/container/vector.hpp"
#include "miko/util/assert.hpp"

namespace miko {

template <typename T>
Vector<T>::Vector(Allocator &allocator):
    _size(0),
    _reserved(0),
    elems(0),
    allocator(allocator)
{
}

template <typename T>
Vector<T>::~Vector()
{
    for (uint32_t i = 0; i < _size; i++) {
        elems[i].~T();
    }

    allocator.free(elems);
}

template <typename T>
bool Vector<T>::resize(uint32_t count)
{
    // Grow
    if (count > _size) {
        if (!growTo(count)) {
            return false;
        }

        return emplace(count - _size);
    }

    // Shrink?
    if (count < _size) {
        // pop the required number of elements from back
        uint32_t toRemove = _size - count;
        for (uint32_t i = 0; i < toRemove; i++) {
            pop();
        }

        return true;
    }

    // Nothing to do
    return true;
}

template <typename T>
bool Vector<T>::reserve(uint32_t count)
{
    if (_reserved >= count) {
        return true;
    }

    T *oldElems = elems;

    T *newElems = reinterpret_cast<T*>(allocator.alloc(count * sizeof(T)));
    if (!newElems) {
        mikoAssert("Vector out of memory");
        return false;
    }

    /* copy construct the new elements */
    for (uint32_t i = 0; i < _size; i++) {
        new (&newElems[i]) T(oldElems[i]);
    }

    /* destruct the old elements */
    for (uint32_t i = 0; i < _size; i++) {
        oldElems[i].~T();
    }

    allocator.free(oldElems);
    elems = newElems;
    _reserved = count;

    return true;
}

template <typename T>
void Vector<T>::clear()
{
    for (uint32_t i = 0; i < _size; i++) {
        elems[i].~T();
    }

    _size = 0;
}

template <typename T>
bool Vector<T>::append(T &t)
{
    if (!growTo(_size + 1)) {
        return false;
    }

    new (&elems[_size]) T(t);
    _size++;

    return true;
}

template <typename T>
bool Vector<T>::append(const T &t)
{
    if (!growTo(_size + 1)) {
        return false;
    }

    new (&elems[_size]) T(t);
    _size++;

    return true;
}

template <typename T>
bool Vector<T>::emplace(uint32_t count)
{
    mikoAssert(_size + count <= _reserved);

    for (unsigned int i = 0; i < count; i++) {
        new (&elems[_size]) T();
        _size++;
    }

    return true;
}

template <typename T>
bool Vector<T>::growTo(uint32_t size)
{
    if (_reserved >= size) {
        return true;
    }

    // lets try initial capacity of at least 8
    // after that grow exponentially until required size is found
    uint32_t newsize = _size == 0 ? 8 : _size;
    while (newsize < size) {
        newsize *= 2;
    }

    return reserve(newsize);
}

template <typename T>
void Vector<T>::pop()
{
    if (_size == 0) {
        return;
    }

    elems[_size - 1].~T();
    _size--;
}

template <typename T>
void Vector<T>::remove(uint32_t i)
{
    mikoAssert(false && "unimplemented");
}

template <typename T>
bool Vector<T>::contains(const T& value) const
{
    return find(value) >= 0;
}

template <typename T>
int Vector<T>::find(const T& value) const
{
    for (unsigned int i = 0; i < _size; i++) {
        if (elems[i] == value) {
            return i;
        }
    }

    return -1;
}
    
} /* namespace miko */

#endif /* MIKO_VECTOR_INL */

