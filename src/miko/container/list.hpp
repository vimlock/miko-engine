#ifndef MIKO_LIST_HPP
#define MIKO_LIST_HPP

#include "miko/memory/memory.hpp"

namespace miko {

/// Simple doubly linked list
template <typename T>
class List
{
private:
    friend class Iterator;

    class Node
    {
    public:
        Node(T &value);
        Node(const T &value);

        Node *next;
        Node *prev;
        T value;
    };

public:
    class Iterator
    {
    public:
        Iterator();

        T& operator * ();
        const T& operator * () const;

        T* operator -> ();
        const T* operator -> () const;

        Iterator& operator ++ ();
        Iterator& operator -- ();

        Iterator operator ++ (int);
        Iterator operator -- (int);

        bool operator == (const Iterator &other) const;
        bool operator != (const Iterator &other) const;

    private:
        friend class List;

        Iterator(List<T> *list, Node *node);

        mutable List<T> *list;
        mutable Node *node;
        uint32_t checksum;
    };

    List(Allocator &allocator=defaultAllocator);
    ~List();

    /// Get iterator to the first node
    Iterator begin();

    /// Get iterator after the last node
    Iterator end();

    /// Insert node at the end of the list
    void append(const T& value);

    /// Insert node at the beginning of the list
    void prepend(const T& value);

    /// Insert node after the given iterator
    void insertAfter(const Iterator &offset, const T& value);

    /// Insert node befofre the given iterator
    void insertBefore(const Iterator &offset, const T& value);

    /// Access the first value in the list, if the list is empty
    /// undefined behaviour occurs
    T & first();
    const T & first() const;

    /// Access the last value in the list, if the list is empty
    /// undefined behaviour occurs
    T & last ();
    const T & last () const;

    /// Remove all nodes
    void clear();

    /// Remove the last node.
    /// If the list is empty, nothing happens
    void removeLast();

    /// Remove the first node
    /// If the list is empty, nothing happens
    void removeFirst();

    Iterator erase(const Iterator &offset);

    bool empty() const
    {
        return size() == 0;
    }

    uint32_t size() const;

private:
    void insertBetween(Node *prev, Node *next, const T& value);
    void invalidateIterators();
    void validateIterator(const Iterator &iter);

    /// First node in the list
    Node *head;

    /// Last node in the list
    Node *tail;

    /// Number of nodes in the list
    uint32_t count;

    /// Incremented every time iterators are invalidated
    uint32_t checksum;

    Allocator &allocator;
};
    
} /* namespace miko */

#include "miko/container/list.inl"

#endif /* MIKO_LIST_HPP */

