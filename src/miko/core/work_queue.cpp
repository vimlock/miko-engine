#include "miko/core/work_queue.hpp"
#include "miko/memory/memory.hpp"

#include "miko/io/log.hpp"

#include <SDL2/SDL_timer.h>

namespace miko {

Worker::Worker(WorkQueue *wq, int id)
{
    this->workQueue = wq;
    this->id = id;

    char name[32];
    snprintf(name, sizeof(name), "miko-worker-%d", id);

    thread = SDL_CreateThread(bootstrap, name, this);
    if (!thread) {
        LOG_ERRORF("failed to create thread: %s", SDL_GetError());
    }
    else {
        LOG_INFOF("created worker \"%s\"", name);
    }
}

Worker::~Worker()
{
}

int Worker::wait()
{
    int status;
    LOG_INFOF("terminating \"%s\"", getName());
    SDL_WaitThread(thread, &status);
    return status;
}

const char * Worker::getName()
{
    return SDL_GetThreadName(thread);
}

void Worker::main()
{
    Task *task;
    while ((task = workQueue->getTask())) {
        task->run();

        SDL_LockMutex(workQueue->taskLock);
        SDL_AtomicDecRef(&workQueue->numWorking);
        SDL_UnlockMutex(workQueue->taskLock);
    }
}

int Worker::bootstrap(void *ptr)
{
    SDL_SetThreadPriority(SDL_THREAD_PRIORITY_LOW);
    reinterpret_cast<Worker *>(ptr)->main();
    return 0;
}

WorkQueue::WorkQueue(int numWorkers):
    workers(defaultAllocator),
    tasks(defaultAllocator)
{
    workerSema = SDL_CreateSemaphore(0);
    taskLock = SDL_CreateMutex();

    SDL_AtomicSet(&finished, 0);
    SDL_AtomicSet(&numWorking, 0);

    for (int i = 0; i < numWorkers; i++) {
        Worker *w = MIKO_NEW(defaultAllocator, Worker) (this, i);

        try {
            workers.append(w);
        }
        catch (...) {
            MIKO_DELETE(defaultAllocator, w);
            throw;
        }
    }
}

WorkQueue::~WorkQueue()
{
    SDL_AtomicSet(&finished, 1);

    // wake all of the workers 
    for (unsigned i = 0; i < workers.size(); i++) {
        SDL_SemPost(workerSema);
    }

    waitAll();

    SDL_DestroySemaphore(workerSema);
    SDL_DestroyMutex(taskLock);
}

void WorkQueue::update()
{
}

void WorkQueue::addTask(Task *t)
{
    mikoAssert(SDL_LockMutex(taskLock) == 0);
    tasks.append(t);

    mikoAssert(SDL_SemPost(workerSema) == 0);
    mikoAssert(SDL_UnlockMutex(taskLock) == 0);
}

void WorkQueue::waitComplete()
{
    while (SDL_AtomicGet(&numWorking) > 0 || !tasks.empty()) {
        // yield CPU to other threads
        SDL_Delay(1);
    }
}

Task * WorkQueue::getTask()
{
    SDL_SemWait(workerSema);

    if (SDL_AtomicGet(&finished)) {
        return NULL;
    }

    if (SDL_LockMutex(taskLock) != 0) {
        LOG_ERRORF("failed to acquire mutex %s", SDL_GetError());
        return NULL;
    }

    mikoAssert(tasks.size() > 0);

    Task *t = tasks.back();
    tasks.pop();

    SDL_AtomicIncRef(&numWorking);

    mikoAssert(SDL_UnlockMutex(taskLock) == 0);

    return t;
}

void WorkQueue::waitAll()
{
    for (unsigned i = 0; i < workers.size(); i++) {
        workers[i]->wait();
        MIKO_DELETE(defaultAllocator, workers[i]);
        workers[i] = NULL;
    }

    workers.clear();
}
    
} /* namespace miko */
