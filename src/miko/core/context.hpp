#ifndef MIKO_CORE_CONTEXT_HPP
#define MIKO_CORE_CONTEXT_HPP

#include "miko/memory/memory.hpp"
#include "miko/core/work_queue.hpp"
#include "miko/resource/resource_manager.hpp"

#include "miko/graphics/gl/mesh.hpp"
#include "miko/graphics/gl/shader.hpp"
#include "miko/graphics/gl/program.hpp"
#include "miko/graphics/animation.hpp"
#include "miko/graphics/sprite.hpp"

namespace miko {

class Context
{
public:
    Context();

    ResourceManager * getResourceManager()
    {
        return &resourceManager;
    }

private:
    WorkQueue workers;
    ResourceManager resourceManager;

    TextureFactory textureFactory;
    MaterialFactory materialFactory;
    GlShaderSourceFactory shaderSourceFactory;
    GlShaderProgramFactory shaderProgramFactory;
    GlMeshFactory meshFactory;
    SpriteAnimationFactory spriteAnimFactory;
    SkeletonAnimationFactory animFactory;
};
    
} /* namespace miko */

#endif /* MIKO_CORE_CONTEXT_HPP */

