#ifndef MIKO_WORK_QUEUE_HPP
#define MIKO_WORK_QUEUE_HPP

#include "miko/container/vector.hpp"

#include <SDL2/SDL_mutex.h>
#include <SDL2/SDL_thread.h>

namespace miko {

class WorkQueue;
class Worker;

class Task
{
public:
    virtual void run() = 0;

private:
};

class Worker
{
public:
    Worker(WorkQueue *wq, int id);
    ~Worker();

    int wait();

    const char * getName();

private:
    void main();
    static int bootstrap(void *ptr);

    WorkQueue *workQueue;
    SDL_Thread *thread;
    int id;
};


class WorkQueue
{
friend class Worker;

public:
    WorkQueue(int numWorkers);
    ~WorkQueue();

    void update();
    void addTask(Task *t);

    void waitComplete();
    bool isComplete();

private:
    Task * getTask();

    void waitAll();

    Vector<Worker*> workers;
    Vector<Task*> tasks;

    SDL_atomic_t numWorking;

    SDL_atomic_t finished;

    SDL_semaphore *workerSema;
    SDL_mutex *taskLock;
    
};
    
} /* namespace miko */

#endif /* MIKO_WORK_QUEUE_HPP */

