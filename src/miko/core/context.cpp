#include "miko/core/context.hpp"

namespace miko {

Context::Context():
    workers(1),
    resourceManager(defaultAllocator, &workers)
{
    resourceManager.addFactory(RESOURCE_TEXTURE, &textureFactory);
    resourceManager.addFactory(RESOURCE_MATERIAL, &materialFactory);
    resourceManager.addFactory(RESOURCE_SHADER_PROGRAM, &shaderProgramFactory);
    resourceManager.addFactory(RESOURCE_SHADER_SOURCE, &shaderSourceFactory);
    resourceManager.addFactory(RESOURCE_SPRITE_ANIMATION, &spriteAnimFactory);
    resourceManager.addFactory(RESOURCE_MESH, &meshFactory);
    resourceManager.addFactory(RESOURCE_MESH_ANIMATION, &animFactory);
}
    
} /* namespace miko */
