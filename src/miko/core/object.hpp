#ifndef MIKO_CORE_OBJECT_HPP
#define MIKO_CORE_OBJECT_HPP

#include "miko/util/refcounted.hpp"
#include "miko/memory/memory.hpp"

namespace miko {

enum ObjectAttrType
{
    ATTR_FLOAT,
    ATTR_INT8,
    ATTR_INT16,
    ATTR_INT32,
    ATTR_UINT8,
    ATTR_UINT16,
    ATTR_UINT32,
    ATTR_ENUM
};

class ObjectAttr
{
public:
    const char *name;
    ObjectAttrType type;
    uint32_t offset;

    union {
        const char **enumNames;
    };
};

class ObjectType
{
public:
    const char *name;
    ObjectType *base;

    uint32_t size;
};

class Object : public Refcounted
{
public:
private:

};
    
} /* namespace miko */

#endif /* MIKO_CORE_OBJECT_HPP */

