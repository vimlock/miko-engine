#ifndef MIKO_HPP
#define MIKO_HPP

#include <stddef.h>
#include <stdint.h>

#if defined(MIKO_GRAPHICS_OPENGL) && defined(MIKO_GRAPHICS_DX9)
    #error "invalid renderer"
#endif

// GCC specific compiler options
#ifdef __GNUC__
#define MIKO_DEPRECATED(msg) \
    __attribute__ ((deprecated (msg)))

#define MIKO_PRINTF_FORMAT(str, arg) \
    __attribute__ ((format (printf, str, arg)))

#define MIKO_WARN_UNUSED() \
    __attribute__ ((warn_unused_result))

#else
#define MIKO_DEPRECATED(msg)
#define MIKO_PRINTF_FORMAT(format firstArg)
#define MIKO_WARN_UNUSED()

#endif

// Not used for anything usefull yet but will be used to
// specify dynamic linking attributes
#define MIKO_API

#endif /* MIKO_HPP */

