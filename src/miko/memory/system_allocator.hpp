#ifndef MIKO_SYSTEM_ALLOCATOR_HPP
#define MIKO_SYSTEM_ALLOCATOR_HPP

#include "miko/memory/allocator.hpp"

namespace miko {

// Allocator which calls for more memory from operating system
class SystemAllocator : public Allocator
{
public:
    SystemAllocator();

    virtual ~SystemAllocator();

    virtual void * alloc(uint32_t size);
    virtual void free(void *ptr);

    virtual const char * name() const
    {
        return "SystemAllocator";
    }
};
    
} /* namespace miko */

#endif /* MIKO_SYSTEM_ALLOCATOR_HPP */

