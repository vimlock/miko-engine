#ifndef MIKO_MEMORY_HPP
#define MIKO_MEMORY_HPP

#include "miko/memory/allocator.hpp"

// enable placement new support 
#include <new>

#define MIKO_NEW(allocator, type) new (allocator.alloc(sizeof(type))) type
#define MIKO_DELETE(allocator, ptr) (destruct(allocator, ptr))

namespace miko {

template <typename T>
void destruct(Allocator &allocator, T *ptr)
{
    if (!ptr) {
        return;
    }

    ptr->~T();
    allocator.free(ptr);
}

class StackAllocator;
class PoolAllocator;
class SystemAllocator;

extern Allocator &defaultAllocator;
extern Allocator &refcountedAllocator;
extern Allocator &tempAllocator;

extern StackAllocator stackAllocator;
extern PoolAllocator poolAllocator;
extern SystemAllocator systemAllocator;

} /* namespace miko */

#endif /* MIKO_MEMORY_HPP */

