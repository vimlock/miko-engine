#include "miko/memory/pool_allocator.hpp"

namespace miko {

PoolAllocator::PoolAllocator(Allocator *backer)
{
    this->backer = backer;
}

PoolAllocator::~PoolAllocator()
{
}

void * PoolAllocator::alloc(uint32_t size)
{
    return backer->alloc(size);
}

void PoolAllocator::free(void *ptr)
{
    backer->free(ptr);
}
    
} /* namespace miko */
