#include "miko/memory/memory.hpp"

#include "miko/memory/stack_allocator.hpp"
#include "miko/memory/pool_allocator.hpp"
#include "miko/memory/system_allocator.hpp"

namespace miko {

SystemAllocator systemAllocator;
StackAllocator stackAllocator(&systemAllocator);
PoolAllocator poolAllocator(&systemAllocator);

Allocator &defaultAllocator = poolAllocator;
Allocator &tempAllocator = stackAllocator;
Allocator &refcountedAllocator = poolAllocator;
    
} /* namespace miko */
