#include "miko/memory/system_allocator.hpp"

#include <stdlib.h>

namespace miko {

SystemAllocator::SystemAllocator()
{
}

SystemAllocator::~SystemAllocator()
{
}

void * SystemAllocator::alloc(uint32_t size)
{
    return malloc(size);
}

void SystemAllocator::free(void *ptr)
{
    if (ptr) {
        ::free(ptr);
    }
}

    
} /* namespace miko */
