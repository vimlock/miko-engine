#ifndef MIKO_ALLOCATOR_HPP
#define MIKO_ALLOCATOR_HPP

#include "miko/miko.hpp"
#include "miko/util/nocopy.hpp"

namespace miko {

struct AllocHeader
{
    const char *tag;
    uint32_t size;
};

class Allocator : public nocopy
{
public:
    virtual ~Allocator()
    {
        // empty
    }

    virtual void * alloc(uint32_t size) MIKO_WARN_UNUSED() = 0 ;
    virtual void free(void *ptr) = 0;

    virtual const char *name() const = 0;
};
    
} /* namespace miko */

#endif /* MIKO_ALLOCATOR_HPP */

