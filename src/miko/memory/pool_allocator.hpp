#ifndef MIKO_POOL_ALLOCATOR_HPP
#define MIKO_POOL_ALLOCATOR_HPP

#include "miko/memory/allocator.hpp"

namespace miko {

class PoolAllocator : public Allocator
{
public:
    PoolAllocator(Allocator *backer);
    virtual ~PoolAllocator();

    virtual void * alloc(uint32_t size);
    virtual void free(void *ptr);

    virtual const char *name() const
    {
        return "PoolAllocator";
    }

private:
    Allocator *backer;

};
    
} /* namespace miko */

#endif /* MIKO_POOL_ALLOCATOR_HPP */

