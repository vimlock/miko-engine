#ifndef MIKO_STACK_ALLOCATOR_HPP
#define MIKO_STACK_ALLOCATOR_HPP

#include "miko/memory/allocator.hpp"

namespace miko {

class StackAllocator : public Allocator
{
public:
    StackAllocator(Allocator *backer);

    virtual void * alloc(uint32_t size);
    virtual void free(void *ptr);

    void clear();

    virtual const char *name() const
    {
        return "StackAllocator";
    }

private:
    Allocator *backer;
    char *bot;
    char *top;
    uint32_t size;
};
    
} /* namespace miko */

#endif /* MIKO_STACK_ALLOCATOR_HPP */

