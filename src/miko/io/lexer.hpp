#ifndef MIKO_LEXER_HPP
#define MIKO_LEXER_HPP

#include <SDL2/SDL_rwops.h>

namespace miko {

class Token
{
public:
    enum Type
    {
        IDENTIFIER = 1,
        STRING     = 2,
        INT        = 4,
        FLOAT      = 8,

        NEWLINE    = 16,
        LCURLY     = 32,
        RCURLY     = 64,
        LPAREN     = 128,
        RPAREN     = 512,

        SEMICOLON  = 1024,

        ERROR      = 4096,
        END        = 8192
    };

    static const char * typeStr(Type type);

    Type type;
    const char *str;
    unsigned int length;
    unsigned int line;
    unsigned int column;
};

class Lexer
{
public:
    enum NewlineMode {
        OMIT,
        EMIT
    };

    Lexer(SDL_RWops *rwops, char *buf, uint32_t bufSize);

    void setNewlineMode(NewlineMode mode);
    NewlineMode getNewlineMode() const;

    const char * getError() const;

    void advance(Token *token);

private:
    void readToken();

    void next();
    void read();

    void error(const char *msg);

    void readIdentifier();
    void readNumber();
    void readString();
    void readWhitespace();

    char *buf;
    uint32_t bufSize;
    uint32_t bufNext;
    Token::Type type;

    unsigned int line;
    unsigned int column;

    int current;

    SDL_RWops *rwops;

    NewlineMode newlineMode;

    const char *errorMsg;
};
    
} /* namespace miko */

#endif /* MIKO_LEXER_HPP */

