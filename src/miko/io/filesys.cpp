#include "miko/io/filesys.hpp"
#include "miko/io/log.hpp"

#include "miko/util/assert.hpp"

#include <sys/stat.h>
#include <sys/param.h>
#include <errno.h>
#include <stdlib.h>

#include <SDL2/SDL_error.h>
#include <SDL2/SDL_filesystem.h>


namespace miko {

bool tryStat(const char *path, struct stat *st)
{
    if (stat(path, st) == 0) {
        return true;
    }

    if (errno != ENOENT) {
        LOG_DEBUGF("failed to stat \"%s\"", path);
    }

    return false;
}

bool fileExists(const char *path)
{
    struct stat st;
    return tryStat(path, &st);
}

bool fileExists(const std::string &path)
{
    return fileExists(path.c_str());
}

bool fileIsRegular(const char *path)
{
    struct stat st;
    if (!tryStat(path, &st)) {
        return false;
    }

    return S_ISREG(st.st_mode);
}

bool fileIsRegular(const std::string &path)
{
    return fileIsRegular(path.c_str());
}

bool fileIsDir(const char *path)
{
    struct stat st;
    if (!tryStat(path, &st)) {
        return false;
    }

    return S_ISDIR(st.st_mode);
}

bool fileIsDir(const std::string &path)
{
    return fileIsDir(path.c_str());
}

std::string getProgramDirectory()
{
    const char *path = SDL_GetBasePath();
    if (!path) {
        LOG_ERRORF("failed to get base path: %s", SDL_GetError());
        return "";
    }

    std::string tmp(path);
    SDL_free(const_cast<char *>(path));
    return tmp;
}

std::string fileBasePath(const std::string &path)
{
    mikoAssert(false && "unimplemented");
}

std::string fileNameExtension(const std::string &path)
{
    mikoAssert(false && "unimplemented");
}

std::string fileNameWithoutExtension(const std::string &path)
{
    size_t lastdot = path.find_last_of('.');
    if (lastdot == std::string::npos) {
        return path;
    }
    else {
        return path.substr(0, lastdot);
    }
}

std::string fileAbsPath(const std::string &relative)
{
    char tmp[PATH_MAX];

    const char * res = realpath(relative.c_str(), tmp);
    if (!res) {
        LOG_ERRORF("failed to resolve absolute path for \"%s\"", relative.c_str());
        return "";
    }
    else {
        return std::string(tmp);
    }
}
    
} /* namespace miko */
