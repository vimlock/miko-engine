#include "miko/io/lexer.hpp"
#include "miko/util/assert.hpp"

#include <ctype.h>

namespace miko {

const char * Token::typeStr(Type type)
{
    switch (type) {
    case IDENTIFIER: return "identifier";
    case STRING:     return "string";
    case INT:        return "integer";
    case FLOAT:      return "float";
    case NEWLINE:    return "newline";
    case LCURLY:     return "'{'";
    case RCURLY:     return "'}'";
    case LPAREN:     return "'('";
    case RPAREN:     return "')'";
    case SEMICOLON:  return "';'";
    case END:        return "<end>";
    case ERROR:      return "error";
    }

    mikoUnreachable();
    return "error";
}

Lexer::Lexer(SDL_RWops *rwops, char *buf, uint32_t bufSize)
{
    mikoAssert(rwops != NULL);
    mikoAssert(buf != NULL);
    mikoAssert(bufSize > 0);

    this->buf = buf;
    this->bufSize = bufSize;

    this->line = 1;
    this->column = 0;
    this->rwops = rwops;
    this->newlineMode = OMIT;
    this->errorMsg = NULL;
    this->type = Token::ERROR;
    this->current = '\0';

    next();
}

void Lexer::setNewlineMode(NewlineMode mode)
{
    newlineMode = mode;
}

Lexer::NewlineMode Lexer::getNewlineMode() const
{
    return newlineMode;
}

const char * Lexer::getError() const
{
    return errorMsg;
}

void Lexer::next()
{
    if (current < 0) {
        return;
    }

    char ch;
    if (SDL_RWread(rwops, &ch, 1, 1) > 0) {
        current = ch;

        if (current == '\n') {
            line++;
            column = 1;
        }
        else {
            column++;
        }
    }
    else {
        current = -1;
    }
}

void Lexer::read()
{
    if (bufNext < bufSize) {
        buf[bufNext] = current;
        bufNext++;
    }
    else {
        error("Too long token");
    }
}

void Lexer::error(const char *msg)
{
    errorMsg = msg;
    type = Token::ERROR;
    current = -1;
}

void Lexer::readIdentifier()
{
    read();
    next();

    while (isdigit(current) || isalpha(current) || current == '_') {
        read();
        next();
    }

    type = Token::IDENTIFIER;
}

void Lexer::readNumber()
{
    read();

    /* prevent stupid stuff like 000, 001.0 and so forth */
    if (current == '0') {
        next();
        if (isdigit(current)) {
            error("unexpected character");
            return;
        }
    }
    else {
        next();
    }

    /* read the integer part */
    while (isdigit(current)) {
        read();
        next();
    }

    if (current != '.') {
        type = Token::INT;
    }
    else {
        /* read the dot */
        read();
        next();

        /* read at least one digit */
        if (!isdigit(current)) {
            error("unexpected charater");
            return;
        }

        while (isdigit(current)) {
            read();
            next();
        }

        type = Token::FLOAT;
    }
}

void Lexer::readString()
{
    int escape = 0;
    
    while (1) {
        if (current < 0) {
            error("unterminated string");
            return;
        }

        if (escape) {
            switch (current) {
            case '"': read(); break;
            case '\\': read(); break;
            default:
                error("bad escape character");
                return;
            }

            escape = 0;
        }
        else {
            if (current == '\\') {
                escape = 1;
            }
            else if (current == '"') {
                next();
                type = Token::STRING;
                return;
            }
            else {
                read();
            }
        }

        next();
    }
}

void Lexer::readWhitespace()
{
    while (1) {
        if (newlineMode == OMIT && current == '\n') {
            next();
        }
        else if (current == ' ' || current == '\t' || current == '\r') {
            next();
        }
        else {
            break;
        }
    }
}

void Lexer::advance(Token *token)
{
    readWhitespace();

    unsigned int startLine = line;
    unsigned int startColumn = column;

    readToken();

    // append null terminator to the token
    if (bufNext < bufSize) {
        buf[bufNext] = '\0';
    }
    else {
        error("Too long token");
    }

    token->type = type;
    token->str = buf;
    token->length = bufNext;
    token->line = startLine;
    token->column = startColumn;
}

void Lexer::readToken()
{
    bufNext = 0;

    /* Error or eof? abort */
    if (current < 0) {
        type = Token::END;
        return;
    }

    /* Identifier token */
    else if (isalpha(current)) {
        readIdentifier();
    }

    /* String token */
    else if (current == '"') {
        next();
        readString();
        return;
    }

    /* integer and float numbers */
    else if (isdigit(current)) {
        readNumber();
    }

    /* negative numbers */
    else if (current == '-') {
        read();
        next();
        readNumber();
    }

    /* one character tokens */
    else {
        switch (current) {
        case '\n': type = Token::NEWLINE; break;
        case '{': type = Token::LCURLY; break;
        case '}': type = Token::RCURLY; break;
        case '(': type = Token::LPAREN; break;
        case ')': type = Token::RPAREN; break;
        case ';': type = Token::SEMICOLON; break;
        default:
            error("unexpected character");
            return;
        }

        read();
        next();
    }
}

} /* namespace miko */
