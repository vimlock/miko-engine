#include "miko/io/text_serializer.hpp"
#include "miko/io/log.hpp"

#include <stdarg.h>

namespace miko {

void TextSerializer::writeRaw(char ch)
{
    size_t nwriten = SDL_RWwrite(rwops, &ch, 1, 1);
    assert(nwriten == 1);
}

void TextSerializer::writeRaw(const char *str)
{
    size_t len = strlen(str);
    size_t nwriten = SDL_RWwrite(rwops, str, 1, len);
    assert (nwriten == len);
}

void TextSerializer::writeFmt(const char *fmt, ...)
{
    char buf[128];

    va_list ap;
    va_start(ap, fmt);

    int len = vsnprintf(buf, sizeof(buf), fmt, ap);
    assert(len >= 0 && (size_t)len < sizeof(buf));

    va_end(ap);

    size_t nwriten = SDL_RWwrite(rwops, buf, 1, len);
    assert(nwriten == (size_t)len);
}

void TextSerializer::writeString(const char *str, int len)
{
    if (len < 0) {
        len = strlen(str);
    }

    writeRaw('"');

    for (int i = 0; i < len; i++) {
        switch (str[i]) {
        case '"':
            writeRaw('\\');
            writeRaw('"');
            break;
        case '\n':
            writeRaw('\\');
            writeRaw('n');
            break;
        case '\t':
            writeRaw('\\');
            writeRaw('t');
            break;
        case '\\':
            writeRaw('\\');
            writeRaw('\\');
            break;
        default:
            writeRaw(str[i]);
        }
    }

    writeRaw('"');
}

void TextSerializer::writeString(const std::string &str)
{
    writeString(str.c_str(), str.size());
}

void TextSerializer::writeIdentifier(const std::string &str)
{
    writeRaw(str.c_str());
}

bool TextSerializer::writeEnum(const EnumInfo *infos, int value)
{
    const EnumInfo *iter = infos;

    while (iter->name) {
        if (iter->value == value) {
            writeString(infos->name);
            return true;
        }

        iter++;
    }

    // No matching enum info with matching value?
    // TODO: We could handle this case by encoding it as an integer

    LOG_ERRORF("can't encode enum with value %d", value);

    return false;
}

void TextSerializer::writeWhitespace()
{
    writeRaw(' ');
}

void TextSerializer::writeNewline()
{
    writeRaw('\r');
    writeRaw('\n');
    writeIndent();
}

void TextSerializer::writeIndent()
{
    if (!pretty) {
        return;
    }

    for (int i = 0; i < indentLevel; i++) {
        writeRaw('\t');
    }
}

void TextSerializer::indent()
{
    indentLevel++;
    writeNewline();
}

void TextSerializer::dedent()
{
    mikoAssert(indentLevel > 0);
    indentLevel--;
    writeNewline();
}


} /* namespace miko */
