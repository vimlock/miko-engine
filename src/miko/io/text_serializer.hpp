#ifndef MIKO_IO_TEXT_SERIALIZER_HPP
#define MIKO_IO_TEXT_SERIALIZER_HPP

#include "miko/io/serialize.hpp"
#include "miko/container/vector.hpp"
#include "miko/container/hash_map.hpp"
#include "miko/io/lexer.hpp"
#include "miko/io/log.hpp"
#include "miko/util/assert.hpp"
#include "miko/util/refcounted.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include <string>

namespace miko {

class TextSerializer : public ISerializer
{
public:
    TextSerializer(SDL_RWops *rwops, bool pretty)
    {
        this->rwops = rwops;
        this->pretty = pretty;
        this->indentLevel = 0;
    }

    template <typename T>
    inline void write(const T &value);

    void writeRaw(char ch);
    void writeRaw(const char *str);
    void writeFmt(const char *fmt, ...);

    void writeString(const char *str, int len=-1);
    void writeString(const std::string &str);
    void writeIdentifier(const std::string &str);
    bool writeEnum(const EnumInfo *infos, int value);

    void writeWhitespace();
    void writeNewline();
    void writeIndent();

    void indent();
    void dedent();

private:
    SDL_RWops *rwops;
    bool pretty;

    int indentLevel;

};

template <>
inline void TextSerializer::write(const int32_t &value)
{
    writeFmt("%d", value);
}

template <>
inline void TextSerializer::write(const int16_t &value)
{
    writeFmt("%d", value);
}

template <>
inline void TextSerializer::write(const int8_t &value)
{
    writeFmt("%d", value);
}

template <>
inline void TextSerializer::write(const uint32_t &value)
{
    writeFmt("%u", value);
}

template <>
inline void TextSerializer::write(const uint16_t &value)
{
    writeFmt("%u", value);
}

template <>
inline void TextSerializer::write(const uint8_t &value)
{
    writeFmt("%u", value);
}

template <>
inline void TextSerializer::write(const float &value)
{
    writeFmt("%10f", value);
}

template <>
inline void TextSerializer::write(const char * const &value)
{
    writeRaw(value);
}

template <>
inline void TextSerializer::write(const std::string &value)
{
    writeRaw('"');
    writeRaw(value.c_str());
    writeRaw('"');
}

template <>
inline void TextSerializer::write(const glm::vec2 &value)
{
    writeRaw('(');
    writeWhitespace();
    write(value.x);
    writeWhitespace();
    write(value.y);
    writeWhitespace();
    writeRaw(')');
}

template <>
inline void TextSerializer::write(const glm::vec3 &value)
{
    writeRaw('(');
    writeWhitespace();
    write(value.x);
    writeWhitespace();
    write(value.y);
    writeWhitespace();
    write(value.z);
    writeWhitespace();
    writeRaw(')');
}

template <>
inline void TextSerializer::write(const glm::vec4 &value)
{
    writeRaw('(');
    writeWhitespace();
    write(value.x);
    writeWhitespace();
    write(value.y);
    writeWhitespace();
    write(value.z);
    writeWhitespace();
    write(value.w);
    writeWhitespace();
    writeRaw(')');
}

template <>
inline void TextSerializer::write(const glm::quat &value)
{
    writeRaw('(');
    write(value.w);
    writeWhitespace();
    write(value.x);
    writeWhitespace();
    write(value.y);
    writeWhitespace();
    write(value.z);
    writeWhitespace();
    writeRaw(')');
}

template <>
inline void TextSerializer::write(const glm::mat3 &value)
{
    writeRaw('(');
    writeWhitespace();

    for (unsigned int i = 0; i < 3; i++) {
        write(value[i]);
        writeWhitespace();
    }

    writeRaw(')');
}

template <>
inline void TextSerializer::write(const glm::mat4 &value)
{
    writeRaw('(');
    writeWhitespace();

    for (unsigned int i = 0; i < 4; i++) {
        write(value[i]);
        writeWhitespace();
    }

    writeRaw(')');
}

} /* namespace miko */

#endif /* MIKO_IO_TEXT_SERIALIZER_HPP */

