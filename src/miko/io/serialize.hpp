#ifndef MIKO_SERIALIZER_HPP
#define MIKO_SERIALIZER_HPP

#include "miko/miko.hpp"

#include <exception>

namespace miko {

struct EnumInfo
{
    const char *name;
    int value;
};

class ISerializer
{
public:
    virtual ~ISerializer()
    {
        // empty
    }

    template <typename T>
    void write(const char *name, T &value);
};

class IDeserializer
{
public:
    virtual ~IDeserializer()
    {
        // empty
    }

};

} /* namespace miko */

#endif /* MIKO_SERIALIZER_HPP */

