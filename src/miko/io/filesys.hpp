#ifndef MIKO_IO_FILESYS_HPP
#define MIKO_IO_FILESYS_HPP

#include <string>

namespace miko {

bool fileExists(const char *path);
bool fileExists(const std::string &path);

bool fileIsRegular(const char *path);
bool fileIsRegular(const std::string &path);

bool fileIsDir(const char *path);
bool fileIsDir(const std::string &path);

std::string getProgramDirectory();

std::string fileBasePath(const std::string &path);
std::string fileNameExtension(const std::string &path);
std::string fileNameWithoutExtension(const std::string &path);

/// Returns the absolute path to the given file
std::string fileAbsPath(const std::string &relative);
    
} /* namespace miko */

#endif /* MIKO_IO_FILESYS_HPP */

