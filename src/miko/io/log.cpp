#include "miko/io/log.hpp"
#include "miko/util/assert.hpp"

#include <string.h>
#include <stdio.h>

#include <SDL2/SDL_mutex.h>

namespace miko {

#define MAX_LOG_HANDLERS 10
#define MAX_LOG_FORMAT_LENGTH 4096

static LogHandler *logHandlers[MAX_LOG_HANDLERS] = { 0 };
static SDL_mutex * logLock = SDL_CreateMutex();

LogHandler::~LogHandler()
{
    remove();
}

void LogHandler::listen()
{
    // check if we are already listening
    for (int i = 0; i < MAX_LOG_HANDLERS; i++) {
        if (logHandlers[i] == this) {
            // we are huh? nothing to do here then
            return;
        }
    }
    
    // no then? lets add ourselves
    for (int i = 0; i < MAX_LOG_HANDLERS; i++) {
        if (logHandlers[i] == NULL) {
            logHandlers[i] = this;
            return;
        }
    }

    mikoAssert(false && "too many log handlers");
}

void LogHandler::remove()
{
    for (int i = 0; i < MAX_LOG_HANDLERS; i++) {
        if (logHandlers[i] == this) {
            logHandlers[i] = NULL;
        }
    }
}

static void logWrite(LogLevel level, const char *msg, uint32_t length)
{
    mikoAssert(SDL_LockMutex(logLock) == 0);

    for (int i = 0; i < MAX_LOG_HANDLERS; i++) {
        if (logHandlers[i]) {
            logHandlers[i]->write(level, msg, length);
        }
    }

    mikoAssert(SDL_UnlockMutex(logLock) == 0);
}

void _logWrite(LogLevel level, const char *msg)
{
    logWrite(level, msg, strlen(msg));
}

void _logWriteFmt(LogLevel level, const char *fmt, ...)
{
    va_list ap; 
    va_start(ap, fmt);
    _logWriteVarargs(level, ap, fmt);
    va_end(ap);
}

void _logWriteVarargs(LogLevel level, va_list ap, const char *fmt)
{
    char buf[MAX_LOG_FORMAT_LENGTH];
    int n;

    n = vsnprintf(buf, sizeof(buf), fmt, ap);
    if (n < 0) {
        _logWrite(LOG_ERROR, "invalid format string for log");
    }
    else if (n < (int)sizeof(buf)) {
        _logWrite(level, buf);
    }
    else {
        _logWrite(level, buf);
        _logWrite(LOG_ERROR, "too long formatted log string (max 4096)");
    }
}

const char * logLevelStr(LogLevel level)
{
    switch (level) {
    case LOG_DEBUG: return "debug";
    case LOG_INFO: return "info";
    case LOG_WARNING: return "warning";
    case LOG_ERROR: return "error";
    case LOG_FATAL: return "fatal";
    default:
        mikoUnreachable();
        return "error";
    }
}
    
} /* namespace miko */
