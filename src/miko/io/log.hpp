#ifndef MIKO_IO_LOG_HPP
#define MIKO_IO_LOG_HPP

#include "miko/miko.hpp"

#include <stdarg.h>

#define LOG_DEBUG(msg) _logWrite(LOG_DEBUG, msg);
#define LOG_INFO(msg) _logWrite(LOG_INFO, msg);
#define LOG_WARN(msg) _logWrite(LOG_WARNING, msg);
#define LOG_ERROR(msg) _logWrite(LOG_ERROR, msg);
#define LOG_FATAL(msg) _logWrite(LOG_FATAL, msg);

#define LOG_DEBUGF(fmt, ...) _logWriteFmt(LOG_DEBUG, fmt, ##__VA_ARGS__)
#define LOG_INFOF(fmt, ...) _logWriteFmt(LOG_INFO, fmt, ##__VA_ARGS__)
#define LOG_WARNF(fmt, ...) _logWriteFmt(LOG_WARNING, fmt, ##__VA_ARGS__)
#define LOG_ERRORF(fmt, ...) _logWriteFmt(LOG_ERROR, fmt, ##__VA_ARGS__)
#define LOG_FATALF(fmt, ...) _logWriteFmt(LOG_FATAL, fmt, ##__VA_ARGS__)

#define LOG_DEBUGV(ap, fmt) _logWriteVarargs(LOG_DEBUG, ap, fmt)
#define LOG_INFOV(ap, fmt) _logWriteVarargs(LOG_INFO, ap, fmt)
#define LOG_WARNV(ap, fmt) _logWriteVarargs(LOG_WARN, ap, fmt)
#define LOG_ERRORV(ap, fmt) _logWriteVarargs(LOG_ERROR, ap, fmt)
#define LOG_FATALV(ap, fmt) _logWriteVarargs(LOG_FATAL, ap, fmt)

enum LogLevel
{
    LOG_DEBUG,
    LOG_INFO,
    LOG_WARNING,
    LOG_ERROR,
    LOG_FATAL
};

namespace miko {

const char * logLevelStr(LogLevel level);

void setMinLogLevel(LogLevel level);

class LogHandler
{
public:
    virtual ~LogHandler();

    void listen();
    void remove();

    virtual void write(LogLevel level, const char *msg, uint32_t length) = 0;

private:
};

void _logWrite(LogLevel level, const char *msg);

void _logWriteFmt(LogLevel level, const char *fmt, ...) MIKO_PRINTF_FORMAT(2, 3);
void _logWriteVarargs(LogLevel level, va_list ap, const char *fmt);
    
} /* namespace miko */

#endif /* MIKO_IO_LOG_HPP */

