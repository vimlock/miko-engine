#include "miko/io/file_logger.hpp"
#include <time.h>

namespace miko {

#define COL_NONE    "\x1B[0m"
#define COL_RED     "\x1B[31m"
#define COL_GREEN   "\x1B[32m"
#define COL_YELLOW  "\x1B[33m"
#define COL_BLUE    "\x1B[34m"
#define COL_MAGENTA "\x1B[35m"
#define COL_CYAN    "\x1B[36m"
#define COL_WHITE   "\x1B[37m"

static const char * logLevelColorCode(LogLevel level)
{
    switch (level) {
    case LOG_DEBUG: return COL_CYAN;
    case LOG_INFO:  return COL_GREEN;
    case LOG_WARNING: return COL_BLUE;
    case LOG_ERROR: return COL_YELLOW;
    case LOG_FATAL: return COL_RED;
    default: return COL_NONE;
    }
}

FileLogger::FileLogger(FILE *stream, bool closeOnExit, bool useColor)
{
    this->stream = stream;
    this->closeOnExit = closeOnExit;
    this->useColor = useColor;
}

FileLogger::~FileLogger()
{
    if (closeOnExit && stream) {
        fclose(stream);
    }
}

void FileLogger::write(LogLevel level, const char *msg, uint32_t length)
{
    const char *ccode = NULL;
    if (useColor) {
        ccode = logLevelColorCode(level);
    }

    time_t timer;
    char timebuf[32];
    struct tm *tm_info;

    time(&timer);
    tm_info = localtime(&timer);
    strftime(timebuf, 32, "%H:%M:%S", tm_info);
    fputs(timebuf, stream);

    fputc(' ', stream);
    fputc('[', stream);

    /* set output color */
    if (ccode) {
        fputs(ccode, stream);
    }

    fputs(logLevelStr(level), stream);

    /* reset the output color */
    if (ccode) {
        fputs(COL_NONE, stream);
    }
    fputc(']', stream);
    fputc(' ', stream);

    fputs(msg, stream);
    fputc('\n', stream);
}
    
} /* namespace miko */
