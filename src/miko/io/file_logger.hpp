#ifndef MIKO_IO_FILE_LOGGER_HPP
#define MIKO_IO_FILE_LOGGER_HPP

#include "miko/io/log.hpp"

#include <stdio.h>

namespace miko {

class FileLogger : public LogHandler
{
public:
    FileLogger(FILE *stream, bool closeOnExit, bool useColor);

    virtual ~FileLogger();

    virtual void write(LogLevel level, const char *msg, uint32_t length);

private:
    FILE *stream;
    bool closeOnExit;
    bool useColor;

};
    
} /* namespace miko */

#endif /* MIKO_IO_FILE_LOGGER_HPP */

