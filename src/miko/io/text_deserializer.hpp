#ifndef MIKO_IO_TEXT_DESERIALIZER_HPP
#define MIKO_IO_TEXT_DESERIALIZER_HPP

#include "miko/io/serialize.hpp"
#include "miko/io/lexer.hpp"
#include "miko/util/assert.hpp"

#include <errno.h>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#define MIKO_LEXER_MAX_TOKEN_SIZE 1024

namespace miko {

class TextDeserializer : public IDeserializer
{
public:
    TextDeserializer(const char *filename, SDL_RWops *rwops);
    virtual ~TextDeserializer();

    void error(const char *fmt, ...);

    template <typename T>
    inline bool read(T &value);

    bool readEnum(const EnumInfo *infos, int& value);

    void advance();

    bool accept(Token::Type);
    bool expect(Token::Type);
    bool skip(Token::Type);

    bool acceptIdentifier(const char *name);
    bool expectIdentifier(const char *name);
    bool skipIdentifier(const char *name);

    Token & nextToken()
    {
        return next;
    }

private:
    const char *filename;
    char buf[MIKO_LEXER_MAX_TOKEN_SIZE];
    Lexer lexer;
    Token next;
};

template<>
inline bool TextDeserializer::read<bool>(bool &value)
{
    if (acceptIdentifier("true")) {
        advance();
        value = true;
        return true;
    }
    else if (acceptIdentifier("false")) {
        advance();
        value = false;
        return true;
    }
    else {
        error("expecting boolean, got %s", Token::typeStr(next.type));
        return false;
    }
}

template<>
inline bool TextDeserializer::read<int64_t>(int64_t &value)
{
    long long int val;
    char *tmp;

    if (!expect(Token::INT)) {
        return false;
    }

    errno = 0;

    val = strtoll(next.str, &tmp, 10);
    if (errno == ERANGE) {
        error("Too large integer");
        return false;
    }

    mikoAssert(tmp != next.str || *tmp == '\0');
    value = val;

    advance();

    return true;
}

template<>
inline bool TextDeserializer::read<int32_t>(int32_t &value)
{
    long val;
    char *tmp;

    if (!expect(Token::INT)) {
        return false;
    }

    errno = 0;

    val = strtol(next.str, &tmp, 10);
    if (errno == ERANGE) {
        error("Too large integer");
        return false;
    }

    mikoAssert(tmp != next.str || *tmp == '\0');
    value = val;

    advance();
    return true;
}

template<>
inline bool TextDeserializer::read<int16_t>(int16_t &value)
{
    int32_t tmp;
    if (!read(tmp)) {
        return false;
    }

    if (tmp < INT16_MIN || tmp > INT16_MAX) {
        error("Too large integer");
        return false;
    }

    value = tmp;
    return true;
}

template<>
inline bool TextDeserializer::read<int8_t>(int8_t &value)
{
    int32_t tmp;
    if (!read(tmp)) {
        return false;
    }

    if (tmp < INT8_MIN || tmp > INT8_MAX) {
        error("integer out of range");
        return false;
    }

    value = tmp;
    return true;
}

template<>
inline bool TextDeserializer::read<uint32_t>(uint32_t &value)
{
    int64_t tmp;
    if (!read(tmp)) {
        return false;
    }

    if (tmp < 0 || tmp > UINT32_MAX) {
        error("integer out of range");
        return false;
    }

    value = tmp;
    return true;
}

template<>
inline bool TextDeserializer::read<uint16_t>(uint16_t &value)
{
    int32_t tmp;
    if (!read(tmp)) {
        return false;
    }

    if (tmp < 0 || tmp > UINT16_MAX) {
        error("integer out of range");
        return false;
    }

    value = tmp;
    return true;
}

template<>
inline bool TextDeserializer::read<uint8_t>(uint8_t &value)
{
    int32_t tmp;
    if (!read(tmp)) {
        return false;
    }

    if (tmp < 0 || tmp > UINT8_MAX) {
        error("integer out of range");
        return false;
    }

    value = tmp;
    return true;
}

template<>
inline bool TextDeserializer::read<float>(float &value)
{
    if (accept(Token::INT)) {
        int32_t v;
        if (!read(v)) {
            return false;
        }
        value = v;
    }
    else  {
        if (!expect(Token::FLOAT)) {
            return false;
        }

        char *tmp;
        float val;

        errno = 0;
        val = strtof(next.str, &tmp);
        if (errno == ERANGE) {
            error("Float out of range");
            return false;
        }

        mikoAssert(tmp != next.str || *tmp == '\0');
        advance();

        value = val;
    }

    return true;
}

template<>
inline bool TextDeserializer::read<std::string>(std::string &value)
{
    if (!expect(Token::STRING)) {
        return false;
    }

    value = std::string(next.str, next.length);
    advance();

    return true;
}

template<>
inline bool TextDeserializer::read<glm::vec2>(glm::vec2 &value)
{
    float tmp[2];

    if (!skip(Token::LPAREN)) {
        return false;
    }

    for (int i = 0; i < 2; i++) {
        if (!read(tmp[i])) {
            return false;
        }
    }

    if (!skip(Token::RPAREN)) {
        return false;
    }

    value = glm::vec2(tmp[0], tmp[1]);

    return true;
}

template<>
inline bool TextDeserializer::read<glm::vec3>(glm::vec3 &value)
{
    float tmp[3];

    if (!skip(Token::LPAREN)) {
        return false;
    }

    for (int i = 0; i < 3; i++) {
        if (!read(tmp[i])) {
            return false;
        }
    }

    if (!skip(Token::RPAREN)) {
        return false;
    }

    value = glm::vec3(tmp[0], tmp[1], tmp[2]);

    return true;
}

template<>
inline bool TextDeserializer::read<glm::vec4>(glm::vec4 &value)
{
    float tmp[4];

    if (!skip(Token::LPAREN)) {
        return false;
    }

    for (int i = 0; i < 4; i++) {
        if (!read(tmp[i])) {
            return false;
        }
    }

    if (!skip(Token::RPAREN)) {
        return false;
    }

    value = glm::vec4(tmp[0], tmp[1], tmp[2], tmp[3]);

    return true;
}

template<>
inline bool TextDeserializer::read<glm::quat>(glm::quat &value)
{
    float tmp[4];

    if (!skip(Token::LPAREN)) {
        return false;
    }

    for (int i = 0; i < 4; i++) {
        if (!read(tmp[i])) {
            return false;
        }
    }

    if (!skip(Token::RPAREN)) {
        return false;
    }

    value = glm::quat(tmp[0], tmp[1], tmp[2], tmp[3]);

    return true;
}

template<>
inline bool TextDeserializer::read(glm::mat3 &value)
{
    glm::mat3 tmp;
    if (!skip(Token::LPAREN)) {
        return false;
    }

    for (unsigned int i = 0; i < 3; i++) {
        if (!read(tmp[i])) {
            return false;
        }
    }

    if (!skip(Token::RPAREN)) {
        return false;
    }

    value = tmp;
    return true;
}

template<>
inline bool TextDeserializer::read(glm::mat4 &value)
{
    glm::mat4 tmp;
    if (!skip(Token::LPAREN)) {
        return false;
    }

    for (unsigned int i = 0; i < 4; i++) {
        if (!read(tmp[i])) {
            return false;
        }
    }

    if (!skip(Token::RPAREN)) {
        return false;
    }

    value = tmp;
    return true;
}
    
} /* namespace miko */

#endif /* MIKO_IO_TEXT_DESERIALIZER_HPP */

