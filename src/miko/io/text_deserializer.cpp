#include "miko/io/text_deserializer.hpp"
#include "miko/io/log.hpp"

namespace miko {

TextDeserializer::TextDeserializer(const char *_filename, SDL_RWops *rwops):
        filename(_filename),
        lexer(rwops, buf, sizeof(buf))
{
    advance();
}

TextDeserializer::~TextDeserializer()
{
    // empty
}

void TextDeserializer::error(const char *fmt, ...)
{
    LOG_ERRORF("%s:%d:%d:", filename, next.line, next.column);

    va_list ap;
    va_start(ap, fmt);

    LOG_ERRORV(ap, fmt);

    va_end(ap);
}

void TextDeserializer::advance()
{
    lexer.advance(&next);
}

bool TextDeserializer::readEnum(const EnumInfo *infos, int& value)
{
    const EnumInfo *iter;

    if (!expect(Token::IDENTIFIER)) {
        return false;
    }

    iter = infos;
    while (iter->name) {
        if (acceptIdentifier(iter->name)) {
            value = iter->value;
            advance();
            return true;
        }

        iter++;
    }

    iter = infos;
    error("expecting identifier %s", iter->name);
    iter++;
    while (iter->name) {
        LOG_ERRORF("or %s", iter->name);
        iter++;
    }

    return false;
}

bool TextDeserializer::accept(Token::Type type)
{
    return next.type == type;
}

bool TextDeserializer::expect(Token::Type type)
{
    if (!accept(type)) {
        error("expecting %s, got %s", 
            Token::typeStr(type),
            Token::typeStr(next.type)
        );

        return false;
    }

    return true;
}

bool TextDeserializer::skip(Token::Type type)
{
    if (!expect(type)) {
        return false;
    }

    advance();
    return true;
}

bool TextDeserializer::acceptIdentifier(const char *name)
{
    if (!accept(Token::IDENTIFIER)) {
        return false;
    }

    return strcmp(name, next.str) == 0;
}

bool TextDeserializer::expectIdentifier(const char *name)
{
    if (!acceptIdentifier(name)) {
        error("expecting identifier %s, got %s", name,
            Token::typeStr(next.type)
        );
        return false;
    }

    return true;
}

bool TextDeserializer::skipIdentifier(const char *name)
{
    if (!expectIdentifier(name)) {
        return false;
    }

    advance();
    return true;
}
    
} /* namespace miko */
