#ifndef MIKO_DEVICE_HPP
#define MIKO_DEVICE_HPP

#include <glm/glm.hpp>
#include <SDL2/SDL.h>

#include "miko/device/window.hpp"
#include "miko/container/vector.hpp"

namespace miko {

namespace DeviceBufferMode
{
    enum Enum
    {
        SINGLE_BUFFERING,
        DOUBLE_BUFFERING
    };
}

struct DeviceOptions
{
    DeviceOptions();

    int32_t multisamplingLevel;
    int32_t depthSize;

    DeviceBufferMode::Enum bufferMode;
};

class IDevice
{
public:
    virtual ~IDevice()
    {
    }

    virtual void update() = 0;
    virtual bool pollEvent(SDL_Event *ev) = 0;

    virtual IWindow * createWindow(
        int32_t width,
        int32_t height,
        bool resizeable,
        WindowDisplayMode::Enum mode
    ) = 0;
};
    
} /* namespace miko */

#endif /* MIKO_DEVICE_HPP */

