#include "miko/device/sdl_window.hpp"
#include "miko/io/log.hpp"
#include "miko/memory/memory.hpp"
#include "miko/graphics/gl/graphics.hpp"
#include "miko/util/assert.hpp"

namespace miko {

SdlWindow::SdlWindow(
        SdlDevice *device,
        SDL_Window *win,
        SDL_GLContext glcontext,
        WindowDisplayMode::Enum mode)
{
    mikoAssert(device != NULL);
    mikoAssert(win != NULL);
    mikoAssert(glcontext != NULL);

    this->device = device;
    this->window = win;
    this->glContext = glcontext;
    this->displayMode = mode;

    this->renderer = NULL;

    int w, h;
    SDL_GetWindowSize(this->window, &w, &h);

    width = w;
    height = h;

    resizable = false;
    mode = WindowDisplayMode::WINDOWED;
}

static SDL_Window * createWindow(int32_t width, int32_t height, bool resizeable, WindowDisplayMode::Enum mode)
{
    SDL_Window *win = NULL;

    int flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;
    if (mode == WindowDisplayMode::FULLSCREEN) {
        flags |= SDL_WINDOW_FULLSCREEN;
    }

    if (resizeable) {
        flags |= SDL_WINDOW_RESIZABLE;
    }

    win = SDL_CreateWindow("MikoEngine",
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        width, height, flags);

    if (!win) {
        LOG_ERRORF("Failed to create window: %s", SDL_GetError());
        return NULL;
    }

    return win;
}

static SDL_GLContext createContext(SDL_Window *win)
{
    SDL_GLContext glcontext;
    GLenum err;

    glcontext = SDL_GL_CreateContext(win);
    if (!glcontext) {
        LOG_ERRORF("Failed to create OpenGL context: %s", SDL_GetError());
        return NULL;
    }

    CHECK_GL_ERROR("SDL_GL_CreateContext()");

    glewExperimental = 1;
    err = glewInit();
    if (err != GLEW_OK) {
        LOG_ERRORF("Failed to initialize GLEW: %s", glewGetErrorString(err));
        goto fail;
    }

    /* GLEW emits GL_INVALID_ENUM on some versions for some reasons but we can safely ignore it */
    glGetError();

    return glcontext;

    fail:
    if (glcontext) {
        SDL_GL_DeleteContext(glcontext);
    }

    return NULL;
}

static bool setupVsync(SDL_Window *window, WindowSyncMode::Enum mode)
{
    int syncmode;
    switch (mode) {
    case WindowSyncMode::VSYNC:
        syncmode = 1;
        break;
    case WindowSyncMode::VSYNC_LATE:
        syncmode = -1;
        break;
    case WindowSyncMode::NOVSYNC:
        syncmode = 0;
        break;
    default:
        syncmode = 0;
        mikoUnreachable();
    }

    do {
        int err = SDL_GL_SetSwapInterval(syncmode);
        if (!err) {
            break;
        }

        LOG_ERRORF("Failed to set VSync mode: %s", SDL_GetError());
        if (syncmode == 1) {
            return false;
        }
        else {
            LOG_ERROR("Retrying with double buffering");
            syncmode = 1;
        }
    } while (true);

    return true;
}

SdlWindow * SdlWindow::create(
        Allocator &allocator,
        SdlDevice *device,
        int32_t width,
        int32_t height,
        bool resizeable,
        WindowDisplayMode::Enum mode)
{
    SDL_Window *win = NULL;
    GlRenderer *renderer = NULL;
    SDL_GLContext context = NULL;
    SdlWindow *window = NULL;

    win = createWindow(width, height, resizeable, mode);
    if (!win) {
        goto fail;
    }

    context = createContext(win);
    if (!context) {
        goto fail;
    }

    if (!setupVsync(win, WindowSyncMode::VSYNC_LATE)) {
        goto fail;
    }

    window = MIKO_NEW(allocator, SdlWindow) (device, win, context, mode);
    if (!window) {
        goto fail;
    }

    renderer = GlRenderer::create(allocator, window);
    if (!renderer) {
        MIKO_DELETE(allocator, window);
        return NULL;
    }

    window->renderer = renderer;

    return window;
    
    fail:

    if (renderer) {
        MIKO_DELETE(allocator, renderer);
    }

    if (window) {
        MIKO_DELETE(allocator, window);
    }

    if (context) {
        SDL_GL_DeleteContext(context);
    }

    if (win) {
        SDL_DestroyWindow(win);
    }

    return NULL;
}

SdlWindow::~SdlWindow()
{
    MIKO_DELETE(defaultAllocator, renderer);
    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(window);
}

void SdlWindow::minimize()
{
}

void SdlWindow::maximize()
{
    //SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    SDL_MaximizeWindow(window);

    int w, h;
    SDL_GetWindowSize(this->window, &w, &h);

    width = w;
    height = h;
}

void SdlWindow::restore()
{
}

void SdlWindow::update()
{
    SDL_GL_SwapWindow(window);
}

glm::ivec2 SdlWindow::getDimensions() const
{
    int w, h;
    SDL_GetWindowSize(this->window, &w, &h);
    return glm::ivec2(w, h);
}

void SdlWindow::setDimensions(const glm::ivec2 &dimensions)
{
    SDL_SetWindowSize(window, dimensions.x, dimensions.y);
}

GlRenderer * SdlWindow::getRenderer()
{
    return renderer;
}
    
} /* namespace miko */
