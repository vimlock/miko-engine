#ifndef MIKO_SDL_DEVICE_HPP
#define MIKO_SDL_DEVICE_HPP

#include "miko/device/device.hpp"
#include "miko/device/sdl_window.hpp"

namespace miko {
    
class SdlDevice : public IDevice
{
public:
    SdlDevice();
    virtual ~SdlDevice();

    bool init(const DeviceOptions &opts);

    virtual void update();
    virtual bool pollEvent(SDL_Event *ev);
    virtual SdlWindow * createWindow(int32_t width, int32_t height, bool resizeable,
        WindowDisplayMode::Enum mode);

private:
    DeviceBufferMode::Enum bufferMode;

    Vector<SdlWindow *> windows;
};

} /* namespace miko */

#endif /* MIKO_SDL_DEVICE_HPP */

