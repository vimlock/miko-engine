#ifndef MIKO_SDL_WINDOW_HPP
#define MIKO_SDL_WINDOW_HPP

#include "miko/device/window.hpp"
#include "miko/graphics/gl/renderer.hpp"
#include "miko/memory/memory.hpp"

#include <SDL2/SDL.h>

namespace miko {

class SdlDevice;


class SdlWindow : public IWindow
{
public:
    static SdlWindow * create(
        Allocator &allocator,
        SdlDevice *device,
        int32_t width,
        int32_t height,
        bool resizeable,
        WindowDisplayMode::Enum mode
    );
    
    virtual ~SdlWindow();

    virtual void minimize();
    virtual void maximize();
    virtual void restore();


    virtual void update();

    virtual glm::ivec2 getDimensions() const;

    virtual void setDimensions(const glm::ivec2 &dimensions);

    virtual GlRenderer * getRenderer();

    /// Returns the underlying SDL_Window that this window manages
    SDL_Window * getSdlWindow()
    {
        return window;
    }

private:
    SdlWindow(
        SdlDevice *device,
        SDL_Window *win,
        SDL_GLContext glcontext,
        WindowDisplayMode::Enum mode
    );

    SdlDevice *device;
    SDL_Window *window;
    SDL_GLContext glContext;

    GlRenderer *renderer;

    int32_t width;
    int32_t height;

    bool resizable;

    WindowDisplayMode::Enum displayMode;
    WindowSyncMode::Enum vsyncMode;
};
    
} /* namespace miko */

#endif /* MIKO_SDL_WINDOW_HPP */

