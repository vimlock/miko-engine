#include "miko/device/sdl_device.hpp"
#include "miko/graphics/gl/graphics.hpp"
#include "miko/io/log.hpp"
#include "miko/memory/memory.hpp"
#include "miko/util/assert.hpp"

namespace miko {

SdlDevice::SdlDevice():
    windows(defaultAllocator)
{
    bufferMode = DeviceBufferMode::DOUBLE_BUFFERING;
}

SdlDevice::~SdlDevice()
{
    for (unsigned i = 0; i < windows.size(); i++) {
        MIKO_DELETE(defaultAllocator, windows[i]);
    }

    SDL_Quit();
}

static bool sdlGlAttribute(const char *name, SDL_GLattr attr, int value)
{
    int err;

    err = SDL_GL_SetAttribute(attr, value);
    if (err < 0) {
        LOG_ERRORF("Failed to set %s: %s", name, SDL_GetError());
        return false;
    }

    return true;
}

static bool initGl(const DeviceOptions &opts)
{
    #define SETATTR(attr, value)\
        if (!sdlGlAttribute(#attr, attr, value)) { return false; }

    SETATTR(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SETATTR(SDL_GL_CONTEXT_MINOR_VERSION, 3);

    SETATTR(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SETATTR(SDL_GL_MULTISAMPLESAMPLES, 4);

    SETATTR(SDL_GL_DEPTH_SIZE, opts.depthSize);

    SETATTR(SDL_GL_DOUBLEBUFFER,
        opts.bufferMode == DeviceBufferMode::DOUBLE_BUFFERING ? 1 : 0);

    SETATTR(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
    SETATTR(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    return true;
}

bool SdlDevice::init(const DeviceOptions &opts)
{
    int initFlags = SDL_INIT_VIDEO |
                    SDL_INIT_AUDIO |
                    SDL_INIT_TIMER |
                    SDL_INIT_EVENTS;

    if (SDL_Init(initFlags) < 0) {
        LOG_ERRORF("Failed to initialize SDL: %s", SDL_GetError());
        return false;
    }

    if (!initGl(opts)) {
        SDL_Quit();
        return false;
    }

    bufferMode = opts.bufferMode;

    return true;
}

void SdlDevice::update()
{
}

bool SdlDevice::pollEvent(SDL_Event *ev)
{
    return SDL_PollEvent(ev);
}

SdlWindow * SdlDevice::createWindow(int32_t width, int32_t height, bool resizable, WindowDisplayMode::Enum mode)
{
    Allocator &allocator = defaultAllocator;

    SdlWindow *win = SdlWindow::create(allocator, this, width, height, resizable, mode);
    if (!win) {
        return NULL;
    }

    if (!windows.append(win)) {
        MIKO_DELETE(allocator, win);
        return NULL;
    }

    return win;
}

} /* namespace miko */
