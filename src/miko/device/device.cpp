#include "miko/device/device.hpp"

namespace miko {

DeviceOptions::DeviceOptions()
{
    multisamplingLevel = 1;
    depthSize = 24;
    bufferMode = DeviceBufferMode::DOUBLE_BUFFERING;
}

} /* namespace miko */
