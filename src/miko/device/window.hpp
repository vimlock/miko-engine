#ifndef MIKO_WINDOW_HPP
#define MIKO_WINDOW_HPP

#include <glm/glm.hpp>

namespace miko {

class IRenderer;

namespace WindowDisplayMode
{
    enum Enum {
        WINDOWED,
        WINDOWED_FULLSCREEN,
        FULLSCREEN
    };
}

namespace WindowSyncMode
{
    enum Enum
    {
        NOVSYNC,
        VSYNC,
        VSYNC_LATE
    };
} 

class IWindow
{
public:
    virtual ~IWindow()
    {
    }

    virtual void minimize() = 0;
    virtual void maximize() = 0;
    virtual void restore() = 0;


    virtual void update() = 0;

    virtual glm::ivec2 getDimensions() const = 0;

    int32_t getWidth() const
    {
        return getDimensions().x;
    }

    int32_t getHeigth() const
    {
        return getDimensions().y;
    }

    virtual void setDimensions(const glm::ivec2 &dimensions) = 0;

    virtual IRenderer * getRenderer() = 0;
};
    
} /* namespace miko */

#endif /* MIKO_WINDOW_HPP */

