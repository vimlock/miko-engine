#include "miko/resource/resource.hpp"
#include "miko/resource/resource_manager.hpp"

#include "miko/util/assert.hpp"

namespace miko {

const char * toStr(ResourceType e)
{
    switch (e) {
    case RESOURCE_TEXTURE:          return "texture";
    case RESOURCE_MATERIAL:         return "material";
    case RESOURCE_SHADER_PROGRAM:   return "shader program";
    case RESOURCE_SHADER_SOURCE:    return "shader source";
    case RESOURCE_MESH:             return "mesh";
    case RESOURCE_MESH_ANIMATION:   return "mesh animation";
    case RESOURCE_SPRITE_ANIMATION: return "sprite animation";
    }

    mikoUnreachable();
    return NULL;
}

Resource::Resource():
        path(),
        resourceManager(NULL),
        loadState(RESOURCE_ASYNC_QUEUED)
{
}

bool Resource::loadAsyncPart(SDL_RWops *rwops)
{
    mikoAssert(loadState == RESOURCE_ASYNC_LOADING);
    return true;
}

bool Resource::loadSyncPart(SDL_RWops *rwops)
{
    mikoAssert(loadState == RESOURCE_SYNC_LOADING);
    return true;
}

void Resource::waitLoaded()
{
    resourceManager->waitLoaded(this);
}

} /* namespace miko */
