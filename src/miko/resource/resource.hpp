#ifndef MIKO_RESOURCE_HPP
#define MIKO_RESOURCE_HPP

#include "miko/core/object.hpp"

#include <SDL2/SDL_rwops.h>

#include <string>

namespace miko {

class ResourceManager;

#define MIKO_RESOURCE(type)                               \
    public:                                               \
    virtual ResourceType getResourceType() const { \
        return getResourceTypeStatic();                   \
    }                                                     \
    static ResourceType getResourceTypeStatic() {         \
        return type;                                      \
    }

enum ResourceType
{
    RESOURCE_TEXTURE,
    RESOURCE_MATERIAL,
    RESOURCE_MESH,
    RESOURCE_MESH_ANIMATION,
    RESOURCE_SPRITE_ANIMATION,
    RESOURCE_SHADER_PROGRAM,
    RESOURCE_SHADER_SOURCE,
};

enum {
    RESOURCE_TYPE_COUNT = RESOURCE_SHADER_SOURCE + 1
};

enum ResourceLoadState
{
    RESOURCE_ASYNC_QUEUED,
    RESOURCE_ASYNC_LOADING,

    RESOURCE_SYNC_QUEUED,
    RESOURCE_SYNC_LOADING,

    RESOURCE_LOADED,
    RESOURCE_FAILED
};

class Resource : public Object
{
friend class ResourceManager;

public:
    Resource();

    virtual bool loadAsyncPart(SDL_RWops *rwops);
    virtual bool loadSyncPart(SDL_RWops *rwops);

    virtual ResourceType getResourceType() const = 0;

    /// Blocks until the resource's state becomes RESOURCE_LOADED or RESOURCE_FAILED
    void waitLoaded();

    bool isLoaded() const
    {
        return loadState == RESOURCE_LOADED;
    }

    const std::string & getName() const
    {
        return path;
    }

    ResourceLoadState getLoadState() const
    {
        return loadState;
    }

    ResourceManager * getResourceManager()
    {
        return resourceManager;
    }

private:
    void setLoadState(ResourceLoadState _state)
    {
        loadState = _state;
    }

    void setPath(const std::string &_path)
    {
        path = _path;
    }

    void setResourceManager(ResourceManager *_resourceManager)
    {
        resourceManager = _resourceManager;
    }

    std::string path;
    ResourceManager *resourceManager;
    ResourceLoadState loadState;
};
    
const char * toStr(ResourceType t);

} /* namespace miko */

#endif /* MIKO_RESOURCE_HPP */

