#ifndef MIKO_RESOURCE_LOADER_HPP
#define MIKO_RESOURCE_LOADER_HPP

#include "miko/resource/resource.hpp"
#include <SDL2/SDL_rwops.h>

namespace miko {

class Resource;
class WorkQueue;
class GlTexture;
class GlShaderSource;

class IResourceFactory;

class IResourceLoader
{
public:
    virtual ~IResourceLoader()
    {
        // empty
    }

    virtual void preload(SDL_RWops *rwops, Resource *res, bool background) = 0;
    virtual Resource * load(SDL_RWops *rwops, Resource *res, bool background) = 0;
};

class IResourceFactory
{
public:
    virtual ~IResourceFactory()
    {
        // empty
    }

    virtual Resource * create(Allocator &allocator) = 0;
};

class TextureResourceFactory : public IResourceFactory
{
public:
    virtual Resource * create(Allocator &allocator);

};


} /* namespace miko */

#endif /* MIKO_RESOURCE_LOADER_HPP */

