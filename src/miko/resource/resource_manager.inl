#ifndef MIKO_RESOURCE_MANAGER_INL
#define MIKO_RESOURCE_MANAGER_INL

#include "miko/resource/resource_manager.hpp"

namespace miko {

template <typename T>
void ResourceManager::preload(const std::string &name)
{
    getDirect(T::getResourceTypeStatic(), name);
}
    
template <typename T>
T * ResourceManager::get(const std::string &name)
{
    return getHandle<T>(name);
}

template <typename T>
ResourceHandle<T> ResourceManager::getHandle(const std::string &name)
{
    ResourceHandle<T> handle = getHandleNonBlocking<T>(name);
    handle.waitLoaded();
    return handle;
}

template <typename T>
ResourceHandle<T> ResourceManager::getHandleNonBlocking(const std::string &name)
{
    Resource *res = getDirect(T::getResourceTypeStatic(), name);
    ResourceHandle<T> handle(static_cast<T*>(res), name);

    return handle;
}

} /* namespace miko */

#endif /* MIKO_RESOURCE_MANAGER_INL */

