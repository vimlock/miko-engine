#ifndef MIKO_RESOURCE_HANDLE_INL
#define MIKO_RESOURCE_HANDLE_INL

#include "miko/resource/resource_handle.hpp"
#include "miko/util/assert.hpp"

namespace  miko {

template <typename T>
ResourceHandle<T>::ResourceHandle():
        ptr(),
        name(),
        ready(true)
{
    // empty
}

template <typename T>
ResourceHandle<T>::ResourceHandle(T *_ptr, const std::string &_name):
        ptr(_ptr),
        name(_name),
        ready(false)
{
    // empty
}

template <typename T>
const std::string& ResourceHandle<T>::getName() const
{
    return name;
}

template <typename T>
void ResourceHandle<T>::waitLoaded()
{
    // Already called this function?
    if (ready) {
        return;
    }

    // No resource to wait for?
    if (!ptr) {
        ready = true;
        return;
    }

    ptr->waitLoaded();

    // If the loading failed, free the reference to the resource, as it is not usable.
    if (ptr->getLoadState() != RESOURCE_LOADED) {
        ptr = NULL;
    }

    ready = true;
}

template <typename T>
bool ResourceHandle<T>::isReady() const
{
    return ready;
}

template <typename T>
ResourceHandle<T>::operator T * ()
{
    mikoAssert(ready);
    return ptr;
}

template <typename T>
Ref<T>& ResourceHandle<T>::operator -> ()
{
    mikoAssert(ready);
    return ptr;
}

} /* namespace  miko */

#endif /* MIKO_RESOURCE_HANDLE_INL */

