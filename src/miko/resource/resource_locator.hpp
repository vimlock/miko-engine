#ifndef MIKO_RESOURCE_LOCATOR_HPP
#define MIKO_RESOURCE_LOCATOR_HPP

#include <SDL2/SDL_rwops.h>
#include <string>

#include "miko/container/vector.hpp"

namespace miko {

class IResourceLocator;

struct ResourceEntry
{
    enum Type
    {
        RESOURCE,
        DIRECTORY
    };

    /// Basename of the entry
    std::string name;

    /// Path of the entry
    std::string path;

    /// Type of the entry.
    Type type;

    /// Size of the entry (in bytes). If the type == DIRECTORY, size will be 0
    uint32_t size;

    /// Locator which was used to find this
    IResourceLocator *locator;
};

typedef Vector<ResourceEntry> ResourceEntryList;

class IResourceLocator
{
public:
    /// Destructor
    virtual ~IResourceLocator()
    {
        // empty
    }

    /// Returns a new RWops stream to the given path.
    /// If the path doesn't exist, NULL is returned.
    virtual SDL_RWops * find(const std::string &path) = 0;

    /// Populate the list with ResourceInfos.
    /// @remarks Returns false if the given path is not a directory or does not exist.
    ///          The result list passed on to this function can already contain
    ///          resource entries. The implementor of this function must make sure
    ///          that no duplicates are added to the list.
    virtual bool getDirContents(const std::string &path, ResourceEntryList *result) = 0;

    /// @todo Garbage function, remove.
    virtual void dumpInfo() const = 0;

    /// Return a string which describes what kind of locator this is
    /// e.g. "archive", "directory"
    virtual const char * getType() const = 0;

    /// Returns a string which describes where this locator looks for resources.
    /// e.g. a path to a directory, path to a file, url, etc.
    virtual const char * getPath() const = 0;
};
    
} /* namespace miko */

#endif /* MIKO_RESOURCE_LOCATOR_HPP */

