#include "miko/resource/resource_manager.hpp"
#include "miko/core/work_queue.hpp"
#include "miko/io/log.hpp"

namespace miko {


ResourceManager::ResourceManager(Allocator &_allocator, WorkQueue *_workQueue):
    locators(_allocator),
    cached(_allocator),
    loading(_allocator),
    workQueue(_workQueue),
    allocator(&_allocator)

{
    // lets assume that our creator is the owner
    ownerThread = SDL_ThreadID();

    for (int i = 0; i < RESOURCE_TYPE_COUNT; i++) {
        factories[i] = NULL;
    }
}

ResourceManager::~ResourceManager()
{
}

void ResourceManager::update()
{
    HashMap<std::string, Ref<Resource> >::Iterator i;

    // This function should only be callable from main thread
    mikoAssert(SDL_ThreadID() == ownerThread);

    mutex.lock();

    // Do synchronous loading parts for background loaded resources
    i = loading.begin();
    while (i != loading.end()) {
        if (i->val->getLoadState() != RESOURCE_SYNC_QUEUED) {
            ++i;
            continue;
        }

        Ref<Resource> res = i->val;
        ResourceType type = res->getResourceType();
        const std::string &name = res->getName();

        if (!loadSyncPart(res)) {
            LOG_ERRORF("failed to load %s %s", toStr(type), name.c_str());
        }

        ResourceLoadState state = res->getLoadState();
        mikoAssert(state == RESOURCE_LOADED || state == RESOURCE_FAILED);

        // Remove the resource from the load queue
        i = loading.erase(i);

        // Add it to the cache
        Resource *tmp = state == RESOURCE_LOADED ? res : NULL;
        if (!cached.set(name, tmp)) {
            // TODO: handle error
        }
    }
    mutex.unlock();

    // We should have time for at least 1 background item per frame
    // TODO: might have to remove this when threads are implemented
    processBackgroundLoadItem();
}

Ref<Resource> ResourceManager::queueForLoading(ResourceType type, const std::string &path)
{
    HashMap<std::string, Ref<Resource> >::Iterator i;
    Ref<Resource> res;

    mutex.lock();

    // Look up the resource in cache first.
    i = cached.find(path);
    if (i != cached.end()) {
        res = i->val;
    }

    // Not found in cache? Look up loading?
    if (!res) {
        i = loading.find(path);
        if (i != loading.end()) {
            res = i->val;
        }
    }

    mutex.unlock();

    // Found it, no need to perform additional work
    if (res) {
        return res;
    }

    res = create(type, path);
    if (!res) {
        /// create does logging of errors already
        return NULL;
    }

    mutex.lock();

    /// Add the resource to the load queue
    if (!loading.set(path, res)) {
        res = NULL;
    }

    mutex.unlock();


    return res;
}

void ResourceManager::processBackgroundLoadItem()
{
    HashMap<std::string, Ref<Resource> >::Iterator i;
    Ref<Resource> res;

    mutex.lock();

    // Get the first queued item.
    for (i = loading.begin(); i != loading.end(); i++) {
        if (i->val->getLoadState() == RESOURCE_ASYNC_QUEUED) {
            res = i->val;
            break;
        }
    }

    // Mark the resource as loading so that no other thread will access it.
    if (res) {
        res->setLoadState(RESOURCE_ASYNC_LOADING);
    }

    mutex.unlock();

    // Nothing to do?
    if (!res) {
        return;
    }

    const std::string &name = res->getName();
    ResourceType type = res->getResourceType();

    SDL_RWops *rwops = locate(name);
    if (rwops) {
        if (!loadHelper(res, rwops)) {
            LOG_ERRORF("failed to load %s %s", toStr(type), name.c_str());
        }

        SDL_RWclose(rwops);
    }
    else {
        LOG_ERRORF("failed to find resource %s", name.c_str());
        res->setLoadState(RESOURCE_FAILED);
    }

    mutex.lock();
    ResourceLoadState state = res->getLoadState();

    // If the resource is queued for sync load, we can't do much with it.
    if (state != RESOURCE_SYNC_QUEUED) {
        mikoAssert(state == RESOURCE_LOADED || state == RESOURCE_FAILED);
        
        // Add it to the cache
        if (state == RESOURCE_LOADED) {
            if (!cached.set(name, res)) {
                // TODO: handle error
            }
        }
        else {
            if (!cached.set(name, NULL)) {
                // TODO: handle error
            }
        }

        // Remove the resource from load queue
        mikoAssert(loading.erase(name));

    }
    
    mutex.unlock();
}

bool ResourceManager::loadHelper(Resource *resource, SDL_RWops *rwops)
{
    if (!loadAsyncPart(resource, rwops)) {
        return false;
    }

    // We can do synchronous loading only in the main thread,
    // so if we're currently not in the main thread; the synchronous
    // loading part is left for later
    if (SDL_ThreadID() != ownerThread) {
        return true;
    }

    // OK, great we're in the main thread. Do the loading
    if (!loadSyncPart(resource)) {
        return false;
    }

    return true;
}

SDL_RWops * ResourceManager::locate(const std::string &path)
{
    for (unsigned i = 0; i < locators.size(); i++) {
        SDL_RWops *tmp = locators[i]->find(path);
        if (tmp) {
            return tmp;
        }
    }

    return NULL;
}

bool ResourceManager::loadAsyncPart(Resource *resource, SDL_RWops *rwops)
{
    resource->setLoadState(RESOURCE_ASYNC_LOADING);
    if (!resource->loadAsyncPart(rwops)) {
        LOG_DEBUGF("failed to load async part for %s", resource->getName().c_str());
        resource->setLoadState(RESOURCE_FAILED);
        return false;
    }

    resource->setLoadState(RESOURCE_SYNC_QUEUED);
    return true;
}

bool ResourceManager::loadSyncPart(Resource *resource)
{
    resource->setLoadState(RESOURCE_SYNC_LOADING);
    if (!resource->loadSyncPart(NULL)) {
        LOG_DEBUGF("failed to load sync part for %s", resource->getName().c_str());
        resource->setLoadState(RESOURCE_FAILED);
        return false;
    }

    resource->setLoadState(RESOURCE_LOADED);

    LOG_INFOF("%s '%s' loaded",
        toStr(resource->getResourceType()),
        resource->getName().c_str());

    return true;
}

void ResourceManager::waitLoaded(Resource *resource)
{
    if (!resource) {
        return;
    }

    mutex.lock();

    while (true) {
        ResourceLoadState state = resource->getLoadState();
        if (state == RESOURCE_FAILED || state == RESOURCE_LOADED) {
            break;
        }

        // Process other resources in the meantime
        mutex.unlock();
        processBackgroundLoadItem();
        mutex.lock();

        //loadedCond.wait(mutex);
    }

    mutex.unlock();
}

void ResourceManager::addLocator(IResourceLocator *locator)
{
    // dont add duplicates
    for (unsigned i = 0; i < locators.size(); i++) {
        if (locators[i] == locator) {
            return;
        }
    }

    if (!locators.append(locator)) {
        // TODO: handle error
    }
}

void ResourceManager::removeLocator(IResourceLocator *locator)
{
    for (unsigned i = 0; i < locators.size(); i++) {
        if (locators[i] == locator) {
            locators.remove(i);
        }
    }
}

void ResourceManager::addFactory(ResourceType type, IResourceFactory *factory)
{
    mikoAssert(factories[type] == NULL);

    // LOG_INFOF("registered %s factory", ResourceType::toStr(type));

    factories[type] = factory;
}

bool ResourceManager::isBackgroundLoadingComplete()
{
    bool ret;
    // just to be safe
    mutex.lock();
    ret = loading.size() == 0 ? true : false;
    mutex.unlock();

    return ret;
}

bool ResourceManager::getDirEntries(const std::string &path, ResourceEntryList *results)
{
    if (!validatePath(path)) {
        LOG_ERRORF("invalid resource path: '%s'", path.c_str());
        return false;
    }

    for (unsigned int i = 0; i < locators.size(); i++) {
        locators[i]->getDirContents(path, results);
    }
    
    return true;
}


bool ResourceManager::validatePath(const std::string &path)
{
    if (path.length() == 0) {
        return false;
    }

    if (path[0] != '/') {
        return false;
    }

    const char *s = path.c_str();

    for (unsigned int i = 0; i < path.length(); i++) {
        if (!isalnum(s[i]) && s[i] != '_' && s[i] != '.' && s[i] != '/') {
            return false;
        }

        // check against relative references
        if (s[i] == '/' && s[i + 1] == '.') {

            if (s[i + 2] == '/' || s[i + 2] == '\0') {
                return false;
            }

            if (s[i + 2] == '.' && (s[i + 3] == '/' || s[i + 3] == '\0')) {
                return false;
            }
        }
    }

    return true;
}

void ResourceManager::dumpInfo() const
{
    LOG_INFO("resource sources:");
    for (unsigned i = 0; i < locators.size(); i++) {
        locators[i]->dumpInfo();
    }
}

Ref<Resource> ResourceManager::getDirect(ResourceType type, const std::string &path)
{
    // LOG_DEBUGF("loading %s %s", toStr(type), path.c_str());
    HashMap<std::string, Ref<Resource> >::Iterator i;
    Ref<Resource> res;

    // Some sanity checking first
    if (!validatePath(path)) {
        LOG_ERRORF("invalid resource path: '%s'", path.c_str());
        return NULL;
    }

    // Need to get lock to access the shared part.
    mutex.lock();
    
    // Look up the resource in cache first.
    i = cached.find(path);
    if (i != cached.end()) {
        res = i->val;
    }

    // Not found in cache? Look up loading?
    if (!res) {
        i = loading.find(path);
        if (i != loading.end()) {
            res = i->val;
        }
    }

    mutex.unlock();

    // Not found in the loading? try to load it
    if (!res) {
        res = queueForLoading(type, path);
    }

    // Still not found? abort
    if (!res) {
        return NULL;
    }

    // Resource has incorrect type?
    if (res->getResourceType() != type) {
        LOG_ERRORF("resource %s requested as %s but it is loaded as %s",
            path.c_str(),
            toStr(res->getResourceType()),
            toStr(type)
        );
        return NULL;
    }

    // Success!
    return res;
}

Resource * ResourceManager::create(ResourceType type, const std::string &path)
{
    if (!factories[type]) {
        LOG_ERRORF("failed to create resource %s", path.c_str());
        LOG_DEBUGF("resource factory for %s not registered", toStr(type));
        return NULL;
    }

    Resource *res = factories[type]->create(*allocator);
    if (!res) {
        LOG_ERRORF("failed to create resource %s", path.c_str());
        LOG_DEBUGF("resource factory for %s returned null", toStr(type));
        return NULL;
    }

    res->setPath(path);
    res->setResourceManager(this);

    return res;
}
    
} /* namespace miko */
