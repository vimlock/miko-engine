#ifndef MIKO_RESOURCE_HANDLE_HPP
#define MIKO_RESOURCE_HANDLE_HPP

#include "miko/resource/resource.hpp"

namespace miko {

template <typename T>
class ResourceHandle
{
public:
    ResourceHandle();

    ResourceHandle(T *_ptr, const std::string &_name);

    /// Returns the name of the handle.
    /// @remarks The returned name might not be same as the one in the resource
    ///          referenced by this handle.
    const std::string& getName() const;

    /// Block the current thread until the resource referened by this handle is loaded.
    /// @remarks If the handle is NULL, the function returns immediately.
    ///          This function MUST be called before the handle can be used.
    void waitLoaded();

    /// Returns true if the resource is loaded
    /// @remarks If the handle is NULL, true is returned, even if the resource is not available.
    bool isReady() const;

    /// Convert the handle to the resource
    /// waitLoaded() must have been called before casting!
    operator T * ();

    /// Access the resource referenced by this handle
    Ref<T>& operator -> ();

private:
    Ref<T> ptr;
    std::string name;
    bool ready;
};
    
} /* namespace miko */

#include "miko/resource/resource_handle.inl"

#endif /* MIKO_RESOURCE_HANDLE_HPP */

