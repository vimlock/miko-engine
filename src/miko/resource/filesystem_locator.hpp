#ifndef MIKO_FILESYSTEM_LOCATOR_HPP
#define MIKO_FILESYSTEM_LOCATOR_HPP

#include "miko/resource/resource_locator.hpp"

namespace miko {

class FilesystemLocator : public IResourceLocator
{
public:
	/// @param path Root of the resource directory
    FilesystemLocator(const std::string &path);

    virtual SDL_RWops * find(const std::string &path);
    virtual bool getDirContents(const std::string &path, ResourceEntryList *result);
    virtual void dumpInfo() const;

    virtual const char * getType() const;
    virtual const char * getPath() const;

private:
    std::string base;

};
    
} /* namespace miko */

#endif /* MIKO_FILESYSTEM_LOCATOR_HPP */

