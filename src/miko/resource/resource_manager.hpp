#ifndef MIKO_RESOURCE_MANAGER_HPP
#define MIKO_RESOURCE_MANAGER_HPP

#include "miko/container/vector.hpp"
#include "miko/container/hash_map.hpp"

#include "miko/resource/resource.hpp"
#include "miko/resource/resource_handle.hpp"
#include "miko/resource/resource_locator.hpp"
#include "miko/resource/resource_loader.hpp"

#include "miko/util/mutex.hpp"

#include <SDL2/SDL_thread.h>

namespace miko {

class IResourceManager : public nocopy
{
public:
    virtual ~IResourceManager()
    {
        // empty
    }
private:
};

class ResourceManager : IResourceManager
{
public:
    friend class Resource;
    
    ResourceManager(Allocator &allocator, WorkQueue *workQueue);
    virtual ~ResourceManager();

    /// Must be called from the main thread
    void update();
    
    /// Give a hint to the resource loader that the given Resource is going to be
    /// needed soon.
    template <typename T>
    void preload(const std::string &path);

    /// Get a raw pointer to the resource.
    ///
    /// This function will block until the resource loading is complete.
    /// Use with caution
    ///
    /// This function should only be called from the thread which owns the
    /// ResourceManager
    template <typename T>
    T * get(const std::string &name);

    /// Get a handle to a resource.
    /// The returned handle is guarenteed to be ready and valid.
    /// This call will block until the resource is loaded.
    template <typename T>
    ResourceHandle<T> getHandle(const std::string &name);

    /// Get a handle to a resource.
    /// This function will not block but waitLoaded() method must be called on
    /// the returned handle before it can be used.
    /// Asynchronous resource loading is implemented using this function.
    template <typename T>
    ResourceHandle<T> getHandleNonBlocking(const std::string &name);

    /// Reloads every resource, note that references to the old versions still remain.
    void reloadAll();

    /// Reloads the given resource, note that references to the old version still remain.
    void reload(const std::string &name);

    /// Unloads every resource, note that references to the old versions still remain.
    void unloadAll();

    /// Unloads the given resource, note that references to the old versions still remain.
    void unload(const std::string &name);

    /// Unload every resource which is only referenced in the cache.
    void unloadUnreferenced();

    void addLocator(IResourceLocator *locator);
    void removeLocator(IResourceLocator *locator);

    void addFactory(ResourceType type, IResourceFactory *factory);
    void removeFactory(ResourceType type);

    /// Get list of all available resources in the given path and store them in results.
    /// @remarks If the path is not a directory, false is returned.
    bool getDirEntries(const std::string &path, ResourceEntryList *results);

    /// Pretty self explanatory.
    bool isBackgroundLoadingComplete();

    /// @todo Should probably remove
    void dumpInfo() const;

private:
    /// Gets a pointer to the given resource.
    /// If the resource does not exist, it will be marked for loading.
    /// @remarks The resource returned by this function might not be fully loaded
    ///          and therefore this function should not be exposed as public.
    Ref<Resource> getDirect(ResourceType type, const std::string &path);

    /// Creates a new resource of the given type
    /// On failure NULL is returned
    Resource * create(ResourceType type, const std::string &path);
    
    /// Queues a new resource from the given path for loading
    /// @remarks The returned resource might not be completly loaded.
    Ref<Resource> queueForLoading(ResourceType type, const std::string &path);

    /// Load the next queued resource.
    void processBackgroundLoadItem();

    /// Do most of the work required in loading.
    bool loadHelper(Resource *resource, SDL_RWops *rwops);

    /// Opens a RWops stream to the given path.
    /// If the path is a directory or does not exist, NULL is returned
    SDL_RWops * locate(const std::string &path);

    /// Load the asynchronous part of the resource
    /// @remarks This function can be called from any thread.
    bool loadAsyncPart(Resource *res, SDL_RWops *rwops);

    /// Load the synchronous part of the resource.
    /// @remarks This function MUST only be called from main thread.
    bool loadSyncPart(Resource *res);

    void waitLoaded(Resource *resource);

    /// Tests if the given path is is valid.
    /// Valid path is:
    ///   - not empty,
    ///   - absolute i.e. starts with /
    ///   - directories separated with /
    ///   - only ASCII alnums, dots, underscores and forwards slashes
    static bool validatePath(const std::string &path);

    /// Thread which was used to create this resource manager, which should be the main thread.
    SDL_threadID ownerThread;

    Vector<IResourceLocator *> locators;

    /// Factories which are used to instansiate new resources of the given type.
    IResourceFactory *factories[RESOURCE_TYPE_COUNT];

    /// Lock which must be acquired before accessing cached or loading dicts
    Mutex mutex;

    /// Signaled every time a resource is loaded.
    Condition loadedCond;

    /// Resources that have been loaded, value is NULL if the load has failed.
    /// @remarks Mutex lock must be acquired to access this
    HashMap<std::string, Ref<Resource> > cached;

    /// Resource that are currently being loaded.
    /// @remarks Mutex lock must be acquired to access this
    HashMap<std::string, Ref<Resource> > loading;

    WorkQueue *workQueue;
    Allocator *allocator;
};
    
} /* namespace miko */

#include "miko/resource/resource_manager.inl"

#endif /* MIKO_RESOURCE_MANAGER_HPP */

