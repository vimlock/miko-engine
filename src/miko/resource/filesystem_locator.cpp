#include "miko/resource/filesystem_locator.hpp"

#include "miko/io/filesys.hpp"
#include "miko/io/log.hpp"

#include "miko/util/assert.hpp"

#include <dirent.h>
#include <string.h>
#include <errno.h>

#include <sys/stat.h>

namespace miko {

FilesystemLocator::FilesystemLocator(const std::string &path):
    base(path)
{
    if (base.length() > 0 && base[path.length() - 1] != '/') {
        base += '/';
    }
}

SDL_RWops * FilesystemLocator::find(const std::string &path)
{
    std::string tmp(base + path);

    if (!fileExists(tmp)) {
        return NULL;
    }

    SDL_RWops *rwops = SDL_RWFromFile(tmp.c_str(), "r");
    if (!rwops) {
        LOG_ERRORF("failed to open file %s for reading: %s", tmp.c_str(), SDL_GetError());
        return NULL;
    }

    return rwops;
}

bool FilesystemLocator::getDirContents(const std::string &path, ResourceEntryList *results)
{
    std::string basepath = base + path;

    DIR *dir = opendir(basepath.c_str());
    if (!dir) {
        LOG_ERRORF("failed to open '%s' for reading: %s", basepath.c_str(), strerror(errno));
        return false;
    }

    bool ret = true;

    struct dirent *ent;
    while ((ent = readdir(dir))) {
        const char *name = ent->d_name;

        // Ignore links to current directory.
        if (name[0] == '.' && name[1] == '\0') {
            continue;
        }

        // Ignore links to parent directory.
        if (name[0] == '.' && name[1] == '.' && name[2] == '\0') {
            continue;
        }

        // Resource name, which is relative to the base directory.
        std::string respath = path;
        
        if (respath.length() > 0 && respath[respath.length() - 1] != '/') {
            respath += '/';
        }

        respath += name;
        
        // Don't add duplicates
        for (unsigned int i = 0; i < results->size(); i++) {
            if ((*results)[i].name == respath) {
                continue;
            }
        }

        // Path in the filesystem.
        std::string fspath = basepath + "/" + name;

        struct stat sb;
        if (stat(fspath.c_str(), &sb) == -1) {
            LOG_WARNF("failed to stat() '%s'", path.c_str());
            continue;
        }

        ResourceEntry::Type type;
        uint32_t size;
        if (S_ISREG(sb.st_mode)) {
            type = ResourceEntry::RESOURCE;
            size = sb.st_size;
        }
        else if (S_ISDIR(sb.st_mode)) {
            type = ResourceEntry::DIRECTORY;
            size = 0;
        }
        else {
            LOG_WARNF("special file '%s' in asset directory, ignoring", fspath.c_str());
            continue;
        }

        ResourceEntry e;
        e.name = name;
        e.path = respath;
        e.type = type;
        e.size = size;
        e.locator = this;

        if (!results->append(e)) {
            ret = false;
            break;
        }
    }

    closedir(dir);

    return ret;
}

void FilesystemLocator::dumpInfo() const
{
    LOG_INFOF("filesystem directory: %s", base.c_str());
}

const char * FilesystemLocator::getType() const
{
    return "directory";
}

const char * FilesystemLocator::getPath() const
{
    return base.c_str();
}
    
} /* namespace miko */
