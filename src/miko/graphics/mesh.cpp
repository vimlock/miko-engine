#include "miko/graphics/mesh.hpp"
#include "miko/graphics/skeleton.hpp"

#include "miko/io/text_serializer.hpp"
#include "miko/io/text_deserializer.hpp"

namespace miko {

SubMeshData::SubMeshData(Allocator &allocator):
    name(),
    indices(allocator),
    vertexFormat(),
    vertices(allocator),
    geometries(allocator)
{
}

bool SubMeshData::serialize(TextSerializer &s) const
{
    std::string formatStr = vertexFormat.toStr();

    s.writeIdentifier("mesh");
    s.writeWhitespace();

    s.writeString(name);
    s.writeWhitespace();

    s.writeRaw('{');
    s.indent();

    s.writeIdentifier("numIndices");
    s.writeWhitespace();
    s.write(indices.size());
    s.writeNewline();

    s.writeIdentifier("numVertices");
    s.writeWhitespace();
    if (vertices.size() > 0) {
        s.write(vertices.size() / vertexFormat.vertexSize());
    }
    else {
        s.write(0);
    }

    s.writeNewline();

    s.writeIdentifier("vertexFormat");
    s.writeWhitespace();
    s.writeString(formatStr);
    s.writeNewline();

    serializeIndices(s);
    serializeVertices(s);
    serializeGeometries(s);

    s.dedent();
    s.writeRaw('}');
    s.writeNewline();

    return true;
}

void SubMeshData::serializeIndices(TextSerializer &s) const
{
    s.writeIdentifier("indices");
    s.writeWhitespace();

    s.writeRaw('{');
    s.indent();

    for (unsigned int i = 0; i < indices.size(); i++) {
        if (i != 0 && i % 20 == 0) {
            s.writeNewline();
        }
        else if (i != 0) {
            s.writeWhitespace();
        }

        s.writeFmt("%-2u", indices[i]);
    }

    s.dedent();
    s.writeRaw('}');

    s.writeNewline();
}

void SubMeshData::serializeVertices(TextSerializer &s) const
{
    s.writeIdentifier("vertices");
    s.writeWhitespace();

    s.writeRaw('{');
    s.indent();

    uint32_t size = vertexFormat.vertexSize();
    for (unsigned int i = 0; i < vertices.size(); i += size) {
        const char *data = &vertices[i];

        for (unsigned int i = 0; i < COMPONENT_NAME_COUNT; i++) {
            VertexComponentName name = static_cast<VertexComponentName>(i);

            if (!vertexFormat.hasComponent(name)) {
                continue;
            }

            unsigned offset = vertexFormat.componentOffset(name);
            VertexComponentType type = vertexFormat.componentType(name);

            if (type == COMPONENT_FLOAT) {
                s.write(*reinterpret_cast<const float *>(&data[offset]));
            }
            else if (type == COMPONENT_VEC2) {
                s.write(*reinterpret_cast<const glm::vec2*>(&data[offset]));
            }
            else if (type == COMPONENT_VEC3) {
                s.write(*reinterpret_cast<const glm::vec3*>(&data[offset]));
            }
            else if (type == COMPONENT_VEC4) {
                s.write(*reinterpret_cast<const glm::vec4*>(&data[offset]));
            }
            else {
                mikoAssert(false);
            }

            s.writeWhitespace();
        }

        s.writeNewline();
    }

    s.dedent();
    s.writeRaw('}');
    s.writeNewline();
}

void SubMeshData::serializeGeometries(TextSerializer &s) const
{
    for (unsigned int i = 0; i < geometries.size(); i++) {
        const GeometryData &geometry = geometries[i];

        s.writeIdentifier("geometry");
        s.writeWhitespace();

        s.writeRaw('{');
        s.indent();

        s.writeIdentifier("material");
        s.writeWhitespace();

        s.writeString(geometry.materialName);
        s.writeNewline();

        s.writeIdentifier("primitive");
        s.writeWhitespace();
        s.writeIdentifier("triangles");
        s.writeNewline();

        s.writeIdentifier("indexOffset");
        s.writeWhitespace();
        s.write(geometry.indexOffset);
        s.writeNewline();

        s.writeIdentifier("indexCount");
        s.writeWhitespace();
        s.write(geometry.indexCount);
        s.writeNewline();

        s.dedent();
        s.writeRaw('}');

        s.writeNewline();
    }
}


bool SubMeshData::deserialize(TextDeserializer &s)
{
    if (!s.skipIdentifier("mesh")) {
        return false;
    }

    if (!s.read(name)) {
        return false;
    }

    if (!s.skip(Token::LCURLY)) {
        return false;
    }

    // Read the memory hints
    uint32_t numIndices;
    uint32_t numVertices;

    if (!s.skipIdentifier("numIndices")) {
        return false;
    }

    if (!s.read(numIndices)) {
        return false;
    }

    if (!s.skipIdentifier("numVertices")) {
        return false;
    }

    if (!s.read(numVertices)) {
        return false;
    }

    // Read and validate the vertex format
    if (!s.skipIdentifier("vertexFormat")) {
        return false;
    }

    if (!s.expect(Token::STRING)) {
        return false;
    }
    
    if (!vertexFormat.addFromFormat(s.nextToken().str)) {
        return false;
    }

    s.advance();

    indices.reserve(numIndices);
    vertices.reserve(numVertices * vertexFormat.vertexSize());

    if (!deserializeIndices(s)) {
        return false;
    }

    if (!deserializeVertices(s)) {
        return false;
    }

    if (!deserializeGeometries(s)) {
        return false;
    }

    if (!s.skip(Token::RCURLY)) {
        return false;
    }

    if (!s.expect(Token::END)) {
        return false;
    }

    return true;
}

bool SubMeshData::deserializeIndices(TextDeserializer &s)
{
    if (!s.skipIdentifier("indices")) {
        return false;
    }

    if (!s.skip(Token::LCURLY)) {
        return false;
    }

    while (s.accept(Token::INT) || s.accept(Token::FLOAT)) {
        uint16_t tmp;
        if (!s.read(tmp)) {
            return false;
        }

        if (!indices.append(tmp)) {
            return false;
        }
    }

    if (!s.skip(Token::RCURLY)) {
        return false;
    }
    
    return true;
}

static void append(Vector<char> &v, const void *data, unsigned int size)
{
    unsigned int offset = v.size();
    v.resize(v.size() + size);
    memcpy(&v[offset], data, size);
}

bool SubMeshData::deserializeVertices(TextDeserializer &s)
{
    if (!s.skipIdentifier("vertices")) {
        return false;
    }

    if (!s.skip(Token::LCURLY)) {
        return false;
    }

    while (!s.accept(Token::RCURLY)) {
        for (unsigned int i = 0; i < COMPONENT_NAME_COUNT; i++) {
            VertexComponentName name = static_cast<VertexComponentName>(i);

            if (!vertexFormat.hasComponent(name)) {
                continue;
            }

            VertexComponentType type = vertexFormat.componentType(name);
            switch (type) {
            case COMPONENT_FLOAT: {
                float tmp;
                if (!s.read(tmp)) {
                    return false;
                }
                append(vertices, &tmp, sizeof(tmp));
                break;
            }
            case COMPONENT_VEC2: {
                glm::vec2 tmp;
                if (!s.read(tmp)) {
                    return false;
                }
                append(vertices, &tmp, sizeof(tmp));
                break;
            }
            case COMPONENT_VEC3: {
                glm::vec3 tmp;
                if (!s.read(tmp)) {
                    return false;
                }
                append(vertices, &tmp, sizeof(tmp));
                break;
            }
            case COMPONENT_VEC4: {
                glm::vec4 tmp;
                if (!s.read(tmp)) {
                    return false;
                }
                append(vertices, &tmp, sizeof(tmp));
                break;
            }
            default:
                mikoUnreachable();
                return false;
            }
        }
    }

    if (!s.skip(Token::RCURLY)) {
        return false;
    }

    return true;
}

bool SubMeshData::deserializeTextures(TextDeserializer &s)
{
    return true;
}

bool SubMeshData::deserializeGeometries(TextDeserializer &s)
{
    while (s.acceptIdentifier("geometry")) {
        s.advance();

        if (!geometries.append(GeometryData())) {
            return false;
        }

        GeometryData &geometry = geometries.back();

        if (!s.skip(Token::LCURLY)) {
            return false;
        }

        if (!s.skipIdentifier("material")) {
            return false;
        }

        if (!s.read(geometry.materialName)) {
            return false;
        }

        if (!s.skipIdentifier("primitive")) {
            return false;
        }

        if (!s.skipIdentifier("triangles")) {
            return false;
        }

        if (!s.skipIdentifier("indexOffset")) {
            return false;
        }

        if (!s.read(geometry.indexOffset)) {
            return false;
        }

        if (!s.skipIdentifier("indexCount")) {
            return false;
        }

        if (!s.read(geometry.indexCount)) {
            return false;
        }

        if (!s.skip(Token::RCURLY)) {
            return false;
        }
    }

    return true;
}

MeshData::MeshData(Allocator &_allocator):
    submeshes(_allocator),
    allocator(_allocator)
{
}

MeshData::~MeshData()
{
    for (unsigned int i = 0; i < submeshes.size(); i++) {
        MIKO_DELETE(allocator, submeshes[i]);
    }
}

bool MeshData::serialize(TextSerializer &s) const
{
    if (skeleton) {
        skeleton->serialize(s);
    }

    for (unsigned int i = 0; i < submeshes.size(); i++) {
        mikoAssert(submeshes[i] != NULL);
        if (!submeshes[i]->serialize(s)) {
            return false;
        }
    }

    return true;
}

bool MeshData::deserialize(TextDeserializer &s)
{
    if (s.acceptIdentifier("skeleton")) {
        s.advance();

        Skeleton *skeleton = addSkeleton();
        if (!skeleton) {
            return false;
        }

        if (!skeleton->deserialize(s)) {
            return false;
        }
    }

    while (true) {
        if (s.accept(Token::END)) {
            break;
        }
        else 
        {
            SubMeshData *submesh = addSubmesh();
            if (!submesh) {
                return false;
            }

            if (!submesh->deserialize(s)) {
                return false;
            }
        }
    }

    return true;
}

void MeshData::dump() const
{
    LOG_INFOF("mesh (%u submeshes)", submeshes.size());
    SDL_RWops *rwops = SDL_RWFromFP(stdout, SDL_FALSE);
    TextSerializer s(rwops, true);
    serialize(s);
}

SubMeshData * MeshData::addSubmesh()
{
    SubMeshData *submesh = MIKO_NEW(allocator, SubMeshData) (allocator);
    if (!submesh) {
        return NULL;
    }

    if (!submeshes.append(submesh)) {
        MIKO_DELETE(allocator, submesh);
        return NULL;
    }

    return submesh;
}

Skeleton * MeshData::addSkeleton()
{
    if (skeleton) {
        return skeleton;
    }

    Allocator &allocator = submeshes.getAllocator();
    skeleton = MIKO_NEW(allocator, Skeleton) (allocator);

    return skeleton;
}

Skeleton * MeshData::getSkeleton()
{
    return skeleton;
}
    
} /* namespace miko */
