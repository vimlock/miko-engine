#include "miko/graphics/animation.hpp"

#include "miko/io/text_serializer.hpp"
#include "miko/io/text_deserializer.hpp"

namespace miko {

SkeletonAnimationChannel::SkeletonAnimationChannel(Allocator &allocator):
    frames(allocator)
{
    // empty
}

bool SkeletonAnimationChannel::serialize(TextSerializer &s) const
{
    s.writeIdentifier("channel");
    s.writeWhitespace();
    s.writeString(name);
    s.writeWhitespace();
    s.writeRaw('{');
    s.indent();

    s.writeIdentifier("numFrames");
    s.writeWhitespace();
    s.write(frames.size());
    s.writeNewline();

    for (unsigned i = 0; i < frames.size(); i++) {
        if (i != 0) {
            s.writeNewline();
        }

        const SkeletonAnimationFrame &frame = frames[i];
        s.write(frame.time);
        s.writeWhitespace();

        s.write(frame.position);
        s.writeWhitespace();

        s.write(frame.rotation);
    }

    s.dedent();
    s.writeRaw('}');
    s.writeNewline();

    return false;
}

bool SkeletonAnimationChannel::deserialize(TextDeserializer &s)
{
    if (!s.skip(Token::LCURLY)) {
        return false;
    }

    if (!s.skipIdentifier("numFrames")) {
        return false;
    }

    uint32_t numFrames;
    if (!s.read(numFrames)) {
        return false;
    }

    frames.reserve(numFrames);

    while (!s.accept(Token::RCURLY)) {
        float time;
        glm::vec3 position;
        glm::quat rotation;

        if (!s.read(time) || !s.read(position) || !s.read(rotation)) {
            return false;
        }

        SkeletonAnimationFrame *frame = addFrame();
        if (!frame) {
            return false;
        }

        frame->time = time;
        frame->position = position;
        frame->rotation = rotation;
    }

    if (!s.skip(Token::RCURLY)) {
        return false;
    }

    return true;
}

SkeletonAnimationFrame * SkeletonAnimationChannel::addFrame()
{
    if (!frames.append(SkeletonAnimationFrame())) {
        return NULL;
    }

    return &frames.back();
}

SkeletonAnimationFrame * SkeletonAnimationChannel::getFrame(unsigned int index)
{
    if (index < frames.size()) {
        return &frames[index];
    }
    else {
        return NULL;
    }
}

unsigned int SkeletonAnimationChannel::getFrameByTime(float time)
{
    if (frames.size() == 1) {
        return 0;
    }

    for (unsigned int i = 0; i < frames.size() - 1; i++) {
        if (time < frames[i + 1].time) {
            return i;
        }
    }

    mikoUnreachable();
    return 0;
}


SkeletonAnimation::SkeletonAnimation(Allocator &allocator):
    duration(0.0f),
    speed(0.0f),
    channels(allocator)
{
}

SkeletonAnimation::~SkeletonAnimation()
{
    Allocator &allocator = channels.getAllocator();

    for (unsigned int i = 0; i < channels.size(); i++) {
        MIKO_DELETE(allocator, channels[i]);
    }
}

bool SkeletonAnimation::loadAsyncPart(SDL_RWops *rwops)
{
    TextDeserializer s(getName().c_str(), rwops);
    return deserialize(s);
}

bool SkeletonAnimation::loadSyncPart(SDL_RWops *rwops)
{
    return true;
}

bool SkeletonAnimation::serialize(TextSerializer &s) const
{
    s.writeIdentifier("numChannels");
    s.writeWhitespace();
    s.write(channels.size());
    s.writeNewline();

    s.writeIdentifier("duration");
    s.writeWhitespace();
    s.write(duration);
    s.writeNewline();

    s.writeIdentifier("speed");
    s.writeWhitespace();
    s.write(speed);
    s.writeNewline();

    for (unsigned int i = 0; i < channels.size(); i++) {
        channels[i]->serialize(s);
    }

    return true;
}

bool SkeletonAnimation::deserialize(TextDeserializer &s)
{
    uint32_t numChannels;

    if (!s.skipIdentifier("numChannels")) {
        return false;
    }

    if (!s.read(numChannels)) {
        return false;
    }

    channels.reserve(numChannels);

    if (!s.skipIdentifier("duration")) {
        return false;
    }

    if (!s.read(duration)) {
        return false;
    }

    if (!s.skipIdentifier("speed")) {
        return false;
    }

    if (!s.read(speed)) {
        return false;
    }

    while (!s.accept(Token::END)) {
        if (!s.skipIdentifier("channel")) {
            return false;
        }

        std::string name;
        
        if (!s.read(name)) {
            return false;
        }

        SkeletonAnimationChannel *channel = addChannel();
        if (!channel) {
            return false;
        }

        channel->name = name;

        if (!channel->deserialize(s)) {
            return false;
        }
    }

    if (!s.expect(Token::END)) {
        return false;
    }

    return true;
}

SkeletonAnimationChannel * SkeletonAnimation::addChannel()
{
    Allocator &allocator = channels.getAllocator();

    SkeletonAnimationChannel *channel = MIKO_NEW(allocator, SkeletonAnimationChannel) (allocator);
    if (!channel) {
        return NULL;
    }

    if (!channels.append(channel)) {
        MIKO_DELETE(allocator, channel);
        return NULL;
    }

    return channel;
}

int SkeletonAnimation::getChannelIndex(const std::string &name)
{
    for (int i = 0; i < (int)channels.size(); i++) {
        if (channels[i]->name == name) {
            return i;
        }
    }

    return -1;
}

SkeletonAnimationChannel * SkeletonAnimation::getChannel(int index)
{
    if (index >= 0 && index < (int)channels.size()) {
        return channels[index];
    }
    else {
        return NULL;
    }
}

SkeletonAnimation * SkeletonAnimationFactory::create(Allocator &allocator)
{
    return MIKO_NEW(allocator, SkeletonAnimation) (allocator);
}
    
} /* namespace miko */
