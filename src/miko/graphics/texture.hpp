#ifndef MIKO_TEXTURE_HPP
#define MIKO_TEXTURE_HPP

#include "miko/resource/resource.hpp"
#include "miko/graphics/graphics.hpp"

#include <glm/glm.hpp>

namespace miko {

class ITexture : public Resource
{
public:
    MIKO_RESOURCE(RESOURCE_TEXTURE)

    virtual uint32_t getWidth() const = 0;
    virtual uint32_t getHeight() const = 0;
};

    
} /* namespace miko */

#endif /* MIKO_TEXTURE_HPP */

