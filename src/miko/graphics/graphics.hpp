#ifndef MIKO_GRAPHICS_HPP
#define MIKO_GRAPHICS_HPP

#include "miko/miko.hpp"
#include "miko/container/vector.hpp"

#include <glm/glm.hpp>
#include <string>

namespace miko {

#define MIKO_MAX_BONES 64
#define MIKO_MAX_BONES_PER_VERTEX 4

/// Describes what primitives are used to render given vertices
enum PrimitiveType
{
    PRIMITIVE_POINTS,

    PRIMITIVE_LINE_STRIP,
    PRIMITIVE_LINE_LOOP,
    PRIMITIVE_LINES,

    PRIMITIVE_TRIANGLE_STRIP,
    PRIMITIVE_TRIANGLE_FAN,
    PRIMITIVE_TRIANGLES
};

/// Describes a shader attachments type
enum ShaderType
{
    SHADER_VERTEX_OBJECT,
    SHADER_FRAGMENT_OBJECT
};

/// Hint to a shader how a attribute should be interpolated
enum ShadingType
{
    SHADING_SMOOTH,
    SHADING_FLAT
};

/// Hint to a shader how nice looking result we want
enum ShadingQuality
{
    SHADING_QUALITY_HIGH,
    SHADING_QUALITY_LOW
};

enum DepthTestType
{
    DEPTH_TEST_NEVER,
    DEPTH_TEST_LESS,
    DEPTH_TEST_LEQUAL,
    DEPTH_TEST_GREATER,
    DEPTH_TEST_GEQUAL,
    DEPTH_TEST_EQUAL,
    DEPTH_TEST_NOTEQUAL,
    DEPTH_TEST_ALWAYS
};

enum BlendType
{
    /// GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA 
    BLEND_ALPHA,

    /// GL_ONE, GL_ONE, GL_FUNC_ADD
    BLEND_ADDITIVE, 

    /// GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA
    BLEND_INTERPOLATIVE,

    /// GL_DST_COLOR, GL_ZERO, GL_FUNC_ADD
    BLEND_MULTIPLICATIVE,

    /// GL_ONE, GL_ZERO
    BLEND_REPLACE,

    /// GL_SRC_ALPHA, GL_ONE, GL_FUNC_ADD
    BLEND_OVERLAY,

    /// GL_ONE, GL_ONE_MINUS_SRC_COLOR
    BLEND_SCREEN
};

enum TextureFormatType
{
    TEXTURE_FORMAT_R,
    TEXTURE_FORMAT_RG,
    TEXTURE_FORMAT_RGB,
    TEXTURE_FORMAT_RGBA,
};

enum TextureSamplerType
{
    TEXTURE_SAMPLER_LINEAR,
    TEXTURE_SAMPLER_NEAREST
};

enum FillType
{
    FILL_POINT,
    FILL_LINE,
    FILL_FACE
};

enum FaceCullType
{
    CULL_NONE,
    CULL_FRONT_AND_BACK
};

enum FaceCullOrder
{
    CULL_CW,
    CULL_CCW
};

class GraphicsObject
{
public:
    virtual ~GraphicsObject()
    {
        // empty
    }
};

enum VertexComponentName
{
    COMPONENT_POSITION = 0,
    COMPONENT_TEXCOORD,
    COMPONENT_NORMAL,
    COMPONENT_TANGENT,
    COMPONENT_COLOR,
    COMPONENT_ALPHA,
    COMPONENT_BONE_WEIGHT,
    COMPONENT_BONE_INDEX,

    COMPONENT_NAME_COUNT
};

enum VertexComponentType
{
    COMPONENT_FLOAT,
    COMPONENT_VEC2,
    COMPONENT_VEC3,
    COMPONENT_VEC4,

    COMPONENET_TYPE_COUNT
};

/// Get size of a component in bytes
uint32_t vertexComponentSize(VertexComponentType t);

/// Get number of members in the component
uint32_t vertexComponentNumMembers(VertexComponentType t);

class VertexComponent
{
public:
    /// Packed VertexComponentType
    uint8_t type;

    /// Offset in bytes to start of the vertex data
    uint8_t offset;
};

class VertexFormat
{
public:
    /// Construct an empty vertex format
    VertexFormat();

    /// Constructor which calls addFromFormat afterwards
    VertexFormat(const char *fmt);

    /// Adds components from string format
    /// @remarks
    /// Format: { name type }
    /// Name: p position 
    ///       t texcoord
    ///       n normal
    ///       g tangent
    ///       c color
    ///       a alpha
    ///       w weight
    ///       b bone
    /// 
    /// Type: 1 float
    //        2 vec2
    //        3 vec3
    //        4 vec4
    ///        
    /// Example: "p3t2"
    bool addFromFormat(const char *fmt);

    std::string toStr() const;

    /// Adds a new component to the vertex
    /// @remarks Component with the same name must not exists already
    /// @todo    Take care of alignment
    bool addComponent(VertexComponentName name, VertexComponentType type);

    /// @return  true if the format has the given component
    bool hasComponent(VertexComponentName name) const;

    /// @return  Offset of the component in bytes
    /// @remarks return -1 if the component does not exists
    int componentOffset(VertexComponentName name) const;

    VertexComponentType componentType(VertexComponentName name) const;

    /// @return size of an invidual vertex in bytes
    uint32_t vertexSize() const;

private:
    uint32_t componentMask;
    uint32_t size;
    VertexComponent components[8];

};

// Convenience classes for building vertices
struct Vertex2
{
    glm::vec2 position;
    glm::vec2 texcoords;
};

struct Vertex3
{
    glm::vec3 position;
    glm::vec2 texcoords;
};

struct Vertex3N
{
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 texcoords;
};

struct Vertex3NT
{
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec3 tangent;
    glm::vec2 texcoords;
};

const char * toStr(ShaderType v);
const char * toStr(ShadingType v);
const char * toStr(ShadingQuality v);

const char * toStr(TextureFormatType v);
const char * toStr(TextureSamplerType v);

const char * toStr(VertexComponentName n);
const char * toStr(VertexComponentType t);

} /* namespace miko */

#endif /* MIKO_GRAPHICS_HPP */

