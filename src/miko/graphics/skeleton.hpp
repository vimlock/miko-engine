#ifndef MIKO_SKELETON_HPP
#define MIKO_SKELETON_HPP

#include "miko/core/object.hpp"
#include "miko/container/vector.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <string>

// spooky scary skeletons

namespace miko {

class TextSerializer;
class TextDeserializer;

class Bone
{
public:
    Bone();

    std::string name;

    /// local translation
    glm::vec3 position;

    /// local rotation
    glm::quat rotation;

    /// tranform from model space into this bone
    glm::mat4 worldMatrix;

    /// inverseBindPose * worldMatrix
    glm::mat4 offsetMatrix;

    /// Offset to our parent or -1 on root bone
    int parent;

    /// Offset to our first child or -1 if we do not have children
    int child;

    /// Offset to our sibling or -1 if we are the last sibling
    int sibling;

    /// true if the bone has vertex weights bound to it
    bool skinned;
};

class Skeleton: public Object
{
public:
    Skeleton(Allocator &allocator);

    bool serialize(TextSerializer &s) const;
    bool deserialize(TextDeserializer &s);

    /// Adds a bone to the skeleton.
    /// @param name     Name of the bone, which must be unique.
    /// @param parent   Parent of the bone,  which must exists or -1 for root bone.
    /// @return         Pointer to the newly created bone.
    /// @remarks        returns NULL on failure.
    Bone * addBone(const std::string &name, int parent);

    /// Get bone by name, slow!
    Bone * getBone(const std::string &name);

    /// Get bone by index, fast!
    Bone * getBone(unsigned int index);

    /// Returns index of a bone named @p name or -1 no such bone was found
    int getBoneIndex(const std::string &name);

    /// Returns pointer to the root bone or NULL if we do not have root (empty skeleton)
    Bone * getRootBone();

    void dump();

    /// Returns index of the root bone or -1 if we do not have root (empty skeleton)
    int getRootBoneIndex();

    /// Returns the number of bones in the skeleton
    unsigned int numBones() const;

private:
    void calculateSiblings();

    void dumpBone(int index, int indent);

    /// Every bone in this skeleton
    Vector<Bone> bones;

    /// Root bone in bones
    int root;

};
    
} /* namespace miko */

#endif /* MIKO_SKELETON_HPP */

