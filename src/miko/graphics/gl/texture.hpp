#ifndef MIKO_GL_TEXTURE_HPP
#define MIKO_GL_TEXTURE_HPP

#include "miko/graphics/gl/graphics.hpp"
#include "miko/graphics/texture.hpp"
#include "miko/resource/resource_loader.hpp"

namespace miko {

/// OpenGL texture object
class GlTexture : public ITexture
{
public:
    friend class TextureFactory;

    static GlTexture * create(TextureFormatType format, uint32_t width, uint32_t height);

    virtual ~GlTexture();

    virtual bool loadAsyncPart(SDL_RWops *rwops);
    virtual bool loadSyncPart(SDL_RWops *rwops);
    
    void clear();

    bool isLoadedToGpu() const;
    bool loadToGpu();
    void unloadFromGpu();

    bool isLoadedToMemory() const;
    void loadToMemory();
    void unloadFromMemory();

    /// Loads the texture from the stream into the main memory.
    /// Method loadToGpu() must be called before the texture can be used for 
    /// rendering.
    bool unserialize(SDL_RWops *rwops);

    /// Write the content of the texture into the given stream in JPEG format.
    /// The texture must be loaded to main memory before calling this function.
    bool serializeJPEG(SDL_RWops *rwops) const;

    /// Write the content of the texture into the given stream in PNG format.
    /// The texture must be loaded to main memory before calling this function.
    bool serializePNG(SDL_RWops *rwops) const;

    void bind();

    /// returns the reference to the object on GPU
    GLuint getGpuObject();

    virtual uint32_t getWidth() const
    {
        return width;
    }

    virtual uint32_t getHeight() const
    {
        return height;
    }

private:
    /// Private constructor, use create() instead
    GlTexture();

    bool loadToGpuEx(
        TextureFormatType internalFormat,
        TextureFormatType dataFormat,
        bool padd,
        GLint dataWidth,
        GLint dataHeight,
        const GLvoid *data
    );

    /// width of the orignal image in pixels
    uint32_t width;

    /// height of the original image in pixels
    uint32_t height;

    /// real width of the image on gpu which is padded to next-power-of-two
    uint32_t widthPadded;

    /// real height of the image on gpu which is padded to next-power-of-two
    uint32_t heightPadded;

    /// content of the texture if the texture is mapped to main memory
    unsigned char *pixeldata;

    TextureFormatType format;

    GLuint gpuObject;
};

class TextureFactory : public IResourceFactory
{
public:
    virtual GlTexture * create(Allocator &allocator);

};
    
} /* namespace miko */

#endif /* MIKO_GL_TEXTURE_HPP */

