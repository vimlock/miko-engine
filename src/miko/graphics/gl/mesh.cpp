#include "miko/graphics/gl/mesh.hpp"
#include "miko/io/log.hpp"
#include "miko/io/text_serializer.hpp"
#include "miko/io/text_deserializer.hpp"

#include "miko/resource/resource_manager.hpp"

namespace miko {

GlSubMesh::GlSubMesh(Allocator &allocator):
    vertexFormat(),
    indices(),
    vertices(),
    geometries(allocator)
{
    // empty
}

GlMesh::GlMesh(Allocator &allocator):
    isLoaded(false),
    submeshes(allocator),
    meshData(NULL)
{
    // empty
}

GlMesh::~GlMesh()
{
    unloadFromMemory();
    unloadFromGpu();
}

bool GlMesh::loadAsyncPart(SDL_RWops *rwops)
{
    mikoAssert(meshData == NULL);

    // Use the same allocator to store the mesh data as the one given in the constructor
    Allocator &allocator = submeshes.getAllocator();

    MeshData *mesh = MIKO_NEW(allocator, MeshData)(allocator);
    if (!mesh) {
        return false;
    }

    TextDeserializer s(getName().c_str(), rwops);

    if (!mesh->deserialize(s)) {
        LOG_ERRORF("failed to deserialize %s", getName().c_str());
        MIKO_DELETE(allocator, mesh);
        return false;
    }

    meshData = mesh;
    skeleton = meshData->getSkeleton();

    return true;
}

bool GlMesh::loadSyncPart(SDL_RWops *rwops)
{
    return loadToGpu();
}

bool GlMesh::isLoadedToMemory() const
{
    return meshData;
}

void GlMesh::unloadFromMemory()
{
    if (meshData) {
        MIKO_DELETE(defaultAllocator, meshData);
    }
}

bool GlMesh::isLoadedToGpu() const
{
    return isLoaded;
}

bool GlMesh::loadToGpu()
{
    if (isLoadedToGpu()) {
        return true;
    }

    if (!isLoadedToMemory()) {
        LOG_ERRORF("%s was not loaded to memory", getName().c_str());
        return false;
    }

    ResourceManager *resourceManager = getResourceManager();

    for (unsigned int i = 0; i < meshData->numSubMeshes(); i++) {
        const SubMeshData *data = meshData->getSubMesh(i);

        GlSubMesh *mesh = addSubmesh();
        if (!mesh) {
            return false;
        }

        mesh->indices = GlIndexBuffer::create(
            BufferUsageHint::STATIC_DRAW,
            data->indices.size(),
            &data->indices[0]
        );

        if (!mesh->indices) {
            return false;
        }

        mesh->vertexFormat = data->vertexFormat;
        mesh->vertices = GlVertexBuffer::create(
            mesh->vertexFormat,
            BufferUsageHint::STATIC_DRAW,
            data->vertices.size() / data->vertexFormat.vertexSize(),
            &data->vertices[0]
        );

        if (!mesh->vertices) {
            return false;
        }

        for (unsigned int i = 0; i < data->geometries.size(); i++) {
            GlGeometry geometry;
            geometry.material = resourceManager->getHandleNonBlocking<GlMaterial>(data->geometries[i].materialName);
            geometry.primitive = PRIMITIVE_TRIANGLES; 
            geometry.indexOffset = data->geometries[i].indexOffset;
            geometry.indexCount = data->geometries[i].indexCount;

            if (!mesh->geometries.append(geometry)) {
                return false;
            }
        }

        for (unsigned int i = 0; i < mesh->geometries.size(); i++) {
            mesh->geometries[i].material.waitLoaded();
        }
    }

    isLoaded = true;

    return true;
}

void GlMesh::unloadFromGpu()
{
    Allocator &allocator = submeshes.getAllocator();

    for (unsigned int i = 0; i < submeshes.size(); i++) {
        MIKO_DELETE(allocator, submeshes[i]);
    }
}

GlSubMesh * GlMesh::addSubmesh()
{
    Allocator &allocator = submeshes.getAllocator();

    GlSubMesh *mesh = MIKO_NEW(allocator, GlSubMesh) (allocator);
    if (!mesh) {
        return NULL;
    }

    if (!submeshes.append(mesh)) {
        MIKO_DELETE(allocator, mesh);
        return NULL;
    }

    return mesh;
}

GlMesh * GlMeshFactory::create(Allocator &allocator)
{
    return MIKO_NEW(allocator, GlMesh) (allocator);
}

    
} /* namespace miko */
