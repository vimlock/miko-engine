#include "miko/graphics/gl/buffer.hpp"
#include "miko/util/assert.hpp"
#include "miko/io/log.hpp"

namespace miko {

static GLenum usageEnum(BufferUsageHint::Enum e)
{
    switch (e) {
    case BufferUsageHint::STREAM_DRAW: return GL_STREAM_DRAW;
    case BufferUsageHint::STREAM_READ: return GL_STREAM_READ;
    case BufferUsageHint::STREAM_COPY: return GL_STREAM_COPY;

    case BufferUsageHint::DYNAMIC_DRAW: return GL_DYNAMIC_DRAW;
    case BufferUsageHint::DYNAMIC_READ: return GL_DYNAMIC_READ;
    case BufferUsageHint::DYNAMIC_COPY: return GL_DYNAMIC_COPY;

    case BufferUsageHint::STATIC_DRAW: return GL_STATIC_DRAW;
    case BufferUsageHint::STATIC_READ: return GL_STATIC_READ;
    case BufferUsageHint::STATIC_COPY: return GL_STATIC_COPY;
    }

    mikoUnreachable();
    return BufferUsageHint::STREAM_DRAW;
}

// ============================================================================
// GlIndexBuffer

GlIndexBuffer::GlIndexBuffer(BufferUsageHint::Enum _usage,
        uint32_t _numReserved,
        GLuint _gpuObject):
    usage(_usage),
    numReserved(_numReserved),
    gpuObject(_gpuObject)
{
    // empty
}

GlIndexBuffer::~GlIndexBuffer()
{
    if (gpuObject) {
        glDeleteBuffers(1, &gpuObject);
    }
}

GlIndexBuffer * GlIndexBuffer::create(
        BufferUsageHint::Enum usage,
        uint32_t numIndices,
        const IndexType *indices)
{
    GLuint tmp = 0;
    uint32_t size = numIndices * sizeof(IndexType);

    glGenBuffers(1, &tmp);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tmp);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, indices, usageEnum(usage));
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    CHECK_GL_ERROR(__func__);

    GlIndexBuffer *buf = MIKO_NEW(refcountedAllocator, GlIndexBuffer) (
        usage, numIndices, tmp);

    if (!buf) {
        glDeleteBuffers(1, &tmp);
    }
    else {
        // LOG_INFOF("index buffer %uB created", size);
    }

    return buf;
}

bool GlIndexBuffer::resize(
        uint32_t numIndices,
        const IndexType *indices,
        bool orphan)
{
    /// @todo
    mikoAssert(false && "unimplemented");
}

bool GlIndexBuffer::updateRange(uint32_t startIndex, uint32_t count, const IndexType *data)
{
    /// @todo
    mikoAssert(false && "unimplemented");
}

void GlIndexBuffer::bind()
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuObject);
}

BufferUsageHint::Enum GlIndexBuffer::getUsage() const
{
    return usage;
}

uint32_t GlIndexBuffer::size() const
{
    return numReserved;
}

uint32_t GlIndexBuffer::maxSize()
{
    return UINT32_MAX;
}

uint32_t GlIndexBuffer::maxIndex()
{
    return UINT16_MAX;
}

// ============================================================================
// GlVertexBuffer

GlVertexBuffer::GlVertexBuffer(
        BufferUsageHint::Enum _usage,
        uint32_t _numReserved,
        GLuint _gpuObject,
        const VertexFormat &_format):
    usage(_usage),
    numReserved(_numReserved),
    gpuObject(_gpuObject),
    format(_format)
{
    // empty
}

GlVertexBuffer::~GlVertexBuffer()
{
    if (gpuObject) {
        glDeleteBuffers(1, &gpuObject);
    }
}

GlVertexBuffer * GlVertexBuffer::create(
        const VertexFormat &format,
        BufferUsageHint::Enum usage,
        uint32_t numVertices,
        const void *vertices)
{
    GLuint gpuObject = 0;

    uint32_t size = numVertices * format.vertexSize();

    glGenBuffers(1, &gpuObject);
    glBindBuffer(GL_ARRAY_BUFFER, gpuObject);
    glBufferData(GL_ARRAY_BUFFER, size, vertices, usageEnum(usage));
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    CHECK_GL_ERROR(__func__);

    GlVertexBuffer *tmp = MIKO_NEW(refcountedAllocator, GlVertexBuffer) (
        usage, numVertices, gpuObject, format);

    if (!tmp) {
        glDeleteBuffers(1, &gpuObject);
    }
    else {
        // LOG_INFOF("vertex buffer %uB created", size);
    }

    return tmp;
}

bool GlVertexBuffer::resize(
        uint32_t numVertices,
        const void *vertices,
        bool orphan)
{
    GLuint newBuf = 0;
    if (orphan) {
        glGenBuffers(1, &newBuf);
    }
    else {
        newBuf = gpuObject;
    }

    glBindBuffer(GL_ARRAY_BUFFER, newBuf);
    glBufferData(GL_ARRAY_BUFFER, numVertices * format.vertexSize(), vertices, usageEnum(usage));

    // free the old buffer if orphaning
    if (orphan) {
        glDeleteBuffers(1, &gpuObject);
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    gpuObject = newBuf;

    return true;
}

bool GlVertexBuffer::updateRange(uint32_t startIndex, uint32_t count, const void *data)
{
    if (startIndex + count > numReserved) {
        mikoAssert(false && "attempting to modify outside buffer range");
        return false;
    }

    uint32_t offset = startIndex * format.vertexSize();
    uint32_t size = count * format.vertexSize();

    glBindBuffer(GL_ARRAY_BUFFER, gpuObject);
    glBufferSubData(GL_ARRAY_BUFFER, offset, size, data);

    return true;
}

void GlVertexBuffer::bind()
{
    mikoAssert(gpuObject != 0);
    glBindBuffer(GL_ARRAY_BUFFER, gpuObject);
}

BufferUsageHint::Enum GlVertexBuffer::getUsage() const
{
    return usage;
}

const VertexFormat & GlVertexBuffer::getVertexFormat() const
{
    return format;
}

uint32_t GlVertexBuffer::size() const
{
    return numReserved;
}

uint32_t GlVertexBuffer::maxSize()
{
    return UINT16_MAX;
}

} /* namespace miko */
