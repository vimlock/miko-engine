#ifndef MIKO_GRAPHICS_GL_MATERIAL_HPP
#define MIKO_GRAPHICS_GL_MATERIAL_HPP

#include "miko/graphics/material.hpp"
#include "miko/graphics/graphics.hpp"
#include "miko/graphics/gl/texture.hpp"
#include "miko/graphics/gl/program.hpp"

#include "miko/resource/resource_handle.hpp"
#include "miko/resource/resource_loader.hpp"


namespace miko {

class TextSerializer;
class TextDeserializer;

/// Class which describes how something should be renderered
class GlMaterial : public IMaterial
{
friend class MaterialFactory;

public:
    virtual bool loadAsyncPart(SDL_RWops *rwops);
    virtual bool loadSyncPart(SDL_RWops *rwops);

    bool serialize(TextSerializer &s);
    bool deserialize(TextDeserializer &s);

    bool depthWrite;
    bool depthTest;
    bool wireframe;
    bool twosided;
    bool alpha;

    BlendType blendMode;

    glm::vec4 ambient;
    glm::vec4 diffuse;
    glm::vec4 specular;
    glm::vec4 emissive;

    ResourceHandle<GlShaderProgram> shader;
    Vector<ResourceHandle<GlTexture> > textures;

private:
    /// Private constructor
    GlMaterial(Allocator &allocator);

};

class MaterialFactory : public IResourceFactory
{
public:
    virtual GlMaterial * create(Allocator &allocator);

};

typedef ResourceHandle<GlMaterial> MaterialHandle;
    
} /* namespace miko */

#endif /* MIKO_GRAPHICS_GL_MATERIAL_HPP */

