#ifndef MIKO_GRAPHICS_GL_BUFFER_HPP
#define MIKO_GRAPHICS_GL_BUFFER_HPP

#include "miko/graphics/buffer.hpp"
#include "miko/graphics/graphics.hpp"
#include "miko/graphics/gl/graphics.hpp"

namespace miko {


/// Wrapper around a GPU index buffer
class GlIndexBuffer : public IIndexBuffer
{
public:
    /// Type of the index data
    typedef uint16_t IndexType;

    virtual ~GlIndexBuffer();

    /// Create a new GlIndexBuffer
    /// @param usage      Hint to the GPU driver about the usage of this buffer
    /// @param numIndices Number of indices to allocate memory for
    /// @param indices    Data of the indices, if NULL buffer is left uninitialized
    /// @remarks          Returns NULL on failure
    static GlIndexBuffer * create(
        BufferUsageHint::Enum usage,
        uint32_t numIndices,
        const IndexType *indices
    );

    /// Attempts to resize the buffer to the new size
    /// @param numIndices Number of indices to allocate memory for.
    /// @param indices    Data of the indices, if NULL buffer is left uninitialized.
    /// @param orphan     Should the old buffer be deleted?
    /// @remarks Returns false on failure, the data is left intact.
    ///          The old data is invalidated.
    ///          Orphaning old buffer might give performance benefits because of
    ///          less need to wait for synchronization
    bool resize(
        uint32_t numIndices,
        const IndexType *indices,
        bool orphan=false
    );

    /// Updates part of the buffer
    /// @param startIndex First index to modify
    /// @param count      How many indices to modify
    /// @param data       Data of the indices
    /// @remarks          Data can't be NULL 
    bool updateRange(uint32_t startIndex, uint32_t count, const IndexType *data);

    void bind();

    /// Returns the usage hint given in the constructor
    BufferUsageHint::Enum getUsage() const;

    /// Returns how many indices we memory reserved for in the GPU
    uint32_t size() const;

    /// Return the maximum number of indices we can place on the buffer
    static uint32_t maxSize();

    /// Return the maximum value of a index on the buffer
    static uint32_t maxIndex();

private:
    /// Private constructor to be called throught create()
    GlIndexBuffer(BufferUsageHint::Enum usage,
        uint32_t numReserved,
        GLuint gpuObject
    );


    /// Usage hint given in the constructor
    BufferUsageHint::Enum usage;

    /// How many indices we have memory reserved on the GPU
    uint32_t numReserved;

    /// Handle to the buffer on the GPU side
    GLuint gpuObject;
};


/// Wrapper around a GPU vertex buffer
class GlVertexBuffer : public IVertexBuffer
{
public:
    virtual ~GlVertexBuffer();

    /// Allocate a new GlVertexBuffer
    /// @param format      VertexFormat to use for vertices in this buffer
    /// @param usage       Hint to the GPU driver about the usage of this buffer
    /// @param numVertices Number of vertices to allocate space for on the buffer
    /// @param vertices    Data of the vertices, if NULL buffer is left uninitialized
    /// @remarks           Returns NULL on error
    static GlVertexBuffer * create(
        const VertexFormat &format,
        BufferUsageHint::Enum usage,
        uint32_t numVertices,
        const void *vertices
    );

    /// Resizes the buffer to a new size
    /// @param numVertices Number of vertices to allocate memory for
    /// @param vertices    Data of the vertices, if NULL buffer is left uninitialized
    /// @param orphan      Should the old buffer be deleted?
    /// @remarks Return false on failure.
    ///          Old data is invalidated on success
    ///          Old data is left intact on failure
    ///          Orphaning old buffer might give performance benefits because of
    ///          less need to wait for synchronization
    bool resize(
        uint32_t numVertices,
        const void *vertices,
        bool orphan=false
    );

    /// Update part of the buffer
    bool updateRange(uint32_t startIndex, uint32_t count, const void *data);

    void bind();

    /// Returns the usage hint given in the constructor.
    BufferUsageHint::Enum getUsage() const;

    /// Returns the vertex format given in the constructor.
    const VertexFormat & getVertexFormat() const;

    /// Returns the number of vertices we have memory reserved for
    uint32_t size() const;

    /// Return maximum number of vertices that can be placed on the buffer
    static uint32_t maxSize();

private:
    /// Private constructor to be called through create()
    GlVertexBuffer(
        BufferUsageHint::Enum usage,
        uint32_t numReserved,
        GLuint gpuObject,
        const VertexFormat &format
    );


    /// Usage hint given in the constructor
    BufferUsageHint::Enum usage;

    /// How many vertices we have memory reserved for on the GPU
    uint32_t numReserved;

    /// Handle to the buffer on the GPU side
    GLuint gpuObject;

    /// Formate of the vertex data
    const VertexFormat &format;
};
    
} /* namespace miko */

#endif /* MIKO_GRAPHICS_GL_BUFFER_HPP */

