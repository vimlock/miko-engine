#include "miko/graphics/gl/program.hpp"

#include "miko/io/text_deserializer.hpp"
#include "miko/io/log.hpp"

#include "miko/resource/resource_manager.hpp"

namespace miko {

static GLuint compileShader(
        GLenum type,
        const char *sourceName,

        GLsizei sourcesCount,
        const GLchar **sourceStrings,
        const GLint  *sourceLengths)
{
    GLuint shader = glCreateShader(type);
    if (!shader) {
        LOG_ERROR("failed to compile shader");
        return 0;
    }

    glShaderSource(shader, sourcesCount, sourceStrings, sourceLengths);
    glCompileShader(shader);

    GLint isCompiled = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
    if (!isCompiled) {
        char buf[4096];
        GLint logLength = 0;

        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);

        if (logLength > (int)sizeof(buf)) {
            LOG_ERRORF("shader info log length too large %d  (%d max)",
                logLength, (int)sizeof(buf));
            logLength = sizeof(buf);
        }

        glGetShaderInfoLog(shader, logLength, &logLength, buf);
        LOG_ERRORF("failed to compile shader %s, details below", sourceName);
        LOG_ERROR(buf);

        #if 0
        LOG_ERRORF("full shader source below");

        for (int i = 0; i < sourcesCount; i++) {
            LOG_ERRORF("%.*s", sourceLengths[i], sourceStrings[i]);
        }
        #endif

        glDeleteShader(shader);
        shader = 0;
    }

    return shader;
}

GlShaderProgram::GlShaderProgram(Allocator &_allocator, GlShaderProgramFactory *_creator):
        gpuObject(0),
        creator(_creator),
        attribs(_allocator),
        uniforms(_allocator)
{
    // empty
}

GlShaderProgram::~GlShaderProgram()
{
    if (gpuObject) {
        glDeleteProgram(gpuObject);
    }
}

bool GlShaderProgram::loadAsyncPart(SDL_RWops *rwops)
{
    TextDeserializer s(getName().c_str(), rwops);

    if (!deserialize(s))
        return false;

    if (!s.expect(Token::END))
        return false;

    return true;
}

bool GlShaderProgram::loadSyncPart(SDL_RWops *rwops)
{
    return loadToGpu();
}

const GlProgramVariableInfo * GlShaderProgram::uniformInfo(const char *name) const
{
    HashMap<std::string, GlProgramVariableInfo>::ConstIterator i = uniforms.find(name);
    if (i != uniforms.end()) {
        return &i->val;
    }
    else {
        return NULL;
    }
}

const GlProgramVariableInfo * GlShaderProgram::attribInfo(const char *name) const
{
    HashMap<std::string, GlProgramVariableInfo>::ConstIterator i = attribs.find(name);
    if (i != attribs.end()) {
        return &i->val;
    }
    else {
        return NULL;
    }
}

GLint GlShaderProgram::uniformLocation(const char *name) const
{
    const GlProgramVariableInfo *i = uniformInfo(name);
    return i ? i->location : -1;
}

GLint GlShaderProgram::attribLocation(const char *name) const
{
    const GlProgramVariableInfo *i = attribInfo(name);
    return i ? i->location : -1;
}

void GlShaderProgram::bind()
{
    glUseProgram(gpuObject);
}

bool GlShaderProgram::deserialize(TextDeserializer &s)
{
    ResourceManager *resourceManager = getResourceManager();

    std::string vertexShaderName;
    std::string fragmentShaderName;

    if (!s.skipIdentifier("vertexShader"))
        return false;

    if (!s.read(vertexShaderName))
        return false;

    if (!s.skipIdentifier("fragmentShader"))
        return false;

    if (!s.read(fragmentShaderName))
        return false;

    vertexSource = resourceManager->getHandleNonBlocking<GlShaderSource>(vertexShaderName);
    fragmentSource = resourceManager->getHandleNonBlocking<GlShaderSource>(fragmentShaderName);

    vertexSource.waitLoaded();
    fragmentSource.waitLoaded();

    // Shader program loading fails along with the sources
    if (!vertexSource || !fragmentSource) {
        LOG_ERRORF("failed to load shader sources");
        return false;
    }

    return true;
}

bool GlShaderProgram::loadToGpu()
{
    GLuint vertexShader = 0;
    GLuint fragmentShader = 0;
    GLuint program = 0;
    GLint isLinked = 0;

    const std::string &name = getName();

    const GLchar *vertexSources[3] = {
        creator->commonHeader.c_str(),
        creator->vertexHeader.c_str(),
        vertexSource->getSource()
    };

    const GLint vertexLengths[3] = {
        (GLint)creator->commonHeader.length(),
        (GLint)creator->vertexHeader.length(),
        (GLint)vertexSource->getLength()
    };

    const GLchar *fragmentSources[3] = {
        creator->commonHeader.c_str(),
        creator->fragmentHeader.c_str(),
        fragmentSource->getSource()
    };

    const GLint fragmentLengths[3] = {
        (GLint)creator->commonHeader.length(),
        (GLint)creator->fragmentHeader.length(),
        (GLint)fragmentSource->getLength()
    };

    vertexShader = compileShader(
        GL_VERTEX_SHADER, name.c_str(), 3, vertexSources, vertexLengths);

    if (!vertexShader) {
        goto fail;
    }

    fragmentShader = compileShader(
        GL_FRAGMENT_SHADER, name.c_str(), 3, fragmentSources, fragmentLengths);

    if (!fragmentShader) {
        goto fail;
    }

    program = glCreateProgram();
    if (!program) {
        LOG_ERROR("Failed to create GL program");
        goto fail;
    }

    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);

    for (int i = 0; i < COMPONENT_NAME_COUNT; i++) {
        VertexComponentName e = static_cast<VertexComponentName>(i);
        const char *name = toStr(e);
        glBindAttribLocation(program, i, name);
    }

    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
    if (isLinked == GL_FALSE) {
        char buf[4096];
        GLint logLength = 0;

        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);

        if (logLength > (int)sizeof(buf)) {
            LOG_ERRORF("shader info log length too large %d  (%d max)",
                logLength, (int)sizeof(buf));
            logLength = sizeof(buf);
        }

        glGetProgramInfoLog(program, logLength, &logLength, buf);
        LOG_ERRORF("failed to compile shader %s, details below", name.c_str());
        LOG_ERROR(buf);

        goto fail;
    }

    glDetachShader(program, vertexShader);
    glDetachShader(program, fragmentShader);

    glDeleteShader(fragmentShader);
    glDeleteShader(vertexShader);

    gpuObject = program;

    if (!cacheUniforms()) {
        return false;
    }

    if (!cacheAttribs()) {
        return false;
    }

    return true;

    fail:
    if (program) {
        glDeleteProgram(program);
    }

    if (fragmentShader) {
        glDeleteShader(fragmentShader);
    }
    
    if (vertexShader) {
        glDeleteShader(vertexShader);
    }

    return false;
}

bool GlShaderProgram::cacheUniforms()
{
    GLint count = 0;
    GLint bufSize = 0;

    glGetProgramiv(gpuObject, GL_ACTIVE_UNIFORMS, &count);
    glGetProgramiv(gpuObject, GL_ACTIVE_UNIFORM_MAX_LENGTH, &bufSize);

    char namebuf[4096];
    mikoAssert(bufSize < (GLint)sizeof(namebuf));

    for (int i = 0; i < count; i++) {
        GLint length = 0; /* Length of the name */
        GLsizei size = 0; /* Size of the attribute, confusing naming */
        GLenum type;

        glGetActiveUniform(gpuObject, i, bufSize, &length, &size, &type, namebuf);
        GLint location = glGetUniformLocation(gpuObject, namebuf);
        mikoAssert(location >= 0);
        
        GlProgramVariableInfo uniform;
        uniform.location = location;
        uniform.size = size;
        uniform.type = type;

        if (!uniforms.set(std::string(namebuf, length), uniform)) {
            return false;
        }
    }

    return true;
}

bool GlShaderProgram::cacheAttribs()
{
    GLint count = 0;
    GLint bufSize = 0;

    glGetProgramiv(gpuObject, GL_ACTIVE_ATTRIBUTES, &count);
    glGetProgramiv(gpuObject, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &bufSize);

    char namebuf[4096];
    mikoAssert(bufSize < (GLint)sizeof(namebuf));

    for (int i = 0; i < count; i++) {
        GLint length = 0; /* Length of the name */
        GLsizei size = 0; /* Size of the attribute, confusing naming */
        GLenum type;

        glGetActiveAttrib(gpuObject, i, bufSize, &length, &size, &type, namebuf);
        GLint location = glGetAttribLocation(gpuObject, namebuf);
        
        GlProgramVariableInfo attrib;
        attrib.location = location;
        attrib.size = size;
        attrib.type = type;

        if (!attribs.set(std::string(namebuf, length), attrib)) {
            return false;
        }
    }

    return true;
}

GlShaderProgramFactory::GlShaderProgramFactory()
{
    commonHeader =
        "#version 330\n"
        "#define MIKO_SHADING_QUALITY 1\n"
        "const int MIKO_MAX_BONES = 64;\n"
        ;

    vertexHeader = 
        "#define MIKO_VERTEX_SHADER\n"
        "#define varying out\n";

    fragmentHeader =
        "#define MIKO_FRAGMENT_SHADER\n"
        "#define varying in\n" ;
}

GlShaderProgram * GlShaderProgramFactory::create(Allocator &allocator)
{
    return MIKO_NEW(allocator, GlShaderProgram(allocator, this));
}
    
} /* namespace miko */
