#include "miko/graphics/gl/texture.hpp"
#include "miko/resource/resource_loader.hpp"
#include "miko/io/log.hpp"
#include "miko/graphics/gl/graphics.hpp"

#include <stb/stb_image.h>

namespace miko {

static uint32_t nearestPowerOfTwo(uint32_t v)
{
    uint32_t i;

    for (i = 1; i < v; i = i << 1) {
        // empty
    }

    return i;
}

// ============================================================================
// StbIoWrapper

class StbIoWrapper
{
public:
    StbIoWrapper(SDL_RWops *_context)
    {
        callbacks.read = read;
        callbacks.skip = skip;
        callbacks.eof = eof;
        context = _context;
        endOffset = SDL_RWseek(context, 0, RW_SEEK_END);
        mikoAssert(endOffset >= 0);
        SDL_RWseek(context, 0, RW_SEEK_SET);
    }

    stbi_io_callbacks * getCallbacks()
    {
        return &callbacks;
    }

private:
    static int read(void *user, char *data, int size);
    static void skip(void *user, int n);
    static int eof(void *user);

    stbi_io_callbacks callbacks;
    SDL_RWops *context;
    Sint64 endOffset;
};

int StbIoWrapper::read(void *user, char *data, int size)
{
    StbIoWrapper *self = reinterpret_cast<StbIoWrapper*>(user);

    size_t numRead = SDL_RWread(self->context, data, 1, size);
    return numRead;
}

void StbIoWrapper::skip(void *user, int n)
{
    StbIoWrapper *self = reinterpret_cast<StbIoWrapper*>(user);

    SDL_RWseek(self->context, n, RW_SEEK_CUR);
}

int StbIoWrapper::eof(void *user)
{
    StbIoWrapper *self = reinterpret_cast<StbIoWrapper*>(user);

    return SDL_RWtell(self->context) == self->endOffset;
}

// ============================================================================
// GlTexture

GLenum textureFormatEnum(TextureFormatType format)
{
    switch (format) {
    case TEXTURE_FORMAT_R: return GL_RED;
    case TEXTURE_FORMAT_RG: return GL_RG;
    case TEXTURE_FORMAT_RGB: return GL_RGB;
    case TEXTURE_FORMAT_RGBA: return GL_RGBA;
    }

    mikoUnreachable();
    return GL_RED;
}

GlTexture::GlTexture():
    width(0),
    height(0),
    widthPadded(0),
    heightPadded(0),
    pixeldata(0),
    format(TEXTURE_FORMAT_R),
    gpuObject(0)
{
}

GlTexture::~GlTexture()
{
    unloadFromGpu();
    unloadFromMemory();
}

GlTexture * GlTexture::create(TextureFormatType format, uint32_t width, uint32_t height)
{
    GlTexture *tmp = MIKO_NEW(refcountedAllocator, GlTexture) ();
    if (!tmp) {
        return NULL;
    }

    if (!tmp->loadToGpuEx(format, format, false, width, height, NULL)) {
        tmp->acquire();
        tmp->release();

        return NULL;
    }

    return tmp;
}

bool GlTexture::loadAsyncPart(SDL_RWops *rwops)
{
    return unserialize(rwops);
}

bool GlTexture::loadSyncPart(SDL_RWops *rwops)
{
    return loadToGpu();
}

void GlTexture::clear()
{
    unloadFromGpu();
    unloadFromMemory();
}

bool GlTexture::isLoadedToGpu() const
{
    return gpuObject != 0;
}
    
bool GlTexture::loadToGpu()
{
    mikoAssert(isLoadedToMemory() && "Attempting to upload incomplete texture to gpu");

    return loadToGpuEx(format, format, true, width, height, pixeldata);
}

bool GlTexture::loadToGpuEx(
        TextureFormatType internalFormat,
        TextureFormatType dataFormat,
        bool padd,
        GLint dataWidth,
        GLint dataHeight,
        const GLvoid *data
)
{
    GLuint object = 0;
    glGenTextures(1, &object);
    glBindTexture(GL_TEXTURE_2D, object);

    width = dataWidth;
    height = dataHeight;

    widthPadded = dataWidth;
    heightPadded = dataHeight;

    if (padd) {
        widthPadded = nearestPowerOfTwo(dataWidth);
        heightPadded = nearestPowerOfTwo(dataHeight);

        if (dataWidth != (int)widthPadded || dataHeight != (int)heightPadded) {
            const char *name = getName().c_str();

            LOG_WARNF("texture %s does not have power of two dimensions", name);
            LOG_WARNF("texture %s size %ux%u", name, width, height);
            LOG_WARNF("texture %s size rounded to %ux%u", name, widthPadded, heightPadded);
        }
    }

    GLenum intrfmt = textureFormatEnum(internalFormat);
    GLenum datafmt = textureFormatEnum(dataFormat);

    // Use linear sampling as default, we will modify this with samplers later anyway
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // We upload the texture in 2 parts because padding is added to some textures
    // and glTexImage* requires full content of the texture to be given,
    // so we just pass NULL as data and patch it later.
    glTexImage2D(GL_TEXTURE_2D, 0, intrfmt,
        widthPadded, heightPadded, 0, datafmt, GL_UNSIGNED_BYTE, NULL
    );

    if (data) {
        glTexSubImage2D(GL_TEXTURE_2D, 0,
            0, 0, dataWidth, dataHeight,
            datafmt, GL_UNSIGNED_BYTE, data
        );
    }

    CHECK_GL_ERROR("textureLoadFunc()");

    // LOG_INFOF("texture %s loaded to gpu", getPath().c_str());

    gpuObject = object;

    return true;
}

void GlTexture::unloadFromGpu()
{
    if (!isLoadedToGpu()) {
        return;
    }
}

bool GlTexture::isLoadedToMemory() const
{
    return pixeldata != NULL;
}

void GlTexture::loadToMemory()
{
    mikoAssert(isLoadedToGpu());
}

void GlTexture::unloadFromMemory()
{
    if (!isLoadedToMemory()) {
        return;
    }

    stbi_image_free(pixeldata);
    pixeldata = NULL;
}

bool GlTexture::unserialize(SDL_RWops *rwops)
{
    int width = 0;
    int height = 0;
    int channels = 0;

    StbIoWrapper stbio(rwops);
    unsigned char *data = stbi_load_from_callbacks(
        stbio.getCallbacks(), &stbio, &width, &height, &channels, 0
    );

    if (data) {
        // modify the texture only if the load was successfull
        clear();

        this->pixeldata = data;
        this->width = width;
        this->height = height;

        switch (channels) {
        case 1: this->format = TEXTURE_FORMAT_R; break;
        case 2: this->format = TEXTURE_FORMAT_RG; break;
        case 3: this->format = TEXTURE_FORMAT_RGB; break;
        case 4: this->format = TEXTURE_FORMAT_RGBA; break;
        default:
            mikoUnreachable();
        }

        //LOG_INFOF("loaded texture: %.2fMB", 
            //(channels * width * height) / 1000.0f / 1000.0f
        //);
        //
        return true;
    }
    else {
        LOG_INFOF("failed to load texture: %s", stbi_failure_reason());
        return false;
    }
}

bool GlTexture::serializeJPEG(SDL_RWops *rwops) const
{
    mikoAssert(isLoadedToMemory());
    return false;
}

bool GlTexture::serializePNG(SDL_RWops *rwops) const
{
    mikoAssert(isLoadedToMemory());
    return false;
}

void GlTexture::bind()
{
    mikoAssert(gpuObject != 0);
    glBindTexture(GL_TEXTURE_2D, gpuObject);
}

GLuint GlTexture::getGpuObject()
{
    return gpuObject;
}

GlTexture * TextureFactory::create(Allocator &allocator)
{
    GlTexture *tex = MIKO_NEW(allocator, GlTexture) ();
    return tex;
}
    
} /* namespace miko */
