#ifndef MIKO_GRAPHICS_GL_FRAMEBUFFER_HPP
#define MIKO_GRAPHICS_GL_FRAMEBUFFER_HPP

#include "miko/core/object.hpp"
#include "miko/graphics/gl/graphics.hpp"
#include "miko/graphics/gl/texture.hpp"

namespace miko {

class IFramebuffer : public Object
{
public:
    enum AttachmentPoint {
        COLOR_ATTACHMENT_0,
        COLOR_ATTACHMENT_1,
        COLOR_ATTACHMENT_2,
        COLOR_ATTACHMENT_3,
        DEPTH_ATTACHMENT,
        STENCIL_ATTACHMENT,

        MAX_ATTACHMENTS
    };

    static const char * attachmentPointStr(AttachmentPoint index);
};

class GlRenderbuffer : public Object
{
public:
    virtual ~GlRenderbuffer();

    static GlRenderbuffer * create(GLenum format, int32_t width, int32_t height);

    int32_t getWidth() const;
    int32_t getHeight() const;

    GLuint getGpuObject();

private:
    /// Private constructor, use create() instead
    GlRenderbuffer(
        GLenum format,
        int32_t width,
        int32_t height,
        GLuint gpuObject
    );

    GLenum format;
    int32_t width;
    int32_t height;
    GLuint gpuObject;

};

class GlFramebufferAttachment
{
public:
    enum Type {
        RENDERBUFFER,
        TEXTURE
    };

    GlFramebufferAttachment();
    GlFramebufferAttachment(GlRenderbuffer *renderbuffer);
    GlFramebufferAttachment(GlTexture *texture);

    GlFramebufferAttachment& operator = (GlRenderbuffer *renderbuffer);
    GlFramebufferAttachment& operator = (GlTexture *texture);

    Object * getObject();
    const Object * getObject() const;
    GlRenderbuffer * getRenderbuffer();
    GlTexture * getTexture();

private:
    Type type;
    Ref<Object> ref;
};

class GlFramebuffer : public IFramebuffer
{
public:
    friend class GlRenderer;

    /// Creates a new framebuffer
    /// @param  name Name which can be used to identify this framebuffer for debugging purposes
    /// @return NULL on failure
    static GlFramebuffer * create(const std::string &name);

    virtual ~GlFramebuffer();

    /// @remarks Any old attachment is replaced
    void setAttachment(AttachmentPoint index, GlTexture *texture);

    /// @remarks Any old attachment is replaced
    void setAttachment(AttachmentPoint index, GlRenderbuffer *renderbuffer);

    /// Returns the texture bound in the index
    /// @remarks If a renderbuffer is bound to the attachment, NULL is returned
    GlTexture * getTextureAttachment(AttachmentPoint index);

    /// Removes any attachment from the index
    /// @remarks It is safe to call this function even if the attachement point is already removed
    void removeAttachment(AttachmentPoint index);

    /// Returns the renderbuffer bound in the index
    /// @remarks If a texture is bound to the attachment, NULL is returned
    GlRenderbuffer * getRenderbufferAttachment(AttachmentPoint index);

    /// Return true if the attachment has renderbuffer or texture bound to it
    bool hasAttachment(AttachmentPoint index) const;

    /// Returns the ID of this framebuffer on GPU
    GLuint getGpuObject();
    
private:
    /// Private constructor, use create() instead
    GlFramebuffer(const std::string &name, GLuint gpuObject);

    std::string name;

    /// Used by the renderer to ensure that the framebuffer is complete
    mutable bool statusDirty;

    /// Used by the renderer to ensure that the framebuffer is complete
    GLenum status;

    GLuint gpuObject;
    GlFramebufferAttachment attachments[MAX_ATTACHMENTS];
};
    
} /* namespace miko */

#endif /* MIKO_GRAPHICS_GL_FRAMEBUFFER_HPP */

