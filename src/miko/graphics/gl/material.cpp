#include "miko/graphics/gl/material.hpp"

#include "miko/io/text_serializer.hpp"
#include "miko/io/text_deserializer.hpp"

namespace miko {

static const EnumInfo blendModeEnum[] = {
    { "alpha",          BLEND_ALPHA          },
    { "additive",       BLEND_ADDITIVE       },
    { "interpolative",  BLEND_INTERPOLATIVE  },
    { "multiplicative", BLEND_MULTIPLICATIVE },
    { "replace",        BLEND_REPLACE        },
    { "overlay",        BLEND_OVERLAY        },
    { "screen",         BLEND_SCREEN         },
    { NULL, 0}
};

GlMaterial::GlMaterial(Allocator &allocator):
    textures(allocator)
{
    // empty
}

bool GlMaterial::loadAsyncPart(SDL_RWops *rwops)
{
    TextDeserializer s(getName().c_str(), rwops);
    return deserialize(s);
}

bool GlMaterial::loadSyncPart(SDL_RWops *rwops)
{
    return true;
}

bool GlMaterial::serialize(TextSerializer &s)
{
    return false;
}

bool GlMaterial::deserialize(TextDeserializer &s)
{
    if (!s.skipIdentifier("depthWrite"))
        return false;

    if (!s.read(depthWrite))
        return false;


    if (!s.skipIdentifier("depthTest"))
        return false;

    if (!s.read(depthTest))
        return false;


    if (!s.skipIdentifier("wireframe"))
        return false;

    if (!s.read(wireframe))
        return false;


    if (!s.skipIdentifier("twosided"))
        return false;

    if (!s.read(twosided)) {
        return false;
    }


    if (!s.skipIdentifier("alpha"))
        return false;

    if (!s.read(alpha)) {
        return false;
    }


    if (!s.skipIdentifier("blendMode"))
        return false;

    int tmp;
    if (!s.readEnum(blendModeEnum, tmp))
        return false;

    blendMode = static_cast<BlendType>(tmp);


    if (!s.skipIdentifier("ambient"))
        return false;

    if (!s.read(ambient))
        return false;


    if (!s.skipIdentifier("diffuse"))
        return false;

    if (!s.read(diffuse))
        return false;


    if (!s.skipIdentifier("specular"))
        return false;

    if (!s.read(specular))
        return false;


    if (!s.skipIdentifier("emissive"))
        return false;

    if (!s.read(emissive))
        return false;

    if (!s.skipIdentifier("shader"))
        return false;

    std::string shaderName;
    if (!s.read(shaderName))
        return false;

    if (!s.skipIdentifier("textures"))
        return false;

    if (!s.skip(Token::LCURLY))
        return false;

    while (s.accept(Token::STRING)) {
        std::string name;
        std::string source;

        if (!s.read(name))
            return false;

        if (!s.read(source)) 
            return false;
    }

    if (!s.skip(Token::RCURLY))
        return false;

    if (!s.expect(Token::END))
        return false;

    return true;
}

GlMaterial * MaterialFactory::create(Allocator &allocator)
{
    return MIKO_NEW(allocator, GlMaterial) (allocator);
}

} /* namespace miko */
