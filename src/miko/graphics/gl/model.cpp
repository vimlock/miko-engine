#include "miko/graphics/gl/model.hpp"
#include "miko/graphics/skeleton.hpp"

#include "miko/io/log.hpp"

#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace miko {

void GlModel::setMesh(GlMesh *_mesh)
{
    mesh = _mesh;
}

void GlAnimatedModel::setMesh(GlMesh *_mesh)
{
    mesh = _mesh;
    time = 0.0f;
    updateBones();
}

void GlAnimatedModel::setAnimation(SkeletonAnimation *_anim)
{
    time = 0.0f;

    anim = _anim;
    if (!_anim) {
        LOG_DEBUG("animation set to NULL");
        return;
    }

    if (!mesh) {
        LOG_DEBUG("no mesh to draw");
        return;
    }

    Skeleton *skeleton = mesh->getSkeleton();
    if (!skeleton) {
        LOG_DEBUG("mesh does not have skeleton");
        return;
    }

    for (unsigned int i = 0; i < skeleton->numBones(); i++) {
        Bone *bone = skeleton->getBone(i);
        int channel = anim->getChannelIndex(bone->name);
        if (channel < 0) {
            LOG_WARNF("bone %s does not have an animation channel", bone->name.c_str());
        }

        if (!boneChannels.append(channel)) {
            // @todo handle error
        }
    }

    LOG_DEBUG("set animation");
}


GlAnimatedModel::GlAnimatedModel():
    boneChannels(defaultAllocator),
    poseMatrices(defaultAllocator),
    skinningMatrices(defaultAllocator),
    anim(),
    time(0)
{
    // empty
}

void GlAnimatedModel::update(float timePassed)
{
    if (!anim || !mesh) {
        LOG_DEBUG("no anim");
        return;
    }

    time = fmodf(time + timePassed, anim->duration);

    if (!skeleton) {
        return;
    }

    glm::mat4 globalInverse(
        glm::vec4(1.0, 0.0, 0.0, 0.0),
        glm::vec4(0.0, 0.0, 1.0, 0.0),
        glm::vec4(0.0, -1.0, 0.0, 0.0),
        glm::vec4(0.0, 0.0, 0.0, 1.0)
    );

    globalInverse = glm::inverse(globalInverse);

    for (int index = 0; index < (int) skeleton->numBones(); index++) {
        Bone *bone = skeleton->getBone(index);
        int channelIndex = boneChannels[index];

        glm::mat4 localTransform;

        SkeletonAnimationChannel *channel = anim->getChannel(channelIndex);
        if (channel) {
            unsigned int prevIndex = channel->getFrameByTime(time);

            SkeletonAnimationFrame *prev = channel->getFrame(prevIndex);
            SkeletonAnimationFrame *next = channel->getFrame(prevIndex + 1);

            float deltaTime = next->time - prev->time;
            float factor = (time - prev->time) / deltaTime;

            glm::vec3 translation = glm::mix(prev->position, next->position, factor);
            glm::quat rotation = glm::normalize(glm::slerp(prev->rotation, next->rotation, factor));
            glm::vec3 scaling = glm::vec3(1.0, 1.0, 1.0);

            localTransform =
                glm::translate(glm::mat4(1.0), translation) *
                glm::mat4_cast(rotation) *
                glm::scale(glm::mat4(1.0f), scaling);
        }
        else {
            //localTransform = glm::translate(glm::mat4(1.0), bone->position) *
                //glm::mat4_cast(bone->rotation);
            localTransform = glm::mat4(1.0f);
        }

        if (bone->parent >= 0) {
            poseMatrices[index] = poseMatrices[bone->parent] * localTransform;
        }
        else {
            poseMatrices[index] = localTransform;
        }

        skinningMatrices[index] = poseMatrices[index] * bone->offsetMatrix;
    }

}

void GlAnimatedModel::updateBones()
{
    if (!mesh) {
        return;
    }

    skeleton = mesh->getSkeleton();
    if (!skeleton) {
        return;
    }

    skinningMatrices.clear();
    poseMatrices.clear();

    for (unsigned i = 0; i < skeleton->numBones(); i++) {
        Bone *bone = skeleton->getBone(i);
        // @todo handle allocation failure
        if (!skinningMatrices.append(glm::mat4(1.0f))) {
        }
        if (!poseMatrices.append(bone->offsetMatrix)) {
        }
    }
}

    
} /* namespace miko */
