#ifndef MIKO_GRAPHICS_GL_MODEL_HPP
#define MIKO_GRAPHICS_GL_MODEL_HPP

#include "miko/graphics/animation.hpp"
#include "miko/graphics/gl/mesh.hpp"

namespace miko {

class GlRenderer;

class GlModel
{
public:
    friend class GlRenderer;

    virtual void setMesh(GlMesh *mesh);

protected:
    Ref<GlMesh> mesh;

private:
};

class GlAnimatedModel : public GlModel
{
public:
    friend class GlRenderer;

    GlAnimatedModel();

    virtual void setMesh(GlMesh *mesh);
    void setAnimation(SkeletonAnimation *anim);

    void update(float timePassed);

    Skeleton * getSkeleton()
    {
        return skeleton;
    }

private:
    void updateBones();
    void updateBoneHiararchy(int index, const glm::mat4 &parentTransform);

    /// Fast lookup table to the animation
    Vector<int> boneChannels;

    /// Current pose of the skeleton
    Vector<glm::mat4> poseMatrices;

    /// Current inverse of the pose matrices
    Vector<glm::mat4> skinningMatrices;

    /// Currently playing animation
    Ref<SkeletonAnimation> anim;

    /// Skelton we're using, might not be the same as the one in mesh
    Ref<Skeleton> skeleton;

    float time;
};
    
} /* namespace miko */

#endif /* MIKO_GRAPHICS_GL_MODEL_HPP */

