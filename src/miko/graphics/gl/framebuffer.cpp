#include "miko/graphics/gl/framebuffer.hpp"

#include "miko/memory/memory.hpp"

namespace miko {

const char * IFramebuffer::attachmentPointStr(AttachmentPoint index)
{
    switch (index) {
    case COLOR_ATTACHMENT_0: return "color attachment 0";
    case COLOR_ATTACHMENT_1: return "color attachment 1";
    case COLOR_ATTACHMENT_2: return "color attachment 2";
    case COLOR_ATTACHMENT_3: return "color attachment 3";
    case DEPTH_ATTACHMENT:   return "depth attachment";
    case STENCIL_ATTACHMENT: return "stencil attachment";
    case MAX_ATTACHMENTS: break;
    }

    mikoUnreachable();
    return "error";
}

// ============================================================================
// GlRenderbuffer


GlRenderbuffer::GlRenderbuffer(
        GLenum _format,
        int32_t _width,
        int32_t _height,
        GLuint _gpuObject):
    format(_format),
    width(_width),
    height(_height),
    gpuObject(_gpuObject)
{
    // empty
}

GlRenderbuffer::~GlRenderbuffer()
{
    if (gpuObject) {
        glDeleteRenderbuffers(1, &gpuObject);
        gpuObject = 0;
    }
}

GlRenderbuffer * GlRenderbuffer::create(GLenum format, int32_t width, int32_t height)
{
    GLuint gpuObject;
    glGenRenderbuffers(1, &gpuObject);
    glBindRenderbuffer(GL_RENDERBUFFER, gpuObject);
    glRenderbufferStorage(GL_RENDERBUFFER, format, width, height);

    GlRenderbuffer *tmp = MIKO_NEW(refcountedAllocator, GlRenderbuffer) (
        format, width, height, gpuObject
    );

    if (!tmp) {
        glDeleteRenderbuffers(1, &gpuObject);
        return NULL;
    }

    return tmp;
}

int32_t GlRenderbuffer::getWidth() const
{
    return width;
}

int32_t GlRenderbuffer::getHeight() const
{
    return height;
}

GLuint GlRenderbuffer::getGpuObject()
{
    return gpuObject;
}


// ============================================================================
// GlFramebufferAttachment

GlFramebufferAttachment::GlFramebufferAttachment():
    type(RENDERBUFFER)
{
    // empty
}

GlFramebufferAttachment::GlFramebufferAttachment(GlRenderbuffer *renderbuffer):
    type(RENDERBUFFER),
    ref(renderbuffer)
{
    // empty
}

GlFramebufferAttachment::GlFramebufferAttachment(GlTexture *texture):
    type(TEXTURE),
    ref(texture)
{
    // empty
}

GlFramebufferAttachment& GlFramebufferAttachment::operator = (GlRenderbuffer *renderbuffer)
{
    type = RENDERBUFFER;
    ref = renderbuffer;
    return *this;
}

GlFramebufferAttachment& GlFramebufferAttachment::operator = (GlTexture *texture)
{
    type = TEXTURE;
    ref = texture;
    return *this;
}

Object * GlFramebufferAttachment::getObject()
{
    return ref;
}

const Object * GlFramebufferAttachment::getObject() const
{
    return ref;
}

GlRenderbuffer * GlFramebufferAttachment::getRenderbuffer()
{
    if (type == RENDERBUFFER) {
        return static_cast<GlRenderbuffer *>(static_cast<Object *>(ref));
    }
    else {
        return NULL;
    }
}

GlTexture * GlFramebufferAttachment::getTexture()
{
    if (type == TEXTURE) {
        return static_cast<GlTexture *>(static_cast<Object *>(ref));
    }
    else {
        return NULL;
    }
}

// ============================================================================
// GlFramebuffer

static GLenum attachmentEnum(GlFramebuffer::AttachmentPoint index)
{
    switch (index) {
    case GlFramebuffer::COLOR_ATTACHMENT_0: return GL_COLOR_ATTACHMENT0 + 0;
    case GlFramebuffer::COLOR_ATTACHMENT_1: return GL_COLOR_ATTACHMENT0 + 1;
    case GlFramebuffer::COLOR_ATTACHMENT_2: return GL_COLOR_ATTACHMENT0 + 2;
    case GlFramebuffer::COLOR_ATTACHMENT_3: return GL_COLOR_ATTACHMENT0 + 3;
    case GlFramebuffer::DEPTH_ATTACHMENT:   return GL_DEPTH_ATTACHMENT;
    case GlFramebuffer::STENCIL_ATTACHMENT: return GL_STENCIL_ATTACHMENT;
    case GlFramebuffer::MAX_ATTACHMENTS:    break;
    }

    mikoUnreachable();
    return GL_COLOR_ATTACHMENT0;
}

GlFramebuffer::GlFramebuffer(const std::string &_name, GLuint _gpuObject):
    name(_name),
    statusDirty(true),
    status(GL_INVALID_VALUE), // lets use some dummy value
    gpuObject(_gpuObject),
    attachments()
{
    // empty
}

GlFramebuffer::~GlFramebuffer()
{
    // don't bother detaching the attachments
    if (gpuObject) {
        glDeleteFramebuffers(1, &gpuObject);
    }
}
    
GlFramebuffer * GlFramebuffer::create(const std::string &name)
{
    GLuint gpuObject;
    glGenFramebuffers(1, &gpuObject);
    glBindFramebuffer(GL_FRAMEBUFFER, gpuObject);

    GlFramebuffer *tmp = MIKO_NEW(refcountedAllocator, GlFramebuffer) (name, gpuObject);
    if (!tmp) {
        glDeleteFramebuffers(1, &gpuObject);
        return NULL;
    }

    return tmp;
}

void GlFramebuffer::setAttachment(AttachmentPoint index, GlTexture *texture)
{
    if (!texture) {
        removeAttachment(index);
        return;
    }

    GLenum ap = attachmentEnum(index);
    GLuint textureObject = texture->getGpuObject();
    GLint mipmapLevel = 0;

    glBindFramebuffer(GL_FRAMEBUFFER, gpuObject);
    glFramebufferTexture2D(
        GL_FRAMEBUFFER, ap, GL_TEXTURE_2D, textureObject, mipmapLevel
    );

    attachments[index] = texture;
}

void GlFramebuffer::setAttachment(AttachmentPoint index, GlRenderbuffer *renderbuffer)
{
    if (!renderbuffer) {
        removeAttachment(index);
        return;
    }

    GLenum ap = attachmentEnum(index);
    GLuint rb = renderbuffer->getGpuObject();

    glBindFramebuffer(GL_FRAMEBUFFER, gpuObject);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, ap, GL_RENDERBUFFER, rb);
}

GlTexture * GlFramebuffer::getTextureAttachment(AttachmentPoint index)
{
    return attachments[index].getTexture();
}

GlRenderbuffer * GlFramebuffer::getRenderbufferAttachment(AttachmentPoint index)
{
    return attachments[index].getRenderbuffer();
}

void GlFramebuffer::removeAttachment(AttachmentPoint index)
{
    GLenum ap = attachmentEnum(index);

    glBindFramebuffer(GL_FRAMEBUFFER, gpuObject);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, ap, GL_RENDERBUFFER, 0);
}

bool GlFramebuffer::hasAttachment(AttachmentPoint index) const
{
    return attachments[index].getObject() != NULL;
}

GLuint GlFramebuffer::getGpuObject()
{
    return gpuObject;
}

} /* namespace miko */
