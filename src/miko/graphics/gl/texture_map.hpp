#ifndef MIKO_GRAPHICS_GL_TEXTURE_MAP_HPP
#define MIKO_GRAPHICS_GL_TEXTURE_MAP_HPP

#include "miko/container/hash_map.hpp"
#include "miko/graphics/gl/texture.hpp"
#include <string>

namespace miko {

class GlTextureMap : public Object
{
public:
    static GlTextureMap * create(Allocator &allocator);

    bool add(const std::string &name, GlTexture *texture);

private:
    GlTextureMap (Allocator &allocator);

    HashMap<std::string, Ref<GlTexture> > textures;

};
    
} /* namespace miko */

#endif /* MIKO_GRAPHICS_GL_TEXTURE_MAP_HPP */

