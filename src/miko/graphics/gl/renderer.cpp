#include "miko/graphics/gl/renderer.hpp"

#include "miko/device/window.hpp"
#include "miko/io/log.hpp"

#include "miko/graphics/gl/mesh.hpp"
#include "miko/graphics/gl/model.hpp"
#include "miko/graphics/sprite.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_inverse.hpp>

namespace miko {

static VertexFormat quadFormat("p2t2");
static VertexFormat debugVertexFormat("p3c4");
static VertexFormat spriteFormat("p3t2a1");

struct Vertex {
    Vertex(float x, float y, float u, float v):
        position(x, y),
        texcoord(u, v)
    {
        // empty
    }

    glm::vec2 position;
    glm::vec2 texcoord;
};

static Vertex vertices[] = {
    Vertex(-1.0f,  1.0f,  1.0f, 0.0f),
    Vertex(-1.0f, -1.0f,  1.0f, 1.0f),
    Vertex( 1.0f, -1.0f,  0.0f, 1.0f),

    Vertex( 1.0f, -1.0f,  0.0f, 1.0f),
    Vertex( 1.0f,  1.0f,  0.0f, 0.0f),
    Vertex(-1.0f,  1.0f,  1.0f, 0.0f)
};

struct RenderBucketKey
{
    GlMaterial *material;
    GlSubMesh *mesh;
};

struct RenderData
{
    glm::vec3 position;
    glm::vec3 rotation;
    glm::vec3 scale;
};

GlRenderer * GlRenderer::create(Allocator &allocator, IWindow *owner)
{
    GlRenderer *renderer = MIKO_NEW(allocator, GlRenderer) (owner);
    return renderer;
}

GlRenderer::GlRenderer(IWindow *_owner):
        debugLines(defaultAllocator),
        sprites(defaultAllocator)
{
    mikoAssert(_owner != NULL);
    owner = _owner;

    mikoAssert(sizeof(DebugVertex) == debugVertexFormat.vertexSize());
    debugVertices = GlVertexBuffer::create(debugVertexFormat, BufferUsageHint::STREAM_DRAW, 1024, NULL);

    mikoAssert(sizeof(Vertex) == quadFormat.vertexSize());
    quadVertices = GlVertexBuffer::create(quadFormat, BufferUsageHint::STATIC_DRAW, 6, vertices);

    mikoAssert(sizeof(SpriteVertex) == spriteFormat.vertexSize());
    spriteVertices = GlVertexBuffer::create(spriteFormat, BufferUsageHint::STREAM_DRAW, 1024 * 4, NULL);

    GLushort indices[1024 * 6];
    for (unsigned int i = 0; i < 1024 * 6; i += 6) {
        indices[i + 0] = i + 0;
        indices[i + 1] = i + 1;
        indices[i + 2] = i + 2;
        indices[i + 3] = i + 0;
        indices[i + 3] = i + 2;
        indices[i + 3] = i + 3;
    }

    spriteIndices = GlIndexBuffer::create(BufferUsageHint::STATIC_DRAW, 1024 * 6, indices);

    vertexArray = 0;
    glGenVertexArrays(1, &vertexArray);
    mikoAssert(vertexArray != 0);
    glBindVertexArray(vertexArray);

    CHECK_GL_ERROR(__func__);

    uniforms.ambientColor = glm::vec3(1.0f);

    uniforms.dirty = 0;
    uniforms.time = 0.0f;

    glEnable(GL_DEPTH_TEST);
    depthTestMode = true;

    glDepthMask(GL_TRUE);
    depthWriteMode = true;

    glEnable(GL_CULL_FACE);
    twosidedMode = false;

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    wireframeMode = false;

    blendMode = BLEND_ADDITIVE;
    setBlend(BLEND_INTERPOLATIVE);
}

GlRenderer::~GlRenderer()
{
    glDeleteVertexArrays(1, &vertexArray);
}

void GlRenderer::update(float timePassed)
{
    uniforms.time += timePassed;
    uniforms.timeDirty = true;

    debugLines.clear();
}

void GlRenderer::drawQuad()
{
    bindVertexBuffer(quadVertices);
    drawArrays(PRIMITIVE_TRIANGLES, 0, quadVertices->size());
}

void GlRenderer::drawDebugShapes()
{
    if (debugLines.empty()) {
        return;
    }
    
    setDepthTest(false);
    setTwosided(true);
    setWireframe(false);
    setBlend(BLEND_ALPHA);

    debugVertices->updateRange(0, debugLines.size() * 2, &debugLines[0]);
    bindVertexBuffer(debugVertices);
    drawArrays(PRIMITIVE_LINES, 0, debugLines.size() * 2);
}

void GlRenderer::drawBatches()
{
    spriteVertices->updateRange(0, sprites.size() * 4, &sprites[0]);
    bindVertexBuffer(spriteVertices);
    bindIndexBuffer(spriteIndices);
    drawElements(PRIMITIVE_TRIANGLES, 0, sprites.size() * 6);
    sprites.clear();
}

void GlRenderer::drawModel(const glm::mat4 &transform, GlModel &model)
{
    drawMesh(transform, model.mesh);
}

void GlRenderer::drawAnimatedModel(const glm::mat4 &transform, GlAnimatedModel &model)
{
    if (!model.skinningMatrices.empty()) {
        uniformMatrix4fv("cBones[0]", model.skinningMatrices.size(), &model.skinningMatrices[0]);
    }

    drawMesh(transform, model.mesh);
    if (!model.skeleton) {
        LOG_DEBUG("no skeleton");
        return;
    }

    drawDebugSkeleton(transform, model);
}

void GlRenderer::drawDebugSkeleton(
        const glm::mat4 &transform,
        GlAnimatedModel &model)
{
    glm::mat4 mvp = uniforms.projection * uniforms.view * transform;

    glm::vec4 origin(0.0f, 0.0f, 0.0f, 1.0f);
    glm::vec4 x(5.0f, 0.0f, 0.0f, 1.0f);
    glm::vec4 y(0.0f, 5.0f, 0.0f, 1.0f);
    glm::vec4 z(0.0f, 0.0f, 5.0f, 1.0f);

    origin = mvp * origin;
    x = mvp * x;
    y = mvp * y;
    z = mvp * z;

    glm::vec4 red(1.0f, 0.0f, 0.0f, 1.0f);
    glm::vec4 green(0.0f, 1.0f, 0.0f, 1.0f);
    glm::vec4 blue(0.0f, 0.0f, 1.0f, 1.0f);

    glm::vec3 ow = glm::vec3(origin) / origin.w;
    glm::vec3 xw = glm::vec3(x) / x.w;
    glm::vec3 yw = glm::vec3(y) / y.w;
    glm::vec3 zw = glm::vec3(z) / z.w;

    drawDebugLine(red, ow, xw);
    drawDebugLine(green, ow, yw);
    drawDebugLine(blue, ow, zw);

    Skeleton *skeleton = model.skeleton;

    for (int i = 0; i < (int)skeleton->numBones(); i++) {
        const Bone *bone = skeleton->getBone(i);
        if (!bone) {
            continue;
        }

        if (bone->parent < 0) {
            continue;
        }

        glm::vec4 s = mvp * model.poseMatrices[bone->parent] * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
        glm::vec4 e = mvp * model.poseMatrices[i] * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);

        glm::vec3 start = glm::vec3(s) / s.w;
        glm::vec3 end = glm::vec3(e) / e.w;

        glm::vec3 color(0.0, 1.0, 1.0);

        drawDebugLine(start, end, glm::vec4(color, 1.0), glm::vec4(color, 0.0f));

        s = mvp * model.poseMatrices[i] * glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
        start = glm::vec3(s) / s.w;
        color = glm::vec3(0.0, 0.0, 1.0);
        drawDebugLine(end, start, glm::vec4(color, 1.0), glm::vec4(color, 0.0f));

        s = mvp * model.poseMatrices[i] * glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
        start = glm::vec3(s) / s.w;
        color = glm::vec3(0.0, 1.0, 0.0);
        drawDebugLine(end, start, glm::vec4(color, 1.0), glm::vec4(color, 0.0f));

        s = mvp * model.poseMatrices[i] * glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
        start = glm::vec3(s) / s.w;
        color = glm::vec3(1.0, 0.0, 0.0);
        drawDebugLine(end, start, glm::vec4(color, 1.0), glm::vec4(color, 0.0f));
    }
}

void GlRenderer::drawMesh(const glm::mat4 &transform, GlMesh *mesh)
{
    if (!mesh) {
        return;
    }

    for (unsigned int i = 0; i < mesh->numSubMeshes(); i++) {
        GlSubMesh *submesh = mesh->getSubMesh(i);
        if (submesh) {
            drawSubMesh(transform, submesh);
        }
    }
}

void GlRenderer::drawSubMesh(const glm::mat4 &transform, GlSubMesh *mesh)
{
    mikoAssert(mesh != NULL);

    GlIndexBuffer *indices = mesh->getIndices();
    GlVertexBuffer *vertices = mesh->getVertices();

    bindIndexBuffer(indices);
    bindVertexBuffer(vertices);

    // uniforms.modelViewProjection = transform * uniforms.view * uniforms.projection;
    uniforms.modelViewProjection = uniforms.projection * uniforms.view * transform;
    uniforms.modelViewProjectionDirty = true;

    uniforms.normalMatrix = glm::inverseTranspose(glm::mat3(uniforms.modelViewProjection));
    uniforms.normalMatrixDirty = true;

    for (unsigned int i = 0; i < mesh->numGeometries(); i++) {
        GlGeometry *geometry = mesh->getGeometry(i);
        if (geometry) {
            setMaterial(geometry->material);
            updateUniforms();
            drawElements(geometry->primitive, geometry->indexOffset, geometry->indexCount);
        }
    }
}

void GlRenderer::drawSprite(const glm::vec3 &position, const Sprite &sprite)
{
    if (!sprites.append(SpriteData())) {
        LOG_DEBUG("out of memory for sprites");
        return;
    }

    SpriteData &data = sprites.back();
    const SpriteFrame &f = sprite.frame;

    float scale = sprite.scale;
    float z = position.z;

    float cos = cosf(sprite.angle);
    float sin = sinf(sprite.angle);

    float w = sprite.frame.width;
    float h = sprite.frame.height;
    //float ox = sprite.frame.origin.x;
    //float oy = sprite.frame.origin.y;
    
    float tx = sprite.frame.x;
    float ty = sprite.frame.y;
    float tw = tx + w;
    float th = ty + h;

    float p0x = -w / 2.0f;
    float p0y = -h / 2.0f;

    float p1x =  w / 2.0f;
    float p1y = -h / 2.0f;

    float p2x =  w / 2.0f;
    float p2y =  h / 2.0f;
    
    float p3x = -w / 2.0f;
    float p3y =  h / 2.0f;

    float x0 = position.x + (cos * p0x - sin * p0y);
    float y0 = position.y + (sin * p0x + cos * p0y);

    float x1 = position.x + (cos * p1x - sin * p1y);
    float y1 = position.y + (sin * p1x + cos * p1y);

    float x2 = position.x + (cos * p2x - sin * p2y);
    float y2 = position.y + (sin * p2x + cos * p2y);

    float x3 = position.x + (cos * p3x - sin * p3y);
    float y3 = position.y + (sin * p3x + cos * p3y);

    data.vertices[0].position = glm::vec3(x0, y0, z);
    data.vertices[1].position = glm::vec3(x1, y1, z);
    data.vertices[2].position = glm::vec3(x2, y2, z);
    data.vertices[3].position = glm::vec3(x3, y3, z);

    data.vertices[0].texcoords = glm::vec2(tx, ty);
    data.vertices[1].texcoords = glm::vec2(tw, ty);
    data.vertices[2].texcoords = glm::vec2(tw, th);
    data.vertices[3].texcoords = glm::vec2(tx, th);

    data.vertices[0].alpha = sprite.alpha;
    data.vertices[1].alpha = sprite.alpha;
    data.vertices[2].alpha = sprite.alpha;
    data.vertices[3].alpha = sprite.alpha;

    #if 0
    for (unsigned int i = 0; i < 4; i++)  {
        LOG_DEBUGF("vertex %u at %f %f", i,
            data.vertices[i].position.x,
            data.vertices[i].position.y
        );
    }
    #endif
}

void GlRenderer::setCamera(const Camera &camera)
{
    uniforms.eyePosition = camera.getPosition();
    uniforms.eyeDirection = camera.getDirection();
    uniforms.view = camera.getViewMatrix();
    uniforms.projection = camera.getProjectionMatrix(owner->getDimensions());

    uniforms.eyePositionDirty = true;
    uniforms.eyeDirectionDirty = true;
    uniforms.viewDirty = true;
    uniforms.projectionDirty = true;
}

void GlRenderer::setTexture(unsigned int index, GlTexture *_texture)
{
    if (textures[index] == _texture) {
        return;
    }

    textures[index] = _texture;

    glActiveTexture(GL_TEXTURE0);

    if (_texture == NULL) {
        glBindTexture(GL_TEXTURE_2D, 0);
        return;
    }

    _texture->bind();
}

void GlRenderer::setMaterial(GlMaterial *_material)
{
    if (material == _material) {
        return;
    }

    material = _material;
    if (!_material) {
        // setShader(NULL);
        return;
    }

    // setShader(material->shader);
    uniforms.diffuse = material->diffuse;
    uniforms.specular = material->specular;
    uniforms.emissive = material->emissive;
    uniforms.ambient = material->ambient;

    uniforms.diffuseDirty = true;
    uniforms.specularDirty = true;
    uniforms.emissiveDirty = true;
    uniforms.ambientDirty = true;

    setDepthWrite(material->depthWrite);
    setDepthTest(material->depthTest);
    setTwosided(material->twosided);
    setWireframe(material->wireframe);
    setBlend(material->blendMode);
}

void GlRenderer::setProgram(GlShaderProgram *_program)
{
    if (program == _program) {
        return;
    }

    program = _program;
    if (program) {
        program->bind();
        invalidateUniforms();
    }
    else {
        LOG_WARN("no shader given");
        glUseProgram(0);
    }

    CHECK_GL_ERROR(__func__);
}

void GlRenderer::setFramebuffer(GlFramebuffer *_framebuffer)
{
    if (framebuffer == _framebuffer) {
        return;
    }

    framebuffer = _framebuffer;
    if (!_framebuffer) {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        return;
    }

    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer->getGpuObject());
    if (framebuffer->statusDirty) {
        framebuffer->status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        framebuffer->statusDirty = false;
    }

    if (framebuffer->status != GL_FRAMEBUFFER_COMPLETE) {
        LOG_ERRORF("framebuffer '%s' is incomplete", framebuffer->name.c_str());
    }
}

void GlRenderer::setDepthTest(bool enabled)
{
    if (depthTestMode == enabled) {
        return;
    }

    if (enabled) {
        glEnable(GL_DEPTH_TEST);
    }
    else {
        glDisable(GL_DEPTH_TEST);
    }

    depthTestMode = enabled;
    CHECK_GL_ERROR(__func__);
}

void GlRenderer::setDepthWrite(bool enabled)
{
    if (depthWriteMode == enabled) {
        return;
    }

    if (enabled) {
        glDepthMask(GL_TRUE);
    }
    else {
        glDepthMask(GL_FALSE);
    }

    depthWriteMode = enabled;
    CHECK_GL_ERROR(__func__);
}

void GlRenderer::setTwosided(bool enabled)
{
    // nothing to do?
    if (twosidedMode == enabled) {
        return;
    }

    if (enabled) {
        glDisable(GL_CULL_FACE);
    }
    else {
        glEnable(GL_CULL_FACE);
    }

    twosidedMode = enabled;
    CHECK_GL_ERROR(__func__);
}

void GlRenderer::setWireframe(bool enabled)
{
    // nothing to do?
    if (wireframeMode == enabled) {
        return;
    }

    if (enabled) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }
    else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    wireframeMode = enabled;
    CHECK_GL_ERROR(__func__);
}

void GlRenderer::setBlend(BlendType mode)
{
    // nothing to do?
    if (blendMode == mode) {
        return;
    }

    blendMode = mode;

    switch (mode) {
    case BLEND_ALPHA:
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glBlendEquation(GL_FUNC_ADD);
        break;

    case BLEND_ADDITIVE:
        glBlendFunc(GL_ONE, GL_ONE);
        glBlendEquation(GL_FUNC_ADD);
        break;

    case BLEND_INTERPOLATIVE:
        glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
        glBlendEquation(GL_FUNC_ADD);
        break;

    case BLEND_MULTIPLICATIVE:
        glBlendFunc(GL_DST_COLOR, GL_ZERO);
        glBlendEquation(GL_FUNC_ADD);
        break;

    case BLEND_OVERLAY:
        glBlendFunc(GL_SRC_ALPHA, GL_ONE);
        glBlendEquation(GL_FUNC_ADD);
        break;

    case BLEND_REPLACE:
        glBlendFunc(GL_ONE, GL_ZERO);
        glBlendEquation(GL_FUNC_ADD);
        break;

    case BLEND_SCREEN:
        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_COLOR);
        glBlendEquation(GL_FUNC_ADD);
        break;

    default:
        mikoUnreachable();
        break;
    };

    CHECK_GL_ERROR(__func__);
}

static GLenum primitiveEnum(PrimitiveType e)
{
    switch (e) {
    case PRIMITIVE_POINTS: return GL_POINTS;

    case PRIMITIVE_LINE_STRIP: return GL_LINE_STRIP;
    case PRIMITIVE_LINE_LOOP: return GL_LINE_LOOP;
    case PRIMITIVE_LINES: return GL_LINES;

    case PRIMITIVE_TRIANGLE_STRIP: return GL_TRIANGLE_STRIP;
    case PRIMITIVE_TRIANGLE_FAN: return GL_TRIANGLE_FAN;
    case PRIMITIVE_TRIANGLES: return GL_TRIANGLES;
    }

    mikoUnreachable();
    return GL_POINTS;
}

void GlRenderer::bindIndexBuffer(GlIndexBuffer *ibo)
{
    if (indexBuffer == ibo) {
        return;
    }

    indexBuffer = ibo;

    if (ibo) {
        ibo->bind();
    }
    else {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
}

void GlRenderer::bindVertexBuffer(GlVertexBuffer *vbo)
{
    if (vertexBuffer == vbo) {
        return;
    }

    vertexBuffer = vbo;

    if (!vbo) {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        return;
    }

    vbo->bind();

    const VertexFormat &vertexFormat = vbo->getVertexFormat();

    for (unsigned int i = 0; i < COMPONENT_NAME_COUNT; i++) {
        VertexComponentName e = static_cast<VertexComponentName>(i);

        if (!vertexFormat.hasComponent(e)) {
            glDisableVertexAttribArray(i);
            CHECK_GL_ERROR("glDisableVertexAttribArray");
            continue;
        }
        else {
            glEnableVertexAttribArray(i);
            CHECK_GL_ERROR("glEnableVertexAttribArray");
        }

        const VertexComponentType &type = vertexFormat.componentType(e);
        GLint size = vertexComponentNumMembers(type);
        GLint offset = vertexFormat.componentOffset(e);
        GLsizei stride = vertexFormat.vertexSize();

        // OpenGL won't let us have more
        mikoAssert(size >= 1 && size <= 4);

        #if 0
        LOG_INFOF("attribute %s bound to location %d, size %d, offset %d with stride %d",
            VertexComponentName::toStr(e),
            i, size, offset, stride
        );
        #endif

        glVertexAttribPointer(i, size, GL_FLOAT, GL_FALSE, stride, reinterpret_cast<GLvoid *>(offset));

        GLenum err = glGetError();
        if (err != GL_NO_ERROR) {
            LOG_ERRORF("error binding attribute %s", toStr(e));
            LOG_ERRORF("%s", gluErrorString(err));
        }
    }
    // LOG_INFOF("bound buffer with %u vertices", vbo->size());

    CHECK_GL_ERROR(__func__);
}

void GlRenderer::drawElements(PrimitiveType type, uint32_t first, uint32_t count)
{
    if (!program) {
        return;
    }

    glDrawElements(primitiveEnum(type), count, GL_UNSIGNED_SHORT, reinterpret_cast<const GLvoid *>(first));
    CHECK_GL_ERROR(__func__);
}

void GlRenderer::drawArrays(PrimitiveType type, uint32_t first, uint32_t count)
{
    if (!program) {
        return;
    }

    glDrawArrays(primitiveEnum(type), first, count);

    CHECK_GL_ERROR(__func__);
}

void GlRenderer::invalidateUniforms()
{
    uniforms.dirty = ~0;
}

void GlRenderer::updateUniforms()
{
    if (!program) {
        return;
    }

    if (uniforms.timeDirty) {
        uniform1f("cTime", uniforms.time);
    }

    if (uniforms.eyePositionDirty) {
        uniform3f("cEyePosition", uniforms.eyePosition);
    }

    if (uniforms.eyeDirectionDirty) {
        uniform3f("cEyeDirection", uniforms.eyeDirection);
    }

    if (uniforms.projectionDirty) {
        uniformMatrix4f("cProjection", uniforms.projection);
    }

    if (uniforms.ambientColorDirty) {
        uniform3f("cAmbient", uniforms.ambientColor);
    }

    if (uniforms.ambientDirty) {
        uniform4f("cMatAmbient", uniforms.ambient);
    }

    if (uniforms.diffuseDirty) {
        uniform4f("cMatDiffuse", uniforms.diffuse);
    }

    if (uniforms.specularDirty) {
        uniform4f("cMatSpecular", uniforms.specular);
    }

    if (uniforms.emissiveDirty) {
        uniform4f("cMatEmissive", uniforms.emissive);
    }

    if (uniforms.viewDirty) {
        uniformMatrix4f("cView", uniforms.view);
    }

    if (uniforms.projectionDirty) {
        uniformMatrix4f("cProjection", uniforms.projection);
    }

    if (uniforms.modelViewProjectionDirty) {
        uniformMatrix4f("cModelViewProjection", uniforms.modelViewProjection);
    }

    if (uniforms.normalMatrixDirty) {
        uniformMatrix3f("cNormalMatrix", uniforms.normalMatrix);
    }

    uniforms.dirty = 0;
}

void GlRenderer::drawDebugLine(
        const glm::vec4 &color,
        const glm::vec3 &start,
        const glm::vec3 &end)
{
    DebugLine line = {
        { start, color },
        { end, color }
    };

    if (!debugLines.append(line)) {
        // TODO: handle error
    }
}

void GlRenderer::drawDebugLine(
        const glm::vec3 &start,
        const glm::vec3 &end,
        const glm::vec4 &startColor,
        const glm::vec4 &endColor)
{
    DebugLine line = {
        {start, startColor },
        {end, endColor }
    };

    if (!debugLines.append(line)) {
        // TODO: handle error
    }
}


void GlRenderer::drawDebugTriangle(
        const glm::vec4 &color,
        const glm::vec3 &a,
        const glm::vec3 &b,
        const glm::vec3 &c)
{
    drawDebugLine(color, a, b);
    drawDebugLine(color, b, c);
    drawDebugLine(color, c, a);
}

void GlRenderer::drawDebugQuad(
        const glm::vec4 &color,
        const glm::vec3 &x,
        const glm::vec3 &y,
        const glm::vec3 &width,
        const glm::vec3 &height)
{
    drawDebugLine(color, x, y);
    drawDebugLine(color, x + width, y);
    drawDebugLine(color, x + width, y + height);
    drawDebugLine(color, x, y + height);
}

void GlRenderer::drawDebugCircle(
        const glm::vec4 &color,
        const glm::vec3 &center,
        float radius,
        int segments)
{
    float sd = (M_PI * 2.0f) / segments;

    for (int i = 0; i < segments; i++) {
        float ad = sd * ((i + 0) % segments);
        float bd = sd * ((i + 1) % segments);

        drawDebugLine(color,
            center + glm::vec3( cosf(ad), sinf(ad), 0.0f),
            center + glm::vec3( cosf(bd), sinf(bd), 0.0f)
        );
    }
}

void GlRenderer::uniform1f(const char *name, float value)
{
    mikoAssert(program != NULL);
    glUniform1f(program->uniformLocation(name), value);
}

void GlRenderer::uniform2f(const char *name, const glm::vec2& value)
{
    mikoAssert(program != NULL);
    glUniform2f(program->uniformLocation(name), value.x, value.y);
}

void GlRenderer::uniform3f(const char *name, const glm::vec3& value)
{
    mikoAssert(program != NULL);
    GLint location = program->uniformLocation(name);
    // if (location < 0) {
    //     LOG_WARNF("uniform %s not defined", name);
    // }
    glUniform3f(location, value.x, value.y, value.z);
}

void GlRenderer::uniform4f(const char *name, const glm::vec4& value)
{
    mikoAssert(program != NULL);
    glUniform4f(program->uniformLocation(name), value.x, value.y, value.z, value.w);
}

void GlRenderer::uniformMatrix3f(const char *name, const glm::mat3 &value, bool transpose)
{
    mikoAssert(program != NULL);
    glUniformMatrix3fv(program->uniformLocation(name), 1, transpose, &value[0][0]);
}

void GlRenderer::uniformMatrix4f(const char *name, const glm::mat4 &value, bool transpose)
{
    mikoAssert(program != NULL);
    GLint location = program->uniformLocation(name);
    // if (location < 0) {
    //     LOG_WARNF("uniform %s not defined", name);
    // }

    glUniformMatrix4fv(location, 1, transpose, &value[0][0]);
}

void GlRenderer::uniformMatrix4fv(const char *name, int count, const glm::mat4 *values, bool transpose)
{
    mikoAssert(program != NULL);
    GLint location = program->uniformLocation(name);
    // if (location < 0) {
    //     LOG_WARNF("uniform %s not defined", name);
    // }
    glUniformMatrix4fv(location, count, transpose, (const GLfloat *)values);
}

} /* namespace miko */
