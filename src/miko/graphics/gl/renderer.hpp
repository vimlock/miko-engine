#ifndef MIKO_GL_RENDERER_HPP
#define MIKO_GL_RENDERER_HPP

#include "miko/container/hash_map.hpp"

#include "miko/graphics/camera.hpp"
#include "miko/graphics/renderer.hpp"

#include "miko/graphics/gl/buffer.hpp"
#include "miko/graphics/gl/framebuffer.hpp"
#include "miko/graphics/gl/program.hpp"
#include "miko/graphics/gl/material.hpp"
#include "miko/graphics/gl/texture_map.hpp"

namespace miko {

class GlMesh;
class GlSubMesh;

class GlModel;
class GlAnimatedModel;

class IWindow;

struct DebugVertex
{
    glm::vec3 position;
    glm::vec4 color;
};

struct DebugLine
{
    DebugVertex start;
    DebugVertex end;
};

struct SpriteVertex
{
    glm::vec3 position;
    glm::vec2 texcoords;
    float     alpha;
};

struct SpriteData
{
    SpriteVertex vertices[4];
};

struct ShaderUniforms
{
    union {
        uint32_t dirty;
        struct {
            uint32_t timeDirty         :1;

            uint32_t fogStartDirty     :1;
            uint32_t fogEndDirty       :1;
            uint32_t fogColorDirty     :1;

            uint32_t ambientColorDirty :1;

            uint32_t eyePositionDirty  :1;
            uint32_t eyeDirectionDirty :1;

            uint32_t diffuseDirty      :1;
            uint32_t specularDirty     :1;
            uint32_t emissiveDirty     :1;
            uint32_t ambientDirty      :1;

            uint32_t projectionDirty   :1;
            uint32_t viewDirty         :1;

            uint32_t modelViewProjectionDirty:1;
            uint32_t normalMatrixDirty :1;

            uint32_t invModelViewProjectionDirty:1;
            uint32_t fovDirty          :1;
            uint32_t nearplaneDirty    :1;
            uint32_t farplaneDirty     :1;
        };
    };

    float time;

    float fogStart;
    float fogEnd;
    glm::vec3 fogColor;

    glm::vec3 ambientColor;

    glm::vec3 eyePosition;
    glm::vec3 eyeDirection;

    glm::vec4 diffuse;
    glm::vec4 specular;
    glm::vec4 emissive;
    glm::vec4 ambient;

    glm::mat4 projection;
    glm::mat4 view;
    glm::mat4 model;

    glm::mat4 modelViewProjection;
    glm::mat3 normalMatrix;
    glm::mat4 invModelViewProjection;
};

/// OpenGL renderer
class GlRenderer : public IRenderer
{
public:
    virtual ~GlRenderer();

    static GlRenderer * create(Allocator &allocator, IWindow *owner);

    void update(float timePassed);

    void setCamera(const Camera &camera);
    void setTexture(unsigned int index, GlTexture *texture);
    void setMaterial(GlMaterial *mat);
    void setProgram(GlShaderProgram *program);
    void setFramebuffer(GlFramebuffer *framebuffer);

    void setDepthTest(bool enabled);
    void setDepthWrite(bool enabled);
    void setTwosided(bool enabled);
    void setWireframe(bool enabled);
    void setBlend(BlendType mode);

    void drawSprite(const glm::vec3 &position, const Sprite &sprite);
    void drawMesh(const glm::mat4 &transform, GlMesh *mesh);
    void drawModel(const glm::mat4 &transform, GlModel &model);
    void drawAnimatedModel(const glm::mat4 &transform, GlAnimatedModel &model);

    void drawQuad();
    void drawDebugShapes();
    void drawBatches();

    void drawDebugSkeleton(
        const glm::mat4 &transform,
        GlAnimatedModel &model
    );

    void drawDebugBone(
        const glm::mat4 &parentTransform,
        GlAnimatedModel &model,
        int index
    );

    void drawDebugLine(
        const glm::vec4 &color,
        const glm::vec3 &start,
        const glm::vec3 &end
    );

    void drawDebugLine(
        const glm::vec3 &start,
        const glm::vec3 &end,
        const glm::vec4 &startColor,
        const glm::vec4 &endColor
    );

    void drawDebugTriangle(
        const glm::vec4 &color,
        const glm::vec3 &a,
        const glm::vec3 &b,
        const glm::vec3 &c
    );

    void drawDebugQuad(
        const glm::vec4 &color,
        const glm::vec3 &x,
        const glm::vec3 &y,
        const glm::vec3 &width,
        const glm::vec3 &height
    );

    void drawDebugCircle(
        const glm::vec4 &color,
        const glm::vec3 &center,
        float radius,
        int segments
    );

private:
    GlRenderer(IWindow *owner);

    void drawSubMesh(const glm::mat4 &translation, GlSubMesh *mesh);

    void endBatch();

    void clearTextureMap();
    void bindTextureMap(GlTextureMap *tm);

    void bindIndexBuffer(GlIndexBuffer *ibo);
    void bindVertexBuffer(GlVertexBuffer *vbo);

    void drawElements(PrimitiveType type, uint32_t first, uint32_t count);
    void drawArrays(PrimitiveType type, uint32_t first, uint32_t count);

    void invalidateUniforms();
    void updateUniforms();

    void uniform1f(const char *name, float value);
    void uniform2f(const char *name, const glm::vec2& value);
    void uniform3f(const char *name, const glm::vec3& value);
    void uniform4f(const char *name, const glm::vec4& value);

    void uniformMatrix4f(const char *name, const glm::mat4 &value, bool transpose=false);
    void uniformMatrix3f(const char *name, const glm::mat3 &value, bool transpose=false);

    void uniformMatrix4fv(const char *name, int count, const glm::mat4 *values, bool transpose=false);

    IWindow *owner;

    GLuint vertexArray;

    Ref<GlIndexBuffer> indexBuffer;
    Ref<GlVertexBuffer> vertexBuffer;

    Ref<GlFramebuffer> framebuffer;

    Ref<GlTexture> textures[8];
    Ref<GlShaderProgram> program;
    Ref<GlMaterial> material;
    Ref<GlVertexBuffer> quadVertices;

    Ref<GlVertexBuffer> debugVertices;
    Vector<DebugLine> debugLines;

    Ref<GlVertexBuffer> spriteVertices;
    Ref<GlIndexBuffer> spriteIndices;
    Vector<SpriteData> sprites;

    ShaderUniforms uniforms;

    bool depthTestMode;
    bool depthWriteMode;
    bool twosidedMode;
    bool wireframeMode;
    BlendType blendMode;

};
    
} /* namespace miko */

#endif /* MIKO_GL_RENDERER_HPP */

