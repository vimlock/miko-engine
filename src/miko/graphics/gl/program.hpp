#ifndef MIKO_GRAPHICS_GL_PROGRAM_HPP
#define MIKO_GRAPHICS_GL_PROGRAM_HPP

#include "miko/container/hash_map.hpp"

#include "miko/graphics/gl/graphics.hpp"
#include "miko/graphics/gl/shader.hpp"

#include "miko/resource/resource_handle.hpp"
#include "miko/resource/resource_loader.hpp"

namespace miko {

class TextDeserializer;
class GlShaderProgramFactory;

struct GlProgramVariableInfo
{
    GLint location;
    GLint  size;
    GLenum type;
};

class GlShaderProgram : public Resource, public GraphicsObject
{
MIKO_RESOURCE(RESOURCE_SHADER_PROGRAM)
public:
    friend class GlShaderProgramFactory;

    virtual ~GlShaderProgram();

    virtual bool loadAsyncPart(SDL_RWops *rwops);
    virtual bool loadSyncPart(SDL_RWops *rwops);

    const GlProgramVariableInfo * uniformInfo(const char *name) const;
    const GlProgramVariableInfo * attribInfo(const char *name) const;

    GLint uniformLocation(const char *name) const;
    GLint attribLocation(const char *name) const;

    void bind();

private:
    bool deserialize(TextDeserializer &s);
    bool loadToGpu();

    bool cacheUniforms();
    bool cacheAttribs();

    /// Private constructor, should only be called trough GlShaderProgramFactory
    GlShaderProgram(Allocator &allocator, GlShaderProgramFactory *creator);

    /// Reference to the shader on the GPU
    GLuint gpuObject;

    // The factory which was used to create this shader
    //
    // Reference to the creator factory is kept to be able to access
    // shader headers and hints.
    GlShaderProgramFactory *creator;

    HashMap<std::string, GlProgramVariableInfo> attribs;
    HashMap<std::string, GlProgramVariableInfo> uniforms;

    ResourceHandle<GlShaderSource> vertexSource;
    ResourceHandle<GlShaderSource> fragmentSource;
};

class GlShaderProgramFactory : public IResourceFactory
{
public:
    friend GlShaderProgram;
    GlShaderProgramFactory();

    virtual GlShaderProgram * create(Allocator &allocator);

private:
    /// Prefix for all shaders
    std::string commonHeader;

    /// Prefix for vertex shaders which is added after commonPrefix
    std::string vertexHeader;

    /// Prefix for fragment shaders which is added after commonPrefix
    std::string fragmentHeader;
};
    
} /* namespace miko */

#endif /* MIKO_GRAPHICS_GL_PROGRAM_HPP */

