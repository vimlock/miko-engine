#ifndef MIKO_GL_SHADER_HPP
#define MIKO_GL_SHADER_HPP

#include "miko/resource/resource_loader.hpp"

#include "miko/graphics/graphics.hpp"
#include "miko/graphics/gl/graphics.hpp"
#include "miko/graphics/shader.hpp"
#include "miko/container/hash_map.hpp"
#include "miko/container/vector.hpp"

#include <glm/glm.hpp>
#include <string>

namespace miko {

class ResourceManager;

class GlShaderSource : public Resource
{
    MIKO_RESOURCE(RESOURCE_SHADER_SOURCE)
public:

    GlShaderSource(Allocator &allocator);

    virtual bool loadAsyncPart(SDL_RWops *rwops);
    virtual bool loadSyncPart(SDL_RWops *rwops);

    const char * getSource() const
    {
        return source.empty() ? NULL : &source[0];
    }

    uint32_t getLength() const
    {
        return source.size();
    }
    
private:
    Vector<char> source;

};

class GlShaderSourceFactory : public IResourceFactory
{
public:
    virtual GlShaderSource * create(Allocator &allocator);
};

} /* namespace miko */

#endif /* MIKO_GL_SHADER_HPP */

