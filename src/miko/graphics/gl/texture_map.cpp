#include "miko/graphics/gl/texture_map.hpp"
#include "miko/io/log.hpp"

namespace miko {

GlTextureMap::GlTextureMap(Allocator &allocator):
    textures(allocator)
{
    // empty
}

GlTextureMap * GlTextureMap::create(Allocator &allocator)
{
    return MIKO_NEW(allocator, GlTextureMap) (allocator);
}

bool GlTextureMap::add(const std::string &name, GlTexture *texture)
{
    if (textures.find(name) != textures.end()) {
        LOG_ERRORF("duplicate texture name: %s", name.c_str());
        return false;
    }

    textures.set(name, texture);

    return true;
}
    
} /* namespace miko */
