#ifndef MIKO_GL_GRAPHICS_HPP
#define MIKO_GL_GRAPHICS_HPP

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "miko/graphics/graphics.hpp"

#define CHECK_GL_ERROR(func)      \
do {                              \
  GLenum err;                     \
  err = glGetError();             \
  if (err != GL_NO_ERROR) {       \
      LOG_ERRORF("%s: %s", func, gluErrorString(err)); \
  }                               \
} while (0)

namespace miko {

class GlObject : public GraphicsObject
{
public:
};
    
} /* namespace miko */

#endif /* MIKO_GL_GRAPHICS_HPP */

