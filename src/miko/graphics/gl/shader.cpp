#include "miko/graphics/gl/shader.hpp"

#include "miko/resource/resource_loader.hpp"
#include "miko/resource/resource_manager.hpp"
#include "miko/graphics/graphics.hpp"
#include "miko/io/log.hpp"

namespace miko {

// ============================================================================
// GlShaderSource

GlShaderSource::GlShaderSource(Allocator &allocator):
    source(allocator)
{
}

bool GlShaderSource::loadAsyncPart(SDL_RWops *rwops)
{
    Sint64 size = SDL_RWsize(rwops);
    if (size < 0) {
        return false;
    }

    // Reserve memory for the source + 1 for null terminator
    source.resize(size + 1);
    char *buf = &source[0];

    Sint64 nread = 0;
    while (nread < size) {
        Sint64 res = SDL_RWread(rwops, buf, 1, size - nread);
        if (res == 0) {
            break;
        }

        nread += res;
        buf += res;
    }

    if (nread != size) {
        return false;
    }

    source[size] = '\0';

    return true;
}

bool GlShaderSource::loadSyncPart(SDL_RWops *rwops)
{
    return true;
}

// ============================================================================
// GlShaderSourceFactory

GlShaderSource * GlShaderSourceFactory::create(Allocator &allocator)
{
    return MIKO_NEW(allocator, GlShaderSource) (allocator);
}

} /* namespace miko */
