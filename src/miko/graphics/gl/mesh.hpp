#ifndef MIKO_GRAPHICS_GL_MESH_HPP
#define MIKO_GRAPHICS_GL_MESH_HPP

#include "miko/graphics/mesh.hpp"
#include "miko/graphics/skeleton.hpp"
#include "miko/graphics/gl/buffer.hpp"
#include "miko/graphics/gl/material.hpp"

#include "miko/resource/resource_loader.hpp"

namespace miko {

class GlGeometry
{
public:
    ResourceHandle<GlMaterial> material;
    PrimitiveType primitive;
    uint32_t indexOffset;
    uint32_t indexCount;
};

class GlSubMesh
{
public:
    friend class GlMesh;

    GlSubMesh(Allocator &allocator);

    uint32_t numGeometries() const
    {
        return geometries.size();
    }

    GlGeometry * getGeometry(unsigned int index)
    {
        if (index < geometries.size()) {
            return &geometries[index];
        }
        else {
            return NULL;
        }
    }

    GlIndexBuffer * getIndices()
    {
        return indices;
    }

    GlVertexBuffer * getVertices()
    {
        return vertices;
    }

private:
    VertexFormat vertexFormat;
    Ref<GlIndexBuffer> indices;
    Ref<GlVertexBuffer> vertices;
    Vector<GlGeometry> geometries;
};

class GlMesh : public Resource
{
MIKO_RESOURCE(RESOURCE_MESH)
public:
    GlMesh(Allocator &allocator);
    virtual ~GlMesh();
    
    virtual bool loadAsyncPart(SDL_RWops *rwops);
    virtual bool loadSyncPart(SDL_RWops *rwops);

    bool isLoadedToGpu() const;
    bool loadToGpu();
    void unloadFromGpu();

    bool isLoadedToMemory() const;
    void unloadFromMemory(); 

    uint32_t numSubMeshes() const
    {
        return submeshes.size();
    }

    GlSubMesh * getSubMesh(uint32_t index)
    {
        if (index < submeshes.size()) {
            return submeshes[index];
        }
        else {
            return NULL;
        }
    }

    Skeleton * getSkeleton()
    {
        return skeleton;
    }

    void setSkeleton(Skeleton *_skeleton)
    {
        skeleton = _skeleton;
    }

private:
    GlSubMesh * addSubmesh();

    bool isLoaded;
    Vector<GlSubMesh *> submeshes;

    MeshData *meshData;
    Ref<Skeleton> skeleton;
};

class GlMeshFactory : public IResourceFactory
{
public:
    virtual GlMesh * create(Allocator &allocator);

};
    
} /* namespace miko */

#endif /* MIKO_GRAPHICS_GL_MESH_HPP */

