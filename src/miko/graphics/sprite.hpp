#ifndef MIKO_SPRITE_HPP
#define MIKO_SPRITE_HPP

#include "miko/miko.hpp"

#include "miko/container/vector.hpp"
#include "miko/container/hash_map.hpp"

#include "miko/graphics/gl/texture_map.hpp"
#include "miko/graphics/gl/material.hpp"

#include "miko/resource/resource.hpp"

#include <string>
#include <glm/glm.hpp>

namespace miko {

class SpriteAnimation;
class TextSerializer;
class TextDeserializer;

struct MIKO_API SpriteFrame
{
    SpriteFrame();

    float x;
    float y;
    float width;
    float height;
    glm::vec2 origin;
};

struct MIKO_API AnimatedSpriteFrame : public SpriteFrame
{
    AnimatedSpriteFrame();

    uint16_t duration;
};

struct MIKO_API SpriteAnimationTrack
{
    std::string name;

    // First frame of the track
    uint16_t start;

    // Length of the track, should be between 1 - frames.size()
    uint16_t length;
};

class MIKO_API SpriteAnimation : public Resource
{
MIKO_RESOURCE(RESOURCE_SPRITE_ANIMATION)
public:
    friend class AnimatedSprite;
    friend class SpriteEditor;

    enum LoopType {
        LOOP_ONCE,
        LOOP_REPEAT
    };

    SpriteAnimation(Allocator &allocator);

    static SpriteAnimation * create(Allocator &allocator);

    virtual bool loadAsyncPart(SDL_RWops *rwops);
    virtual bool loadSyncPart(SDL_RWops *rwops);

private:
    bool serialize(TextSerializer &s) const;
    bool deserialize(TextDeserializer &s);

    LoopType loop;

    Vector<AnimatedSpriteFrame> frames;
    Vector<SpriteAnimationTrack> tracks;

    ResourceHandle<GlTexture> texture;
    ResourceHandle<GlMaterial> material;
};

class MIKO_API SpriteAnimationFactory : public IResourceFactory
{
public:
    virtual SpriteAnimation * create(Allocator &allocator);

};

class MIKO_API Sprite
{
public:
    friend class GlRenderer;

    Sprite();


    SpriteFrame frame;
    // in radians 
    float angle;

    // original scale is 1.0
    float scale;

    // [0.0, 1.0]
    float alpha;

    Ref<GlMaterial> material;
    Ref<GlTexture> texture;
};

class MIKO_API AnimatedSprite : public Sprite
{
public:
    AnimatedSprite();
    AnimatedSprite(SpriteAnimation *anim);

    void setAnimation(SpriteAnimation *anim);
    void update();

private:
    Ref<SpriteAnimation> anim;

    /// Index of the start frame (used if looping)
    uint16_t startFrame;

    // Index of the endFrame
    uint16_t endFrame;

    uint16_t currentFrame;

    uint16_t timer;
};

} /* namespace miko */

#endif /* MIKO_SPRITE_HPP */

