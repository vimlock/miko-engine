#ifndef MIKO_MATERIAL_HPP
#define MIKO_MATERIAL_HPP

#include "miko/resource/resource.hpp"
#include <glm/glm.hpp>

namespace miko {

class IMaterial : public Resource
{
MIKO_RESOURCE(RESOURCE_MATERIAL)
public:
};

} /* namespace miko */

#endif /* MIKO_MATERIAL_HPP */

