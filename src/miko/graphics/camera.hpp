#ifndef MIKO_GRAPHICS_CAMERA_HPP
#define MIKO_GRAPHICS_CAMERA_HPP

#include <glm/glm.hpp>

namespace miko {

namespace CameraMode
{
    enum Enum
    {
        DISABLED,
        ORTHO,
        PERSPECTIVE
    };
}

class Camera
{
public:
    Camera();

    void setPerspective(float fov, float xscale, float yscale);
    void setOrtho(float xscale, float yscale);
    void setClipPlane(float nearplane, float farplane);

    void setPosition(const glm::vec3 &position);
    void setTarget(const glm::vec3 &target);

    const glm::vec3& getPosition() const
    {
        return position;
    }

    const glm::vec3& getTarget() const
    {
        return target;
    }

    const glm::vec3 getDirection() const;

    const glm::mat4 getViewMatrix() const;
    const glm::mat4 getProjectionMatrix(const glm::vec2& dimensions) const;

private:
    CameraMode::Enum mode;

    glm::vec3 position;
    glm::vec3 target;
    glm::vec3 up;

    float nearplane;
    float farplane;

    float fov;

    glm::vec2 scale;
};
    
} /* namespace miko */

#endif /* MIKO_GRAPHICS_CAMERA_HPP */

