#ifndef MIKO_GRAPHICS_MESH_HPP
#define MIKO_GRAPHICS_MESH_HPP

#include "miko/container/vector.hpp"
#include "miko/container/hash_map.hpp"

#include "miko/core/object.hpp"

#include "miko/graphics/graphics.hpp"

#include <string>
#include <glm/glm.hpp>

namespace miko {

class TextSerializer;
class TextDeserializer;

class Skeleton;

class GeometryData
{
public:
    GeometryData()
    {
        // empty
    }

    GeometryData(const std::string &_material, uint32_t _indexOffset, uint32_t _indexCount):
            materialName(_material),
            primitiveType(PRIMITIVE_TRIANGLES),
            indexOffset(_indexOffset),
            indexCount(_indexCount)
    {
        // empty
    }

    std::string materialName;
    PrimitiveType primitiveType;

    uint32_t indexOffset;
    uint32_t indexCount;
};

class BoneWeight
{
public:
    uint16_t index;
    float weight;
};

class BoneData
{
public:
    BoneData(Allocator &allocator);

    std::string name;
    glm::mat4 transform;
    Vector<BoneWeight> weights;
};

class SubMeshData
{
public:
    SubMeshData(Allocator &allocator);
    
    bool serialize(TextSerializer &s) const;
    bool deserialize(TextDeserializer &s);

    void serializeIndices(TextSerializer &s) const;
    void serializeVertices(TextSerializer &s) const;
    void serializeTextures(TextSerializer &s) const;
    void serializeGeometries(TextSerializer &s) const;

    bool deserializeIndices(TextDeserializer &s);
    bool deserializeVertices(TextDeserializer &s);
    bool deserializeTextures(TextDeserializer &s);
    bool deserializeGeometries(TextDeserializer &s);

    std::string name;

    Vector<uint16_t> indices;

    VertexFormat vertexFormat;
    Vector<char> vertices;

    Vector<GeometryData> geometries;
};

class MeshData
{
public:
    MeshData(Allocator &allocator);
    ~MeshData();
    
    bool serialize(TextSerializer &s) const;
    bool deserialize(TextDeserializer &s);

    void dump() const;

    SubMeshData * addSubmesh();
    Skeleton * addSkeleton();
    Skeleton * getSkeleton();

    uint32_t numSubMeshes() const
    {
        return submeshes.size();
    }

    const SubMeshData * getSubMesh(unsigned int index) const
    {
        if (index < submeshes.size()) {
            return submeshes[index];
        }
        else {
            return NULL;
        }
    }

private:

    Vector<SubMeshData *> submeshes;

    Allocator &allocator;
    Ref<Skeleton> skeleton;
};

} /* namespace miko */

#endif /* MIKO_GRAPHICS_MESH_HPP */

