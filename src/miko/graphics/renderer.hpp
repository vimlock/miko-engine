#ifndef MIKO_RENDERER_HPP
#define MIKO_RENDERER_HPP

namespace miko {

class Sprite;

class IRenderer
{
public:
    virtual ~IRenderer()
    {
        // empty
    }
};
    
} /* namespace miko */

#endif /* MIKO_RENDERER_HPP */

