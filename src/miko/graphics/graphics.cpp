#include "miko/graphics/graphics.hpp"
#include "miko/io/log.hpp"
#include "miko/util/assert.hpp"

namespace miko
{

uint32_t vertexComponentNumMembers(VertexComponentType t)
{
    switch (t) {
    case COMPONENT_FLOAT: return 1;
    case COMPONENT_VEC2:  return 2;
    case COMPONENT_VEC3:  return 3;
    case COMPONENT_VEC4:  return 4;
    case COMPONENET_TYPE_COUNT: break;
    }

    mikoUnreachable();
    return 1;
}

uint32_t vertexComponentSize(VertexComponentType t)
{
    switch (t) {
    case COMPONENT_FLOAT: return sizeof(float);
    case COMPONENT_VEC2: return sizeof(float) * 2;
    case COMPONENT_VEC3: return sizeof(float) * 3;
    case COMPONENT_VEC4: return sizeof(float) * 4;
    case COMPONENET_TYPE_COUNT: break;
    }

    mikoUnreachable();
    return 0;
}

VertexFormat::VertexFormat()
{
    componentMask = 0;
    size = 0;
}

VertexFormat::VertexFormat(const char *fmt)
{
    componentMask = 0;
    size = 0;

    mikoAssert(addFromFormat(fmt));
}

bool VertexFormat::addFromFormat(const char *fmt)
{
    const char *iter = fmt;

    while (*iter) {
        VertexComponentName name;
        VertexComponentType type;
    
        // read name
        switch (*iter) {
        case 'p': name = COMPONENT_POSITION; break;
        case 't': name = COMPONENT_TEXCOORD; break;
        case 'n': name = COMPONENT_NORMAL; break;
        case 'g': name = COMPONENT_TANGENT; break;
        case 'c': name = COMPONENT_COLOR; break;
        case 'a': name = COMPONENT_ALPHA; break;
        case 'w': name = COMPONENT_BONE_WEIGHT; break;
        case 'b': name = COMPONENT_BONE_INDEX; break;
        default:
            LOG_ERRORF("invalid format specifier %c", *iter);
            return false;
        }

        iter++;

        // read type
        switch (*iter) {
        case '1': type = COMPONENT_FLOAT; break;
        case '2': type = COMPONENT_VEC2; break;
        case '3': type = COMPONENT_VEC3; break;
        case '4': type = COMPONENT_VEC4; break;
        default:
            LOG_ERRORF("invalid format specifier %c", *iter);
            return false;
        }

        iter++;

        if (!addComponent(name, type)) {
            return false;
        }
    }

    return true;
}

std::string VertexFormat::toStr() const
{
    std::string tmp;

    for (unsigned int i = 0; i < 8; i++) {
        if (!(componentMask & (1 << i))) {
            continue;
        }

        switch (i) {
        case COMPONENT_POSITION: tmp += 'p'; break;
        case COMPONENT_TEXCOORD: tmp += 't'; break;
        case COMPONENT_NORMAL:   tmp += 'n'; break;
        case COMPONENT_TANGENT:  tmp += 'g'; break;
        case COMPONENT_COLOR:    tmp += 'c'; break;
        case COMPONENT_ALPHA:    tmp += 'a'; break;
        case COMPONENT_BONE_WEIGHT:  tmp += 'w'; break;
        case COMPONENT_BONE_INDEX:   tmp += 'b'; break;
        }

        switch (components[i].type) {
        case COMPONENT_FLOAT: tmp += '1'; break;
        case COMPONENT_VEC2: tmp += '2'; break;
        case COMPONENT_VEC3: tmp += '3'; break;
        case COMPONENT_VEC4: tmp += '4'; break;
        }
    }

    return tmp;
}

bool VertexFormat::addComponent(VertexComponentName name, VertexComponentType type)
{
    if (hasComponent(name)) {
        return false;
    }

    components[name].type = type;
    components[name].offset = size;

    componentMask |= (1 << name);
    size += vertexComponentSize(type);

    return true;
}

bool VertexFormat::hasComponent(VertexComponentName name) const
{
    return componentMask & (1 << name);
}

int VertexFormat::componentOffset(VertexComponentName name) const
{
    mikoAssert(hasComponent(name));
    return components[name].offset;
}

VertexComponentType VertexFormat::componentType(VertexComponentName name) const
{
    mikoAssert(hasComponent(name));
    return static_cast<VertexComponentType>(components[name].type);
}

uint32_t VertexFormat::vertexSize() const
{
    return size;
}

const char * toStr(VertexComponentName e)
{
    switch (e) {
    case COMPONENT_POSITION: return "iPosition";
    case COMPONENT_TEXCOORD: return "iTexcoord";
    case COMPONENT_NORMAL:   return "iNormal";
    case COMPONENT_TANGENT:  return "iTangent";
    case COMPONENT_COLOR:    return "iColor";
    case COMPONENT_ALPHA:    return "iAlpha";
    case COMPONENT_BONE_WEIGHT: return "iBoneWeight";
    case COMPONENT_BONE_INDEX:  return "iBoneIndex";
    }

    mikoUnreachable();
    return "error";
}

const char * toStr(VertexComponentType t)
{
    switch (t) {
    case COMPONENT_FLOAT: return "float";
    case COMPONENT_VEC2: return "vec2";
    case COMPONENT_VEC3: return "vec3";
    case COMPONENT_VEC4: return "vec4";
    }

    mikoUnreachable();
    return "error";
}

} /* namespace miko */
