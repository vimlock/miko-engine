#include "miko/graphics/sprite.hpp"
#include "miko/io/text_deserializer.hpp"
#include "miko/io/text_serializer.hpp"

#include "miko/resource/resource_manager.hpp"

namespace miko {

static const EnumInfo loopTypeEnum[] = {
    { "once",   SpriteAnimation::LOOP_ONCE },
    { "repeat", SpriteAnimation::LOOP_REPEAT },

    { NULL, 0} // sentinel
};

SpriteFrame::SpriteFrame():
        x(0.0f),
        y(0.0f),
        width(0.0f),
        height(0.0f),
        origin(0.0f, 0.0f)
{
    // empty
}

AnimatedSpriteFrame::AnimatedSpriteFrame():
        duration(0)
{
    // empty
}

SpriteAnimation::SpriteAnimation(Allocator &allocator):
    loop(LOOP_REPEAT),
    frames(allocator),
    tracks(allocator)
{
    // empty
}

SpriteAnimation * SpriteAnimation::create(Allocator &allocator)
{
    return MIKO_NEW(allocator, SpriteAnimation) (allocator);
}

bool SpriteAnimation::loadAsyncPart(SDL_RWops *rwops)
{
    TextDeserializer s(getName().c_str(), rwops);
    return deserialize(s);
}

bool SpriteAnimation::loadSyncPart(SDL_RWops *rwops)
{
    return true;
}

bool SpriteAnimation::serialize(TextSerializer &s) const
{
    s.writeIdentifier("texture");
    s.writeWhitespace();
    s.writeString(texture.getName());
    s.writeNewline();

    s.writeIdentifier("material");
    s.writeWhitespace();
    s.writeString(material.getName());
    s.writeNewline();

    s.writeIdentifier("loop");
    s.writeWhitespace();
    s.writeEnum(loopTypeEnum, loop);
    s.writeNewline();

    s.writeIdentifier("frames");
    s.writeWhitespace();
    s.writeRaw('{');
    s.indent();
    for (unsigned int i = 0; i < frames.size(); i++){
        const AnimatedSpriteFrame &f = frames[i];

        // Format the texcoords in group of 4 for readability
        glm::vec4 texcoords(f.x, f.y, f.width, f.height);

        s.write(f.duration);
        s.write(texcoords);
        s.write(f.origin);
    }
    s.dedent();
    s.writeRaw('}');

    s.writeIdentifier("tracks");
    s.writeWhitespace();
    s.writeRaw('{');
    s.indent();
    for (unsigned int i = 0; i < tracks.size(); i++) {
        const SpriteAnimationTrack &t = tracks[i];

        s.write(t.name);
        s.write(t.start);
        s.write(t.length);
    }
    s.dedent();
    s.writeRaw('}');

    return true;
}

bool SpriteAnimation::deserialize(TextDeserializer &s)
{
    ResourceManager *resourceManager = getResourceManager();

    std::string textureName;
    std::string materialName;

    // Read texture 
    
    if (!s.skipIdentifier("texture"))
        return false;

    if (!s.read(textureName))
        return false;

    // Read material 
    
    if (!s.skipIdentifier("material"))
        return false;

    if (!s.read(materialName))
        return false;

    // Read loop type

    if (!s.skipIdentifier("loop"))
        return false;

    int loopType;
    if (!s.readEnum(loopTypeEnum, loopType))
        return false;

    loop = static_cast<LoopType>(loopType);

    // Read frames

    if (!s.skipIdentifier("frames"))
        return false;

    if (!s.skip(Token::LCURLY)) {
        return false;
    }
    
    while (!s.accept(Token::RCURLY)) {
        uint32_t duration;
        glm::vec4 texcoords;
        glm::vec2 origin;

        if (!s.read(duration)) {
            return false;
        }

        if (!s.read(texcoords)) {
            return false;
        }

        if (!s.read(origin)) {
            return false;
        }

        AnimatedSpriteFrame frame;
        frame.duration = duration;
        frame.x = texcoords.x;
        frame.y = texcoords.y;
        frame.width = texcoords.z;
        frame.width = texcoords.w;
        frame.origin = origin;

        if (!frames.append(frame))
            return false;
    }

    if (!s.skip(Token::RCURLY))
        return false;

    // Read tracks
    
    if (!s.skipIdentifier("tracks"))
        return false;

    if (!s.skip(Token::LCURLY))
        return false;

    while (!s.accept(Token::RCURLY)) {
        SpriteAnimationTrack track;

        if (!s.read(track.name))
            return false;

        if (!s.read(track.start))
            return false;

        if (!s.read(track.length))
            return false;

        if (track.start > frames.size()) {
            LOG_ERRORF("track '%s' start out of range, ignoring", track.name.c_str());
            continue;
        }

        if (track.start + track.length >= frames.size()) {
            LOG_ERRORF("track '%s' end out of range, ignoring", track.name.c_str());
            continue;
        }

        if (!tracks.append(track))
            return false;
    }

    if (!s.skip(Token::RCURLY))
        return false;

    texture = resourceManager->getHandleNonBlocking<GlTexture>(textureName);
    material = resourceManager->getHandleNonBlocking<GlMaterial>(materialName);

    texture.waitLoaded();
    material.waitLoaded();

    return true;
}

SpriteAnimation * SpriteAnimationFactory::create(Allocator &allocator)
{
    return SpriteAnimation::create(allocator);
}

Sprite::Sprite():
        angle(0.0f),
        scale(1.0f),
        alpha(1.0f)
{
    // empty
}

AnimatedSprite::AnimatedSprite():
        startFrame(0),
        endFrame(0),
        currentFrame(0),
        timer(0)
{
    // empty
}

AnimatedSprite::AnimatedSprite(SpriteAnimation *_anim):
        anim(NULL),
        startFrame(0),
        endFrame(0),
        currentFrame(0),
        timer(0)
{
    setAnimation(_anim);
}

void AnimatedSprite::setAnimation(SpriteAnimation *_anim)
{
    anim = _anim;
    timer = 0;
    currentFrame = 0;

    if (_anim) {
        startFrame = 0;
        endFrame = _anim->frames.size();

        texture = _anim->texture;
        material = _anim->material;

        if (startFrame != endFrame) {
            frame = _anim->frames[0];
        }
    }
    else {
        startFrame = 0;
        endFrame = 0;

        texture = NULL;
        material = NULL;
    }
}

void AnimatedSprite::update()
{
    if (!anim) {
        return;
    }

    if (timer != 0) {
        timer--;
    }

    // still got time in the current frame?
    if (timer != 0) {
        return;
    }

    // Next frame
    if (currentFrame + 1 < endFrame) {
        currentFrame++;
    }
    else {
        if (anim->loop == SpriteAnimation::LOOP_REPEAT) {
            currentFrame = startFrame;
        }
        else {
            return;
        }
    }

    frame = anim->frames[currentFrame];
}
    
} /* namespace miko */
