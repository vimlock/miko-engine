#ifndef MIKO_GRAPHICS_BUFFER_HPP
#define MIKO_GRAPHICS_BUFFER_HPP

#include "miko/core/object.hpp"

namespace miko {

namespace BufferUsageHint
{
    enum Enum {
        DYNAMIC_DRAW,
        DYNAMIC_READ,
        DYNAMIC_COPY,

        STATIC_DRAW,
        STATIC_READ,
        STATIC_COPY,

        STREAM_DRAW,
        STREAM_READ,
        STREAM_COPY
    };

    const char * toStr(Enum e);
}


class IIndexBuffer : public Object
{
public:
};

class IVertexBuffer : public Object
{
public:
};
    
} /* namespace miko */

#endif /* MIKO_GRAPHICS_BUFFER_HPP */

