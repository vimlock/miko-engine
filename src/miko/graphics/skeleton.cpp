#include "miko/graphics/skeleton.hpp"

#include "miko/io/text_serializer.hpp"
#include "miko/io/text_deserializer.hpp"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/matrix_decompose.hpp>

namespace miko {

Bone::Bone():
    parent(-1),
    child(-1),
    sibling(-1),
    skinned(true)
{
    // empty
}

Skeleton::Skeleton(Allocator &allocator):
    bones(allocator),
    root(-1)
{
    // empty
}

bool Skeleton::serialize(TextSerializer &s) const
{
    s.writeIdentifier("skeleton");
    s.writeWhitespace();

    s.writeRaw('{');
    s.indent();

    s.writeIdentifier("numBones");
    s.writeWhitespace();
    s.write(bones.size());
    s.writeNewline();

    for (unsigned int i = 0; i < bones.size(); i++) {
        if (i != 0) {
            s.writeNewline();
        }

        const Bone &bone = bones[i];

        s.writeString(bone.name);
        s.writeWhitespace();

        s.write(bone.parent);
        s.writeWhitespace();

        s.write(bone.position);
        s.writeWhitespace();

        s.write(bone.rotation);
        s.writeWhitespace();

        s.write(bone.offsetMatrix);
    }

    s.dedent();
    s.writeRaw('}');
    s.writeNewline();

    return true;
}

bool Skeleton::deserialize(TextDeserializer &s)
{
    if (!s.skip(Token::LCURLY)) {
        return false;
    }

    if (!s.skipIdentifier("numBones")) {
        return false;
    }

    uint32_t numBones;
    if (!s.read(numBones)) {
        return false;
    }

    bones.reserve(numBones);

    while (!s.accept(Token::RCURLY)) {

        std::string name;

        if (!s.read(name)) {
            return false;
        }

        int parent;

        if (!s.read(parent)) {
            return false;
        }

        Bone *bone = addBone(name, parent);
        if (!bone) {
            return false;
        }

        if (!s.read(bone->position)) {
            return false;
        }

        if (!s.read(bone->rotation)) {
            return false;
        }

        if (!s.read(bone->offsetMatrix)) {
            return false;
        }

        bone->offsetMatrix = bone->offsetMatrix;

        glm::mat4 localTransform(1.0f);
        localTransform = glm::translate(localTransform, bone->position);
        localTransform = localTransform * glm::mat4_cast(bone->rotation);

        if (bone->parent >= 0) {
            bone->worldMatrix = getBone(bone->parent)->worldMatrix * localTransform;
        }
        else {
            bone->worldMatrix = localTransform;
        }
    }


    if (!s.skip(Token::RCURLY)) {
        return false;
    }

    dump();

    return true;
}

void Skeleton::dump()
{
    dumpBone(getRootBoneIndex(), 0);
}

void Skeleton::dumpBone(int index, int indent)
{
    Bone *bone = getBone(index);
    if (!bone) {
        return;
    }

    LOG_INFOF("%*s%d sbone %s", indent, "", index, bone->name.c_str());

    for (int i = bone->child; i != -1; i = getBone(i)->sibling) {
        dumpBone(i, indent + 4);
    }
}

Bone * Skeleton::addBone(const std::string &name, int parent)
{
    if (parent < 0) {
        if (root >= 0) {
            LOG_ERRORF("duplicate root bone, '%s'", name.c_str());
            return NULL;
        }
    }
    else if (parent > (int)bones.size()) {
        LOG_ERRORF("invalid bone parent index: '%d'", parent);
        return NULL;
    }

    if (getBone(name)) {
        LOG_ERRORF("duplicate bone name '%s'", name.c_str());
        return NULL;
    }

    if (!bones.append(Bone())) {
        return NULL;
    }

    // our id
    int id = bones.size() - 1;

    bones[id].parent = parent;
    bones[id].name = name;

    // set the bone as root bone if it does not have a parent
    if (parent < 0) {
        root = id;
    }

    // else add it to the parents children
    else {
        bones[id].sibling = bones[parent].child;
        bones[parent].child = id;
    }

    return &bones[id];
}

Bone * Skeleton::getBone(const std::string &name)
{
    for (unsigned i = 0; i < bones.size(); i++) {
        if (bones[i].name == name) {
            return &bones[i];
        }
    }

    return NULL;
}

Bone * Skeleton::getBone(unsigned int index)
{
    if (index < bones.size()) {
        return &bones[index];
    }

    return NULL;
}

int Skeleton::getBoneIndex(const std::string &name)
{
    for (int i = 0; i < (int)bones.size(); i++) {
        if (bones[i].name == name) {
            return i;
        }
    }

    return -1;
}
unsigned int Skeleton::numBones() const
{
    return bones.size();
}

Bone * Skeleton::getRootBone()
{
    if (root < 0) {
        return NULL;
    }
    else {
        return getBone(root);
    }
}

int Skeleton::getRootBoneIndex()
{
    return root;
}
   
} /* namespace miko */
