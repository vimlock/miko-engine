#include "miko/graphics/camera.hpp"
#include "miko/io/log.hpp"

#include <glm/gtc/matrix_transform.hpp>

namespace miko {

Camera::Camera()
{
    mode = CameraMode::DISABLED;

    position = glm::vec3(0.0, 0.0, 0.0);
    target = glm::vec3(0.0, 0.0, -1.0);
    up = glm::vec3(0.0, 1.0, 0.0);

    nearplane = 0.1f;
    farplane = 100.0f;

    scale = glm::vec2(1.0f, 1.0f);
}

void Camera::setPerspective(float _fov, float _xscale, float _yscale)
{
    mode = CameraMode::PERSPECTIVE;
    fov = _fov;
    scale.x = _xscale;
    scale.y = _yscale;
}

void Camera::setOrtho(float _xscale, float _yscale)
{
    mode = CameraMode::ORTHO;
    scale.x = _xscale;
    scale.y = _yscale;
}

void Camera::setClipPlane(float _nearplane, float _farplane)
{
    nearplane = _nearplane;
    farplane = _farplane;
}

void Camera::setPosition(const glm::vec3 &_position)
{
    position = _position;
}

void Camera::setTarget(const glm::vec3 &_target)
{
    target = _target;
}

const glm::vec3 Camera::getDirection() const
{
    return glm::normalize(target - position);
}

const glm::mat4 Camera::getViewMatrix() const
{
    return glm::lookAt(position, target, up);
}

const glm::mat4 Camera::getProjectionMatrix(const glm::vec2& dimensions) const
{
    if (mode == CameraMode::ORTHO) {
        return glm::ortho(0.0f, dimensions.x, 0.0f, dimensions.y, nearplane, farplane);
    }
    else if (mode == CameraMode::PERSPECTIVE) {
        float aspect = dimensions.x / dimensions.y;
        return glm::perspective(fov, aspect, nearplane, farplane);
    }
    else {
        return glm::mat4(1.0f);
    }
}

    
} /* namespace miko */
