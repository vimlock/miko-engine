#ifndef MIKO_GRAPHICS_ANIMATION_HPP
#define MIKO_GRAPHICS_ANIMATION_HPP

#include "miko/container/vector.hpp"
#include "miko/graphics/gl/graphics.hpp"

#include "miko/resource/resource.hpp"
#include "miko/resource/resource_loader.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

namespace miko {

class TextSerializer;
class TextDeserializer;

/// Frame in the animation channel
class SkeletonAnimationFrame
{
public:
    /// Time when this frame should be used
    float time;

    /// Position relative to the parent bone
    glm::vec3 position;

    /// Rotation relative to the parent bone
    glm::quat rotation;
};

class SkeletonAnimationChannel
{
public:
    SkeletonAnimationChannel(Allocator &allocator);

    bool serialize(TextSerializer &s) const;
    bool deserialize(TextDeserializer &s);

    SkeletonAnimationFrame * addFrame();
    SkeletonAnimationFrame * getFrame(unsigned int index);

    unsigned int  getFrameByTime(float time);

    std::string name;
    Vector<SkeletonAnimationFrame> frames;
};

class SkeletonAnimation : public Resource
{
MIKO_RESOURCE(RESOURCE_MESH_ANIMATION)
public:
    SkeletonAnimation(Allocator &allocator);
    ~SkeletonAnimation();

    virtual bool loadAsyncPart(SDL_RWops *rwops);
    virtual bool loadSyncPart(SDL_RWops *rwops);

    bool serialize(TextSerializer &s) const;
    bool deserialize(TextDeserializer &s);

    SkeletonAnimationChannel * addChannel();

    int getChannelIndex(const std::string &name);
    SkeletonAnimationChannel * getChannel(int index);

    unsigned int numChannels() const
    {
        return channels.size();
    }

    float duration;
    float speed;

private:
    Vector<SkeletonAnimationChannel*> channels;
};

class SkeletonAnimationFactory : public IResourceFactory
{
    virtual SkeletonAnimation * create(Allocator &allocator);
};
    
} /* namespace miko */

#endif /* MIKO_GRAPHICS_ANIMATION_HPP */

