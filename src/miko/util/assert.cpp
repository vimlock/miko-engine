#include "miko/util/assert.hpp"
#include "miko/io/log.hpp"

#include <sys/signal.h>

namespace miko {

void _assert(const char *file, int line, const char *str)
{
    LOG_ERRORF("%s:%d: Assert(%s) failed", file, line, str);
    raise(SIGTRAP);
}
    
} /* namespace miko */
