#ifndef MIKO_REFCOUNTED_HPP
#define MIKO_REFCOUNTED_HPP

#include "miko/miko.hpp"
#include "miko/util/assert.hpp"

namespace miko {

class Refcounted
{
public:
    /// Constructor.
    ///
    /// Initializes the reference count to zero.
    Refcounted():
        refcount(0)
    {
        // empty
    }

    virtual ~Refcounted();

    /// Function to be called when reference count reaches zero
    ///
    /// Calls MIKO_DELETE on the object by default.
    virtual void deallocate();
    
    /// Increments the reference count by one.
    void acquire();

    /// Decrements the reference count by one.
    ///
    /// If the reference count reaches zero, the object is freed
    void release();

    /// Returns the number of references this object has
    uint32_t getRefcount() const;

private:
    uint32_t refcount;

};

template<typename T>
class Ref
{
public:
    Ref();
    Ref(T *p);
    Ref(const Ref<T> &other);
    ~Ref();

    /// Assignment from an another reference
    Ref<T>& operator = (const Ref<T> &other);

    /// Assignment from a refcountable object
    Ref<T>& operator = (T *other);

    /// Increment the reference cout by one.
    ///
    /// Calling this function on NULL reference is safe.
    void acquire();

    /// Decrements the reference cout by one and sets the reference to NULL.
    ///
    /// Calling this function on NULL reference is safe.
    void release();

    /// Returns the raw pointer this ref manages
    T*& asPointer();

    /// Returns the raw pointer this ref manages
    const T*& asPointer() const;

    /// Cast to pointer (lvalue)
    operator T * ();

    /// Cast to pointer (lvalue)
    operator const T * () const;

    
    /// Dereference operator
    T & operator * ();

    /// Dereference operator
    const T & operator * () const;

    /// Arrow operator
    T * operator -> ();

    /// Arrow operator
    const T * operator -> () const;

private:
    T *ptr;

};
    
} /* namespace miko */

#include "miko/util/refcounted.inl"

#endif /* MIKO_REFCOUNTED_HPP */

