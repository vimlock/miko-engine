#ifndef MIKO_CORE_ASSERT_HPP
#define MIKO_CORE_ASSERT_HPP

#ifdef MIKO_UNITTEST
#include <CppUTest/TestHarness.h>
#endif

namespace miko {

#ifdef MIKO_UNITTEST
#define mikoAssert(expr) CHECK(expr)
#else

#define mikoAssert(expr) \
do {                 \
    if (!(expr))     \
     miko::_assert(__FILE__, __LINE__, #expr); \
} while (0)

#endif

#define mikoUnreachable() \
 mikoAssert(0 && "This code path should not be reachable")

void _assert(const char *file, int line, const char *str);
    
} /* namespace miko */

#endif /* MIKO_CORE_ASSERT_HPP */

