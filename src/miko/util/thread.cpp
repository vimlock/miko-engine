#include "miko/util/thread.hpp"

#include <SDL2/SDL_thread.h>

namespace miko {

static SDL_threadID mainThreadId = SDL_ThreadID();

bool isMainThread()
{
    return mainThreadId == SDL_ThreadID();
}
    
} /* namespace miko */
