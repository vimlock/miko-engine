#ifndef MIKO_MUTEX_HPP
#define MIKO_MUTEX_HPP

#include <SDL2/SDL_mutex.h>

namespace miko {

class Condition;

bool isMainThread();

class Mutex
{
friend class Condition;

public:
    Mutex();
    ~Mutex();

    void lock();
    bool tryLock(uint32_t timeout);

    void unlock();
    
private:
    SDL_mutex *mutex;

};

class Semaphore
{
public:
    Semaphore(int count=0);
    ~Semaphore();

    void post();

    void wait();
    bool tryWait(uint32_t timeout);

private:
    SDL_semaphore *semaphore;

};

class Condition
{
public:
    Condition();
    ~Condition();

    void broadcast();
    void signal();

    void wait(Mutex &mutex);
    bool tryWait(Mutex &mutex, uint32_t timeout);

private:
    SDL_cond *cond;

};
    
} /* namespace miko */

#endif /* MIKO_MUTEX_HPP */

