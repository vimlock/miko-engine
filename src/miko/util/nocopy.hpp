#ifndef MIKO_NOCOPY_HPP
#define MIKO_NOCOPY_HPP

namespace miko {

class nocopy
{
public:
    nocopy()
    {
        // empty
    }

private:
    nocopy(const nocopy&);
    void operator = (const nocopy &);

};
    
} /* namespace miko */

#endif /* MIKO_NOCOPY_HPP */

