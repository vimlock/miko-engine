#include "miko/util/mutex.hpp"
#include "miko/util/assert.hpp"

namespace miko {

Mutex::Mutex()
{
    mutex = SDL_CreateMutex();
    mikoAssert(mutex != NULL);
}

Mutex::~Mutex()
{
    SDL_DestroyMutex(mutex);
}

void Mutex::lock()
{
    mikoAssert(SDL_LockMutex(mutex) == 0);
}

bool Mutex::tryLock(uint32_t timeout)
{
    int ret = SDL_TryLockMutex(mutex);
    mikoAssert(ret >= -1);

    return ret == 0;
}

void Mutex::unlock()
{
    mikoAssert(SDL_UnlockMutex(mutex) == 0);
}

Semaphore::Semaphore(int count)
{
    semaphore = SDL_CreateSemaphore(count);
    mikoAssert(semaphore != NULL);
}

Semaphore::~Semaphore()
{
    SDL_DestroySemaphore(semaphore);
}

void Semaphore::post()
{
    mikoAssert(SDL_SemPost(semaphore) == 0);
}

void Semaphore::wait()
{
    mikoAssert(SDL_SemWait(semaphore) == 0);
}

bool Semaphore::tryWait(uint32_t timeout)
{
    int ret;

    if (timeout) {
        ret = SDL_SemWaitTimeout(semaphore, timeout);
    }
    else {
        ret = SDL_SemTryWait(semaphore);
    }

    mikoAssert(ret >= 0);

    return ret == 0;
}

Condition::Condition()
{
    cond = SDL_CreateCond();
    mikoAssert(cond != NULL);
}

Condition::~Condition()
{
    SDL_DestroyCond(cond);
}

void Condition::broadcast()
{
    mikoAssert(SDL_CondBroadcast(cond) == 0);
}

void Condition::signal()
{
    mikoAssert(SDL_CondSignal(cond) == 0);
}

void Condition::wait(Mutex &mutex)
{
    mikoAssert(SDL_CondWait(cond, mutex.mutex) == 0);
}

bool Condition::tryWait(Mutex &mutex, uint32_t timeout)
{
    int ret = SDL_CondWaitTimeout(cond, mutex.mutex, timeout);
    mikoAssert(ret >= 0);

    return ret == 0;
}
    
} /* namespace miko */
