#ifndef MIKO_UTIL_PROFILER_HPP
#define MIKO_UTIL_PROFILER_HPP

#include "miko/container/hash_map.hpp"
#include "miko/container/vector.hpp"

#include <string>

namespace miko {

struct ProfilingSample
{
    std::string name;
    uint32_t duration;
};

class Profiler
{
public:
};
    
} /* namespace miko */

#endif /* MIKO_UTIL_PROFILER_HPP */

