#ifndef MIKO_UTIL_THREAD_HPP
#define MIKO_UTIL_THREAD_HPP

namespace miko {

bool isMainThread();
    
} /* namespace miko */

#endif /* MIKO_UTIL_THREAD_HPP */

