#ifndef MIKO_REFCOUNTED_INL
#define MIKO_REFCOUNTED_INL

#include "miko/util/refcounted.hpp"
#include "miko/memory/memory.hpp"

namespace miko {

inline Refcounted::~Refcounted()
{
    mikoAssert(refcount == 0);
}

inline void Refcounted::deallocate()
{
    MIKO_DELETE(defaultAllocator, this);
}

inline void Refcounted::acquire()
{
    refcount++;
}

inline void Refcounted::release()
{
    mikoAssert(refcount != 0);
    refcount--;

    // was this the last reference ?
    if (refcount == 0) {
        // no references left, commit suicide
        deallocate();
    }
}

inline uint32_t Refcounted::getRefcount() const
{
    return refcount;
}

template <typename T>
Ref<T>::Ref():
        ptr(0)
{
    // empty
}

template <typename T>
Ref<T>::Ref(T *p)
{
    if (p) {
        p->acquire();
    }

    ptr = p;
}

template <typename T>
Ref<T>::Ref(const Ref<T> &other)
{
    if (other.ptr) {
        other.ptr->acquire();
    }

    this->ptr = other.ptr;
}

template <typename T>
Ref<T>::~Ref()
{
    if (ptr) {
        ptr->release();
    }
}

template <typename T>
Ref<T>& Ref<T>::operator = (const Ref<T> &other)
{
    if (other.ptr) {
        other.ptr->acquire();
    }

    if (this->ptr) {
        this->ptr->release();
    }

    this->ptr = other.ptr;

    return *this;
}

template <typename T>
Ref<T>& Ref<T>::operator = (T *other)
{
    if (other) {
        other->acquire();
    }

    if (this->ptr) {
        this->ptr->release();
    }

    this->ptr = other;

    return *this;
}

template <typename T>
void Ref<T>::acquire()
{
    if (ptr) {
        ptr->acquire();
    }
}

template <typename T>
void Ref<T>::release()
{
    if (ptr) {
        ptr->release();
    }

    ptr = NULL;
}

template <typename T>
T*& Ref<T>::asPointer()
{
    return ptr;
}

template <typename T>
const T*& Ref<T>::asPointer() const
{
    return ptr;
}

template <typename T>
Ref<T>::operator T * ()
{
    return ptr;
}

template <typename T>
Ref<T>::operator const T * () const
{
    return ptr;
}

template <typename T>
T & Ref<T>::operator * ()
{
    return *ptr;
}

template <typename T>
const T & Ref<T>::operator * () const
{
    return *ptr;
}

template <typename T>
T * Ref<T>::operator -> ()
{
    return ptr;
}

template <typename T>
const T * Ref<T>::operator -> () const
{
    return ptr;
}
    
} /* namespace miko */

#endif /* MIKO_REFCOUNTED_INL */

